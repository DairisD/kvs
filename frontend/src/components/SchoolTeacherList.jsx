import { Component } from "react";
import { withRouter } from "react-router-dom";
import {
  Layout,
  Spin,
  Button,
  Input,
  Form,
  message,
  Alert,
  Table,
} from "antd";
import { PlusOutlined, SearchOutlined } from "@ant-design/icons";
import { acquire, send } from "../lib/api";

const { Content } = Layout;

class SchoolTeacherList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      teachers: [],
      loaded: false,
      authorized: false,
      searched: null,
      schoolId: this.props.match.params.id,
    };
  }

  // Kolonnas skolotāju tabulai
  renderColumns = () => {
    return [
      {
        title: "Vārds",
        dataIndex: ["user", "name"],
        key: ["user", "name"],
        sorter: (a, b) =>
          a.user.name.toLowerCase().localeCompare(b.user.name.toLowerCase()),
      },
      {
        title: "Uzvārds",
        dataIndex: ["user", "surname"],
        key: ["user", "surname"],
        sorter: (a, b) =>
          a.user.surname.toLowerCase().localeCompare(b.user.surname.toLowerCase()),
      },
      {
        title: "Darbības",
        render: (text, record) => {
          return <Button onClick={() => this.props.history.push(`/teacher/${record.id}/edit`)}>Rediģēt skolotāja datus</Button>;
        },
      },
    ];
  }

  // Funkcija, kas iegūst datus par skolotājiem
  getTeacherData = () => {
    if (this.state.searched) {
      this.searchRoom(this.state.searched, false);
    } else {
      acquire(`school/${this.state.schoolId}/teachers`, {
        page: this.state.current,
      })
        .then((res) => {
          if (res.status === 200) {
            this.setState({
              teachers: res.data.data,
              loaded: true,
              authorized: true,
            });
          }
        })
        .catch((err) => {
          if (err.status !== 403) {
            message.error("Sistēmas kļūda! Pamēģiniet vēlreiz.");
          }
          this.setState({ loaded: true });
        });
    }
  };

  // Skolotāju meklēšana
  searchTeacher = (data) => {
    send(`school/${this.state.schoolId}/teachers/search`, data)
      .then((res) => {
        if (res.status === 200) {
          this.setState({
            teachers: res.data.data,
          });
        }
      })
      .catch((err) => {
        message.error("Sistēmas kļūda! Pamēģiniet vēlreiz.");
      });
  };

  // Ielādes laikā tiek iegūti dati par skolotājiem
  componentDidMount() {
    this.getTeacherData();
  }

  render() {
    // Ja lietotājs nav autorizēts redzēt konkrēto resursu, tad tiek attēlots paziņojums par to
    if (!this.state.authorized && this.state.loaded) {
      return (
        <Layout className="class-list-layout">
          <Content>
            <Alert
              message="Nav piekļuves"
              description="Jums nav piekļuves šiem datiem vai funkcionalitātei!"
              type="error"
              showIcon
            />
          </Content>
        </Layout>
      );
    } else {
      return (
        <Layout className="class-list-layout">
          <Content>
            <div className="class-list-head">
              <Button
                type="primary"
                onClick={() =>
                  this.props.history.push(
                    `/school/${this.state.schoolId}/teachers/new`
                  )
                }
              >
                Pievienot jaunu skolotāju
                <PlusOutlined />
              </Button>
              <Form layout="inline" onFinish={this.searchTeacher}>
                <Form.Item name="student_name">
                  <Input placeholder="Meklēt skolotāju" />
                </Form.Item>
                <Form.Item>
                  <Button type="primary" htmlType="submit">
                    <SearchOutlined />
                  </Button>
                </Form.Item>
              </Form>
            </div>
            {this.state.loaded ? (
              <>
                <Table
                  columns={this.renderColumns()}
                  dataSource={this.state.teachers}
                ></Table>
              </>
            ) : (
              <div className="class-list-spin-div">
                <Spin />
              </div>
            )}
          </Content>
        </Layout>
      );
    }
  }
}

export default withRouter(SchoolTeacherList);
