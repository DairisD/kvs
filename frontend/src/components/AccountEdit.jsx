import { Layout, Form, Input, Button, message, Alert, Spin } from "antd";
import { Component } from "react";
import { acquire, edit } from "../lib/api";
import { withRouter } from "react-router-dom";

const { Content } = Layout;

class AccountEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      authorized: false,
      loaded: false,
      editName: false,
      passwordChange: false
    };
  }

  // Funkcija, kas nosūta datus par konta labojumiem
  sendAccountData = (values) => {
    edit(`account/edit`, {
      ...values,
      passwordChange: this.state.passwordChange,
    })
      .then((res) => {
        if (res.status === 200) {
          message.success("Konta dati atjaunoti!");
        }
      })
      .catch((err) => {
        if(err.response.status !== 422) {
          message.error("Sistēmas kļūda! Pamēģiniet vēlreiz!");
        }
        else {
          message.error("Lietotājvārds jau aizņemts!");
        }
      });
  };

  // Attēlo paroles laukus, ja lietotājs ir nospiedis attiecīgo pogu
  renderPasswordFields = () => {
    return (
      <>
        <Form.Item
          name="password"
          label="Parole"
          rules={[
            {
              required: true,
              message: "Ievadiet lietotāja paroli!",
            },
          ]}
          hasFeedback
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          name="confirm_password"
          label="Atkārtot paroli"
          dependencies={["password"]}
          hasFeedback

          // Ant design piemērs kā izveidot paroles lauka validāciju
          rules={[
            {
              required: true,
              message: "Apstipriniet paroli!",
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue("password") === value) {
                  return Promise.resolve();
                }

                return Promise.reject(
                  new Error("Ievadītās paroles nav vienādas!")
                );
              },
            }),
          ]}
        >
          <Input.Password />
        </Form.Item>
      </>
    );
  };

  // Komponentes ielādes brīdī tiek iegūti jau esošie dati par kontu
  componentDidMount() {
    acquire(`account/edit`)
      .then((res) => {
        this.setState({
          authorized: true,
          loaded: true,
          editName: res.data.editName,
          passwordChange: false,
          accountData: res.data.data,
        });
      })
      .catch((err) => {
        this.setState({ authorized: false, loaded: true });
      });
  }

  render() {
    // Ja dati vēl nav ielādēti, tad tiek attēlots "Spin" ielādes elements
    if (!this.state.loaded) {
      return (
        <Layout className="new-edit-student-content">
          <Content>
            <div className="edit-student-form-div">
              <Spin />
            </div>
          </Content>
        </Layout>
      );
    }
    // Ja lietotājs nav autorizēts redzēt konkrēto saturu, tad par to tiek brīdināts ar "Alert" elementu
    if (!this.state.authorized) {
      return (
        <Layout className="student-list-layout">
          <Content>
            <Alert
              message="Nav piekļuves"
              description="Jums nav piekļuves šiem datiem vai funkcionalitātei!"
              type="error"
              showIcon
            />
          </Content>
        </Layout>
      );
    }
    return (
      <Layout className="new-edit-student-content">
        <Content>
          <div className="new-student-form-div">
            <Form
              labelCol={{ span: 6 }}
              wrapperCol={{ span: 24 }}
              onFinish={this.sendAccountData}
              className="new-student-form"
              initialValues={{
                username: this.state.accountData.username,
                name: this.state.accountData.name,
                surname: this.state.accountData.surname,
              }}
            >
              <Form.Item
                name="username"
                label="Lietotājvārds"
                rules={[
                  { required: true, message: "Ievadiet lietotājvārdu!" },
                  {
                    pattern: "^[a-z0-9_]{5,255}$",
                    message:
                      "Lietotājvārds nesatur atstarpes, lielos burtus un ir vismaz 5 simbolus garš!",
                  },
                ]}
              >
                <Input disabled={!this.state.editName} />
              </Form.Item>

              <Form.Item
                name="name"
                label="Vārds"
                rules={[{ required: true, message: "Ievadiet vārdu!" }]}
              >
                <Input disabled={!this.state.editName} />
              </Form.Item>
              <Form.Item
                name="surname"
                label="Uzvārds"
                rules={[{ required: true, message: "Ievadiet uzvārdu!" }]}
                disabled={!this.state.editName}
              >
                <Input disabled={!this.state.editName} />
              </Form.Item>
              {this.state.passwordChange ? this.renderPasswordFields() : null}
              <Form.Item>
                <Button
                  className="edit-room-button"
                  onClick={() => {
                    this.setState({
                      passwordChange: !this.state.passwordChange,
                    });
                  }}
                >
                  {this.state.passwordChange
                    ? "Nemainīt paroli"
                    : "Mainīt paroli"}
                </Button>
              </Form.Item>
              <Form.Item>
                <Button
                  className="new-student-button"
                  htmlType="submit"
                  type="primary"
                >
                  Atjaunot profila datus
                </Button>
              </Form.Item>
            </Form>
          </div>
        </Content>
      </Layout>
    );
  }
}

export default withRouter(AccountEdit);
