import { Layout, Form, Input, Button, message, Alert, Spin } from "antd";
import { Component } from "react";
import { send } from "../lib/api";
import { withRouter } from "react-router-dom";
import { acquire } from "../lib/api";

const { Content } = Layout;

class NewSchool extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newSchoolAdmin: false,
      authorized: false,
    };
  }

  // Funkcija, kas maina pogas tekstu atkarībā no tā, vai
  // lietotājs ir izvēlējies veidot jaunu skolas administratora kontu
  renderAccountButton = () => {
    return this.state.newSchoolAdmin
      ? "Lietot esošu kontu"
      : "Veidot jaunu administratora kontu";
  };

  // Formas lauku attēlošana
  renderFormFields = () => {
    // Gadījums, ja tiek veidots jauns konts skolas admiistratoram
    return this.state.newSchoolAdmin ? (
      <>
        <Form.Item
          name="username"
          label="Lietotājvārds"
          rules={[
            { required: true, message: "Ievadiet lietotājvārdu!" },
            {
              pattern: "^[a-z0-9_]{5,255}$",
              message:
                "Lietotājvārds nesatur atstarpes, lielos burtus un ir vismaz 5 simbolus garš!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="name"
          label="Vārds"
          rules={[{ required: true, message: "Ievadiet vārdu!" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="surname"
          label="Uzvārds"
          rules={[{ required: true, message: "Ievadiet uzvārdu!" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="password"
          label="Parole"
          rules={[
            {
              required: true,
              message: "Ievadiet lietotāja paroli!",
            },
          ]}
          hasFeedback
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          name="confirm_password"
          label="Atkārtot paroli"
          dependencies={["password"]}
          hasFeedback
          rules={[
            {
              required: true,
              message: "Apstipriniet paroli!",
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue("password") === value) {
                  return Promise.resolve();
                }

                return Promise.reject(
                  new Error("Ievadītās paroles nav vienādas!")
                );
              },
            }),
          ]}
        >
          <Input.Password />
        </Form.Item>
      </>
    ) : (
      // Gadījums, ja tiek tiek izveidots jauns profils esošam kontam
      <Form.Item
        name="username"
        label="Lietotajvārds"
        rules={[{ required: true, message: "Ievadiet lietotājvārdu!" }]}
      >
        <Input />
      </Form.Item>
    );
  };

  // Jauna skolas ieraksta datu nosūtīšana uz serveri
  sendSchoolData = (values) => {
    send("school", { ...values, newUser: this.state.newSchoolAdmin })
      .then((res) => {
        message.success("Skolas ieraksts saglabāts!");
        this.props.history.push("/schools");
      })
      .catch((err) => {
        if (err.response.status === 422 || err.response.status === 404) {
          message.error(err.response.data.message);
        } else {
          message.error("Sistēmas kļūda! Pamēģiniet vēlreiz!");
        }
      });
  };

  // Komponentes ielādes laikā tiek veikta tiesību pārbaude
  componentDidMount() {
    acquire(`school/create`)
      .then((res) => {
        this.setState({ authorized: true, loaded: true });
      })
      .catch((err) => {
        this.setState({ authorized: false, loaded: true });
      });
  }

  render() {
    // Ja dati nav ielādēti, tad tiek attēlots "Spin" elements
    if (!this.state.loaded) {
      return (
        <Layout className="new-edit-class-content">
          <Content>
            <div className="edit-class-form-div">
              <Spin />
            </div>
          </Content>
        </Layout>
      );
    }
    // Ja lietotājam nav tiesības redzēt konkrēto resursu, tad tiek attēlots "Alert" brīdinājums
    if (!this.state.authorized) {
      return (
        <Layout className="class-list-layout">
          <Content>
            <Alert
              message="Nav piekļuves"
              description="Jums nav piekļuves šiem datiem vai funkcionalitātei!"
              type="error"
              showIcon
            />
          </Content>
        </Layout>
      );
    }
    return (
      <Layout className="new-edit-school-content">
        <Content>
          <div className="new-school-form-div">
            <Form
              labelCol={{ span: 6 }}
              wrapperCol={{ span: 24 }}
              onFinish={this.sendSchoolData}
              className="new-school-form"
            >
              <Form.Item
                name="school_name"
                label="Skolas nosaukums"
                rules={[
                  { required: true, message: "Ievadiet skolas nosaukumu!" },
                ]}
              >
                <Input />
              </Form.Item>
              <Button
                className="new-school-button"
                onClick={() => {
                  this.setState({ newSchoolAdmin: !this.state.newSchoolAdmin });
                }}
              >
                {this.renderAccountButton()}
              </Button>
              {this.renderFormFields()}
              <Form.Item>
                <Button
                  className="new-school-button"
                  htmlType="submit"
                  type="primary"
                >
                  Izveidot skolas ierakstu
                </Button>
              </Form.Item>
            </Form>
          </div>
        </Content>
      </Layout>
    );
  }
}

export default withRouter(NewSchool);
