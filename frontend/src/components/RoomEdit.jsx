import { Component } from "react";
import { withRouter } from "react-router-dom";
import {
  Layout,
  Form,
  Input,
  Button,
  message,
  Alert,
  Spin,
  Modal,
  InputNumber,
} from "antd";
import { edit, acquire, remove } from "../lib/api";

const { Content } = Layout;

class RoomEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      room: this.props.match.params.id,
      roomData: {},
      loaded: false,
      modalVisible: false,
      authorized: true,
    };
  }

  // Atjaunoto datu nosūtīšana
  editRoom = (data) => {
    edit(`rooms/${this.state.room}`, data)
      .then((res) => {
        if (res.status === 200) {
          message.success("Telpas dati veiksmīgi nomainīti!");
          this.props.history.push(`/school/${res.data.schoolId}/rooms`);
        }
      })
      .catch((err) => {
        if (err.response.status === 422 || err.response.status === 404) {
          message.error(err.response.data.message);
        } else {
          message.error("Sistēmas kļūda! Pamēģiniet vēlreiz!");
        }
      });
  };

  // Modālā loga aizvēršanas / atvēršanas funkcija
  setModalVisible = (value) => {
    this.setState({ modalVisible: value });
  };

  // Telpas dzēšanas funkcija
  deleteRoom = () => {
    remove(`rooms/${this.state.room}`)
      .then((res) => {
        if (res.status === 200) {
          message.success("Telpas ieraksts dzēsts");
          this.props.history.push(`/school/${res.data.schoolId}/rooms`);
        }
      })
      .catch((err) => {
        message.error("Sistēmas kļūda! Pamēģiniet vēlreiz!");
      });
  };

  // Ielādes laikā tiek iegūti dati par telpu
  componentDidMount() {
    acquire(`rooms/${this.state.room}/edit`)
      .then((res) => {
        this.setState({ roomData: res.data.data, loaded: true });
      })
      .catch((err) => {
        this.setState({ authorized: false, loaded: true });
      });
  }

  render() {
    // Ja dati vēl nav ielādēti, tad tiek attēlots "spinner" ielādes elements
    if (!this.state.loaded) {
      return (
        <Layout className="new-edit-room-content">
          <Content>
            <div className="edit-room-form-div">
              <Spin />
            </div>
          </Content>
        </Layout>
      );
    }
    // Ja lietotājs nav autorizēts redzēt konkrēto saturu, tad par to tiek brīdināts ar "Alert" elementu
    if (!this.state.authorized) {
      return (
        <Layout className="school-list-layout">
          <Content>
            <Alert
              message="Nav piekļuves"
              description="Jums nav piekļuves šiem datiem vai funkcionalitātei!"
              type="error"
              showIcon
            />
          </Content>
        </Layout>
      );
    }
    return (
      <Layout className="new-edit-room-content">
        <Content>
          <Modal
            title="Vai dzēst telpas ierakstu?"
            onCancel={() => this.setModalVisible(false)}
            centered
            visible={this.state.modalVisible}
            footer={[
              <Button onClick={() => this.setModalVisible(false)}>
                Atcelt
              </Button>,
              <Button type="primary" danger onClick={this.deleteRoom}>
                Dzēst
              </Button>,
            ]}
          >
            Šo darbību nevar atsaukt! Tiks dzēsti arī ar šo telpu saistītie
            dati!
          </Modal>
          <div className="edit-room-form-div">
            <Form
              labelCol={{ span: 9 }}
              wrapperCol={{ span: 24 }}
              onFinish={this.editRoom}
              className="edit-school-form"
              initialValues={{
                room_name: this.state.roomData.name,
                room_size: this.state.roomData.seat_count,
              }}
            >
              <Form.Item
                name="room_name"
                label="Jauns telpas nosaukums"
                rules={[
                  { required: true, message: "Ievadiet telpas nosaukumu!" },
                ]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                name="room_size"
                label="Sēdvietu skaits"
                rules={[
                  {
                    type: "number",
                    min: 1,
                    max: 10000,
                    message: "Sēdvietu skaitam jābūt robežās no 1 līdz 10000!",
                  },
                  {
                    message: "Norādiet sēdvietu skaitu!",
                    required: true,
                  },
                ]}
              >
                <InputNumber />
              </Form.Item>
              <Form.Item>
                <Button
                  className="edit-room-button"
                  htmlType="submit"
                  type="primary"
                >
                  Rediģēt telpas datus
                </Button>
              </Form.Item>
              <Button
                className="delete-room-button"
                danger
                type="primary"
                onClick={() => this.setState({ modalVisible: true })}
              >
                Dzēst telpas ierakstu
              </Button>
            </Form>
          </div>
        </Content>
      </Layout>
    );
  }
}

export default withRouter(RoomEdit);
