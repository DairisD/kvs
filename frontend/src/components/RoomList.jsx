import { Component } from "react";
import { withRouter, Link } from "react-router-dom";
import {
  Layout,
  Spin,
  Pagination,
  Button,
  Input,
  Form,
  message,
  Collapse,
  Typography,
  Alert
} from "antd";
import { PlusOutlined, SearchOutlined } from "@ant-design/icons";
import { acquire, send } from "../lib/api";

const { Content } = Layout;
const { Panel } = Collapse;
const { Text } = Typography;

class RoomList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rooms: [],
      loaded: false,
      authorized: false,
      current: 1,
      total: null,
      searched: null,
      schoolId: this.props.match.params.id,
    };
  }

  // Funkcija, kas nodrošina pārvietošanos pa lapām un secīgu datu ielādi
  pageChange = (page) => {
    this.setState({ current: page }, () => {
      this.getRoomData();
    });
  };

  // Telpu datu attēlošana sarakstā
  renderRoomData = () => {
    if (this.state.rooms.length !== 0) {
      return this.state.rooms.map((room) => {
        return (
          <Collapse key={room.id} className="room-collapse-item">
            <Panel header={[<Text strong>{room.name}</Text>,` [sēdvietas: ${room.seat_count}]`]}>
              <div className="room-config-buttons">
                <Button>
                  <Link to={"/room/" + room.id + "/edit"}>
                    Pārvaldīt telpas datus
                  </Link>
                </Button>
              </div>
            </Panel>
          </Collapse>
        );
      });
    } else {
      return (
        <Text className="schools-not-found-text">
          Nav atrasta neviena telpa
        </Text>
      );
    }
  };

  // Datu ieguve par telpām. Tiek ņemta vērā pašreizējā lapa. Lapas numurs tiek pievienots vaicājumā uz serveri.
  getRoomData = () => {
    if (this.state.searched) {
      this.searchRoom(this.state.searched, false);
    } else {
      acquire(`school/${this.state.schoolId}/rooms`, {
        page: this.state.current,
      })
        .then((res) => {
          if (res.status === 200) {
            this.setState({
              rooms: res.data.data.data,
              total: res.data.data.total,
              loaded: true,
              authorized: true,
            });
          }
        })
        .catch((err) => {
          if (err.status !== 403) {
            message.error("Sistēmas kļūda! Pamēģiniet vēlreiz.");
          }
          this.setState({ loaded: true });
        });
    }
  };

  // Telpas meklēšana. Dati no nodotās formas tiek pievienoti vaicājumam uz serveri.
  // Tiek veikta pārbaude, vai lietotājs maina tikai lapu vai arī veic jaunu meklēšanu (resetToOne).
  searchRoom = (data, resetToOne = true) => {
    if (!data?.room_name) {
      this.setState({ searched: null });
      this.getRoomData();
    } else {
      if (resetToOne) {
        this.setState({current: 1});
      }
      this.setState({ searched: data });
      send(`school/${this.state.schoolId}/rooms/search`, { ...data, page: this.state.current })
        .then((res) => {
          if (res.status === 200) {
            this.setState({
              rooms: res.data.data.data,
              total: res.data.data.total,
            });
          }
        })
        .catch((err) => {
          message.error("Sistēmas kļūda! Pamēģiniet vēlreiz.");
        });
    }
  };

  // Komponentes ielādes laikā tiek iegūti dati par telpām
  componentDidMount() {
    this.getRoomData();
  }

  render() {
    // Ja lietotājs nav autorizēts redzēt konkrēto resursu, tad tiek attēlots paziņojums par to
    if (!this.state.authorized && this.state.loaded) {
      return (
        <Layout className="room-list-layout">
          <Content>
            <Alert
              message="Nav piekļuves"
              description="Jums nav piekļuves šiem datiem vai funkcionalitātei!"
              type="error"
              showIcon
            />
          </Content>
        </Layout>
      );
    } else {
      return (
        <Layout className="room-list-layout">
          <Content>
            <div className="room-list-head">
              <Button
                type="primary"
                onClick={() => this.props.history.push(`/school/${this.state.schoolId}/rooms/new`)}
              >
                Izveidot jaunu telpu <PlusOutlined />
              </Button>
              <Form layout="inline" onFinish={this.searchRoom}>
                <Form.Item name="room_name">
                  <Input placeholder="Meklēt telpu" />
                </Form.Item>
                <Form.Item>
                  <Button type="primary" htmlType="submit">
                    <SearchOutlined />
                  </Button>
                </Form.Item>
              </Form>
            </div>
            {this.state.loaded ? (
              <>
                <div>{this.renderRoomData()}</div>
                <Pagination
                  current={this.state.current}
                  onChange={this.pageChange}
                  total={this.state.total}
                  defaultPageSize={8}
                />
              </>
            ) : (
              <div className="room-list-spin-div">
                <Spin />
              </div>
            )}
          </Content>
        </Layout>
      );
    }
  }
}

export default withRouter(RoomList);
