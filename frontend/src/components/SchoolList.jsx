import {
  Layout,
  Button,
  Form,
  Input,
  message,
  Collapse,
  Spin,
  Alert,
  Typography,
  Pagination,
} from "antd";
import { Component } from "react";
import { PlusOutlined, SearchOutlined } from "@ant-design/icons";
import { acquire, send } from "../lib/api";
import { withRouter, Link } from "react-router-dom";

const { Content } = Layout;
const { Panel } = Collapse;
const { Text } = Typography;

class SchoolList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      schools: [],
      loaded: false,
      authorized: false,
      current: 1,
      total: null,
      searched: null
    };
  }

  // Funkcija, kas nodrošina pārvietošanos pa lapām un secīgu datu ielādi
  pageChange = (page) => {
    this.setState({ current: page }, () => {
      this.getSchoolData();
    });
  };

  // Klašu datu attēlošana sarakstā
  renderSchoolData = () => {
    if (this.state.schools.length !== 0) {
      return this.state.schools.map((school) => {
        return (
          <Collapse key={school.id} className="school-collapse-item">
            <Panel header={<Text strong>{school.name}</Text>}>
              <div className="school-config-buttons">
                <Button>
                  <Link to={"/school/" + school.id + "/edit"}>
                    Pārvaldīt skolas datus
                  </Link>
                </Button>
                <Button>
                  <Link to={`/school/${school.id}/rooms`}>
                    Pārvaldīt telpu datus
                  </Link>
                </Button>
                <Button>
                  <Link to={`/school/${school.id}/classes`}>
                    Pārvaldīt klašu datus
                  </Link>
                </Button>
                <Button>
                  <Link to={`/school/${school.id}/admins`}>
                    Pārvaldīt skolas administratorus
                  </Link>
                </Button>
                <Button>
                  <Link to={`/school/${school.id}/teachers`}>
                    Pārvaldīt skolas skolotājus
                  </Link>
                </Button>
                <Button>
                  <Link to={`/school/${school.id}/consultations/`}>
                    Pārvaldīt konsultācijas
                  </Link>
                </Button>
              </div>
            </Panel>
          </Collapse>
        );
      });
    } else {
      return (
        <Text className="schools-not-found-text">
          Nav atrasta neviena skola
        </Text>
      );
    }
  };

  // Datu ieguve par skolām. Tiek ņemta vērā pašreizējā lapa. Lapas numurs tiek pievienots vaicājumā uz serveri.
  getSchoolData = () => {
    if(this.state.searched) {
      this.searchSchool(this.state.searched, false);
    }
    else {
    acquire("school", {page: this.state.current})
      .then((res) => {;
        if (res.status === 200) {
          this.setState({
            schools: res.data.data.data,
            total: res.data.data.total,
            loaded: true,
            authorized: true,
          });
        }
      })
      .catch((err) => {
        if (err.response.status !== 403) {
          message.error("Sistēmas kļūda! Pamēģiniet vēlreiz.");
        }
        this.setState({ loaded: true });
      });
    }
  };

  // Skolas meklēšana. Dati no nodotās formas tiek pievienoti vaicājumam uz serveri.
  // Tiek veikta pārbaude, vai lietotājs maina tikai lapu vai arī veic jaunu meklēšanu (resetToOne).
  searchSchool = (data, resetToOne = true) => {
    if (!data?.school_name) {
      this.setState({searched: null});
      this.getSchoolData();
    } else {
      if (resetToOne) {
        this.setState({current: 1});
      }
      this.setState({searched: data});
      send("schools/search", {...data, page: this.state.current})
        .then((res) => {
          if (res.status === 200) {
            this.setState({ 
              schools: res.data.data.data,
              total: res.data.data.total, });
          }
        })
        .catch((err) => {
          message.error("Sistēmas kļūda! Pamēģiniet vēlreiz.");
        });
    }
  };

  // Komponentes ielādes laikā tiek iegūti dati par skolām
  componentDidMount() {
    this.getSchoolData();
  }

  render() {
    // Ja lietotājs nav autorizēts redzēt konkrēto resursu, tad tiek attēlots paziņojums par to
    if (!this.state.authorized && this.state.loaded) {
      return (
        <Layout className="school-list-layout">
          <Content>
            <Alert
              message="Nav piekļuves"
              description="Jums nav piekļuves šiem datiem vai funkcionalitātei!"
              type="error"
              showIcon
            />
          </Content>
        </Layout>
      );
    } else {
      return (
        <Layout className="school-list-layout">
          <Content>
            <div className="school-list-head">
              <Button
                type="primary"
                onClick={() => this.props.history.push("/schools/new")}
              >
                Izveidot jaunu skolu <PlusOutlined />
              </Button>
              <Form layout="inline" onFinish={this.searchSchool}>
                <Form.Item name="school_name">
                  <Input placeholder="Meklēt skolu" />
                </Form.Item>
                <Form.Item>
                  <Button type="primary" htmlType="submit">
                    <SearchOutlined />
                  </Button>
                </Form.Item>
              </Form>
            </div>
            {this.state.loaded ? (
              <>
                <div className="school-list-body">
                  {this.renderSchoolData()}
                </div>
                <Pagination
                  current={this.state.current}
                  onChange={this.pageChange}
                  total={this.state.total}
                  defaultPageSize={8}
                />
              </>
            ) : (
              <div className="school-list-spin-div">
                <Spin />
              </div>
            )}
          </Content>
        </Layout>
      );
    }
  }
}

export default withRouter(SchoolList);
