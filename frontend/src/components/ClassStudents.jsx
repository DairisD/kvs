import { Component } from "react";
import { withRouter } from "react-router-dom";
import {
  Layout,
  Spin,
  Button,
  Input,
  Form,
  message,
  Alert,
  Table,
} from "antd";
import { PlusOutlined, SearchOutlined } from "@ant-design/icons";
import { acquire, send } from "../lib/api";

const { Content } = Layout;

class ClassStudents extends Component {
  constructor(props) {
    super(props);
    this.state = {
      students: [],
      loaded: false,
      authorized: false,
      searched: null,
      classId: this.props.match.params.id,
    };
  }

  // Kolonnas skolēnu tabulai
  renderColumns = () => {
    return [
      {
        title: "Vārds",
        dataIndex: ["user", "name"],
        key: ["user", "name"],
        sorter: (a, b) =>
          a.user.name.toLowerCase().localeCompare(b.user.name.toLowerCase()),
      },
      {
        title: "Uzvārds",
        dataIndex: ["user", "surname"],
        key: ["user", "surname"],
        sorter: (a, b) =>
          a.user.surname.toLowerCase().localeCompare(b.user.surname.toLowerCase()),
      },
      {
        title: "Darbības",
        render: (text, record) => {
          return <Button onClick={() => this.props.history.push(`/student/${record.id}/edit`)}>Rediģēt skolēna datus</Button>;
        },
      },
    ];
  }

  // Funkcija, kas iegūst datus par skolēniem konkrētajā klasē
  getStudentsData = () => {
    if (this.state.searched) {
      this.searchRoom(this.state.searched, false);
    } else {
      acquire(`class/${this.state.classId}/students`, {
        page: this.state.current,
      })
        .then((res) => {
          if (res.status === 200) {
            this.setState({
              students: res.data.data,
              loaded: true,
              authorized: true,
            });
          }
        })
        .catch((err) => {
          if (err.response.status !== 403) {
            message.error("Sistēmas kļūda! Pamēģiniet vēlreiz.");
          }
          this.setState({ loaded: true });
        });
    }
  };

  // Skolēnu meklēšana
  searchStudent = (data) => {
    send(`class/${this.state.classId}/students/search`, data)
      .then((res) => {
        if (res.status === 200) {
          this.setState({
            students: res.data.data,
          });
        }
      })
      .catch((err) => {
        if(err.response.status === 500) {
          message.error("Sistēmas kļūda! Pamēģiniet vēlreiz.");
        }
      });
  };

  // Ielādes laikā tiek iegūti dati par skolēniem
  componentDidMount() {
    this.getStudentsData();
  }

  render() {
    // Ja lietotājs nav autorizēts redzēt konkrēto resursu, tad tiek attēlots paziņojums par to
    if (!this.state.authorized && this.state.loaded) {
      return (
        <Layout className="class-list-layout">
          <Content>
            <Alert
              message="Nav piekļuves"
              description="Jums nav piekļuves šiem datiem vai funkcionalitātei!"
              type="error"
              showIcon
            />
          </Content>
        </Layout>
      );
    } else {
      return (
        <Layout className="class-list-layout">
          <Content>
            <div className="class-list-head">
              <Button
                type="primary"
                onClick={() =>
                  this.props.history.push(
                    `/class/${this.state.classId}/students/new`
                  )
                }
              >
                Pievienot jaunu skolēnu
                <PlusOutlined />
              </Button>
              <Form layout="inline" onFinish={this.searchStudent}>
                <Form.Item name="student_name">
                  <Input placeholder="Meklēt skolēnu" />
                </Form.Item>
                <Form.Item>
                  <Button type="primary" htmlType="submit">
                    <SearchOutlined />
                  </Button>
                </Form.Item>
              </Form>
            </div>
            {this.state.loaded ? (
              <>
                <Table
                  columns={this.renderColumns()}
                  dataSource={this.state.students}
                ></Table>
              </>
            ) : (
              <div className="class-list-spin-div">
                <Spin />
              </div>
            )}
          </Content>
        </Layout>
      );
    }
  }
}

export default withRouter(ClassStudents);
