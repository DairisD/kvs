import { Component } from "react";
import { withRouter } from "react-router-dom";
import {
  Layout,
  Spin,
  Button,
  Input,
  Form,
  message,
  Alert,
  Table,
} from "antd";
import { PlusOutlined, SearchOutlined } from "@ant-design/icons";
import { acquire, send } from "../lib/api";

const { Content } = Layout;

class SchoolAdminsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      admins: [],
      loaded: false,
      authorized: false,
      searched: null,
      schoolId: this.props.match.params.id,
    };
  }

  // Kolonnas administratoru tabulai
  renderColumns = () => {
    return [
      {
        title: "Vārds",
        dataIndex: ["user", "name"],
        key: ["user", "name"],
        sorter: (a, b) =>
          a.user.name.toLowerCase().localeCompare(b.user.name.toLowerCase()),
      },
      {
        title: "Uzvārds",
        dataIndex: ["user", "surname"],
        key: ["user", "surname"],
        sorter: (a, b) =>
          a.user.surname.toLowerCase().localeCompare(b.user.surname.toLowerCase()),
      },
      {
        title: "Darbības",
        render: (text, record) => {
          return <Button onClick={() => this.props.history.push(`/admin/${record.id}/edit`)}>Rediģēt administratora datus</Button>;
        },
      },
    ];
  }

  // Funkcija, kas iegūst datus par skolas administratoriem
  getAdminData = () => {
    if (this.state.searched) {
      this.searchAdmin(this.state.searched, false);
    } else {
      acquire(`school/${this.state.schoolId}/admins`, {
        page: this.state.current,
      })
        .then((res) => {
          if (res.status === 200) {
            this.setState({
              admins: res.data.data,
              loaded: true,
              authorized: true,
            });
          }
        })
        .catch((err) => {
          if (err.status !== 403) {
            message.error("Sistēmas kļūda! Pamēģiniet vēlreiz.");
          }
          this.setState({ loaded: true });
        });
    }
  };

  // Administratoru meklēšana
  searchAdmin = (data) => {
    send(`school/${this.state.schoolId}/admins/search`, data)
      .then((res) => {
        if (res.status === 200) {
          this.setState({
            admins: res.data.data,
          });
        }
      })
      .catch((err) => {
        message.error("Sistēmas kļūda! Pamēģiniet vēlreiz.");
      });
  };

  // Ielādes laikā tiek iegūti dati par administratoriem
  componentDidMount() {
    this.getAdminData();
  }

  render() {
    // Ja lietotājs nav autorizēts redzēt konkrēto resursu, tad tiek attēlots paziņojums par to
    if (!this.state.authorized && this.state.loaded) {
      return (
        <Layout className="class-list-layout">
          <Content>
            <Alert
              message="Nav piekļuves"
              description="Jums nav piekļuves šiem datiem vai funkcionalitātei!"
              type="error"
              showIcon
            />
          </Content>
        </Layout>
      );
    } else {
      return (
        <Layout className="class-list-layout">
          <Content>
            <div className="class-list-head">
              <Button
                type="primary"
                onClick={() =>
                  this.props.history.push(
                    `/school/${this.state.schoolId}/admins/new`
                  )
                }
              >
                Pievienot jaunu skolas administratoru
                <PlusOutlined />
              </Button>
              <Form layout="inline" onFinish={this.searchAdmin}>
                <Form.Item name="student_name">
                  <Input placeholder="Meklēt administratoru" />
                </Form.Item>
                <Form.Item>
                  <Button type="primary" htmlType="submit">
                    <SearchOutlined />
                  </Button>
                </Form.Item>
              </Form>
            </div>
            {this.state.loaded ? (
              <>
                <Table
                  columns={this.renderColumns()}
                  dataSource={this.state.admins}
                ></Table>
              </>
            ) : (
              <div className="class-list-spin-div">
                <Spin />
              </div>
            )}
          </Content>
        </Layout>
      );
    }
  }
}

export default withRouter(SchoolAdminsList);
