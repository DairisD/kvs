import { Layout, Form, Input, Button, message, Alert, Spin } from "antd";
import { Component } from "react";
import { send, acquire } from "../lib/api";
import { withRouter } from "react-router-dom";

const { Content } = Layout;

class NewSchoolTeacher extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newTeacherUser: false,
      authorized: false,
      schoolId: this.props.match.params.id,
    };
  }

  // Funkcija, kas maina pogas tekstu atkarībā no tā, vai
  // lietotājs ir izvēlējies veidot jaunu skolotāja kontu
  renderAccountButton = () => {
    return this.state.newTeacherUser
      ? "Lietot esošu kontu"
      : "Veidot jaunu skolotāja kontu";
  };

  // Formas lauku attēlošana
  renderFormFields = () => {
    // Gadījums, ja tiek veidots jauns konts
    return this.state.newTeacherUser ? (
      <>
        <Form.Item
          name="username"
          label="Lietotājvārds"
          rules={[
            { required: true, message: "Ievadiet lietotājvārdu!" },
            {
              pattern: "^[a-z0-9_]{5,255}$",
              message:
                "Lietotājvārds nesatur atstarpes, lielos burtus un ir vismaz 5 simbolus garš!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="name"
          label="Vārds"
          rules={[{ required: true, message: "Ievadiet vārdu!" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="surname"
          label="Uzvārds"
          rules={[{ required: true, message: "Ievadiet uzvārdu!" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="password"
          label="Parole"
          rules={[
            {
              required: true,
              message: "Ievadiet lietotāja paroli!",
            },
          ]}
          hasFeedback
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          name="confirm_password"
          label="Atkārtot paroli"
          dependencies={["password"]}
          hasFeedback
          rules={[
            {
              required: true,
              message: "Apstipriniet paroli!",
            },
            // Ant design piemērs par to kā izveidot paroles lauka validāciju
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue("password") === value) {
                  return Promise.resolve();
                }

                return Promise.reject(
                  new Error("Ievadītās paroles nav vienādas!")
                );
              },
            }),
          ]}
        >
          <Input.Password />
        </Form.Item>
      </>
    ) : (
      // Gadījums, ja tiek veidots tikai jauns profils
      <Form.Item
        name="username"
        label="Lietotajvārds"
        rules={[{ required: true, message: "Ievadiet lietotājvārdu!" }]}
      >
        <Input />
      </Form.Item>
    );
  };

  // Skolotāja datu nosūtīšana serverim
  sendTeacherData = (values) => {
    send(`school/${this.state.schoolId}/teachers`, {
      ...values,
      newUser: this.state.newTeacherUser,
    })
      .then((res) => {
        if (res.status === 200) {
          message.success("Skolotāja ieraksts saglabāts!");
          this.props.history.push(`/school/${this.state.schoolId}/teachers`);
        }
      })
      .catch((err) => {
        if(err.response.status === 422) {
          message.error("Lietotājvārds ir aizņemts!");
        }
        else {
          message.error("Sistēmas kļūda! Pamēģiniet vēlreiz!");
        }
      });
  };

  // Tiesību pārbaude komponentes ielādes brīdī
  componentDidMount() {
    acquire(`school/${this.state.schoolId}/teachers/new`)
      .then((res) => {
        this.setState({ authorized: true, loaded: true });
      })
      .catch((err) => {
        this.setState({ authorized: false, loaded: true });
      });
  }

  render() {
    // Ja dati nav ielādēti, tad tiek attēlota "Spin" komponente
    if (!this.state.loaded) {
      return (
        <Layout className="new-edit-student-content">
          <Content>
            <div className="edit-student-form-div">
              <Spin />
            </div>
          </Content>
        </Layout>
      );
    }
    // Ja lietotājs nav autorizēts redzēt konkrēto resursu, tad tiek brīdināts ar "Alert" komponenti
    if (!this.state.authorized) {
      return (
        <Layout className="student-list-layout">
          <Content>
            <Alert
              message="Nav piekļuves"
              description="Jums nav piekļuves šiem datiem vai funkcionalitātei!"
              type="error"
              showIcon
            />
          </Content>
        </Layout>
      );
    }
    return (
      <Layout className="new-edit-student-content">
        <Content>
          <div className="new-student-form-div">
            <Form
              labelCol={{ span: 6 }}
              wrapperCol={{ span: 24 }}
              onFinish={this.sendTeacherData}
              className="new-student-form"
            >
              <Button
                className="new-student-button"
                onClick={() => {
                  this.setState({ newTeacherUser: !this.state.newTeacherUser });
                }}
              >
                {this.renderAccountButton()}
              </Button>
              {this.renderFormFields()}
              <Form.Item>
                <Button
                  className="new-student-button"
                  htmlType="submit"
                  type="primary"
                >
                  Izveidot skolotāja ierakstu
                </Button>
              </Form.Item>
            </Form>
          </div>
        </Content>
      </Layout>
    );
  }
}

export default withRouter(NewSchoolTeacher);
