import {
  Layout,
  Form,
  Input,
  Button,
  message,
  Alert,
  Spin,
  Modal,
  Select,
} from "antd";
import { Component } from "react";
import { acquire, edit, remove } from "../lib/api";
import { withRouter } from "react-router-dom";

const { Content } = Layout;
const { Option } = Select;

class StudentProfileEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newStudentUser: false,
      authorized: false,
      student: {},
      studentId: this.props.match.params.id,
      passwordChange: false,
    };
  }

  // Modālā loga parādīšana vai paslēpšana
  setModalVisible = (value) => {
    this.setState({ modalVisible: value });
  };

  // Datu nosūtīšana uz serveri
  editUser = (values) => {
    edit(`students/${this.state.studentId}`, values)
      .then((res) => {
        message.success("Skolēna ieraksts saglabāts!");
        this.props.history.push(`/class/${res.data.classId}/students`);
      })
      .catch((err) => {
        if(err.response.status !== 422) {
          message.error("Sistēmas kļūda! Pamēģiniet vēlreiz!");
        }
        else {
          message.error("Lietotājvārds jau aizņemts!");
        }
      });
  };

  // Dzēšanas pieprasījuma nosūtīšana
  deleteProfile = () => {
    remove(`students/${this.state.studentId}`)
      .then((res) => {
        if (res.status === 200) {
          message.success("Skolēna profils dzēsts");
          this.props.history.push(`/class/${res.data.classId}/students`);
        }
      })
      .catch((err) => {
        message.error("Sistēmas kļūda! Pamēģiniet vēlreiz!");
      });
  };

  // Klašu datu ievietošana iekš "Select" komponentes
  renderClassOptions = () => {
    return this.state.classes.map((c, index) => {
      return <Option value={c.id}>{c.name}</Option>;
    });
  };

  // Attēlo paroles laukus, ja lietotājs ir nospiedis attiecīgo pogu
  renderPasswordFields = () => {
    return (
      <>
        <Form.Item
          name="password"
          label="Parole"
          rules={[
            {
              required: true,
              message: "Ievadiet lietotāja paroli!",
            },
          ]}
          hasFeedback
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          name="confirm_password"
          label="Atkārtot paroli"
          dependencies={["password"]}
          hasFeedback
          rules={[
            {
              required: true,
              message: "Apstipriniet paroli!",
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue("password") === value) {
                  return Promise.resolve();
                }

                return Promise.reject(
                  new Error("Ievadītās paroles nav vienādas!")
                );
              },
            }),
          ]}
        >
          <Input.Password />
        </Form.Item>
      </>
    );
  };

  // Komponentes ielādes brīdī tiek iegūti jau esošie dati par kontu
  componentDidMount() {
    acquire(`students/${this.state.studentId}/edit`)
      .then((res) => {
        this.setState({
          authorized: true,
          student: res.data.data,
        });

        acquire(`school/${this.state.student.school_id}/studentClasses`)
          .then((res) => {
            this.setState({
              loaded: true,
              classes: res.data.data,
            });
          })
          .catch((err) => {
            this.setState({ authorized: false, loaded: true });
          });
      })
      .catch((err) => {
        this.setState({ authorized: false, loaded: true });
      });
  }

  render() {
    // Ja dati vēl nav ielādēti, tad tiek attēlots "Spin" ielādes elements
    if (!this.state.loaded) {
      return (
        <Layout className="new-student-content">
          <Content>
            <div className="new-student-form-div">
              <Spin />
            </div>
          </Content>
        </Layout>
      );
    }
    // Ja lietotājs nav autorizēts redzēt konkrēto saturu, tad par to tiek brīdināts ar "Alert" elementu
    if (!this.state.authorized) {
      return (
        <Layout className="school-list-layout">
          <Content>
            <Alert
              message="Nav piekļuves"
              description="Jums nav piekļuves šiem datiem vai funkcionalitātei!"
              type="error"
              showIcon
            />
          </Content>
        </Layout>
      );
    }
    return (
      <Layout className="new-edit-room-content">
        <Content>
          <Modal
            title="Vai dzēst skolēna skolas profilu?"
            onCancel={() => this.setModalVisible(false)}
            centered
            visible={this.state.modalVisible}
            footer={[
              <Button onClick={() => this.setModalVisible(false)}>
                Atcelt
              </Button>,
              <Button type="primary" danger onClick={this.deleteProfile}>
                Dzēst
              </Button>,
            ]}
          >
            Šo darbību nevar atsaukt! Tiks dzēsti arī ar šo profilu saistītie
            dati! Šīs darbības rezultātā netiks dzēsts lietotāja konts.
          </Modal>
          <div className="edit-room-form-div">
            <Form
              labelCol={{ span: 9 }}
              wrapperCol={{ span: 24 }}
              onFinish={this.editUser}
              className="edit-school-form"
              initialValues={{
                username: this.state.student.user.username,
                name: this.state.student.user.name,
                surname: this.state.student.user.surname,
                className: this.state.student.class_id,
              }}
            >
              <Form.Item
                name="username"
                label="Lietotājvārds"
                rules={[
                  { required: true, message: "Ievadiet lietotājvārdu!" },
                  {
                    pattern: "^[a-z0-9_]{5,255}$",
                    message:
                      "Lietotājvārds nesatur atstarpes, lielos burtus un ir vismaz 5 simbolus garš!",
                  },
                ]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                name="name"
                label="Vārds"
                rules={[{ required: true, message: "Ievadiet skolēna vārdu!" }]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                name="surname"
                label="Uzvārds"
                rules={[
                  { required: true, message: "Ievadiet skolēna uzvārdu!" },
                ]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                name="className"
                label="Klase"
                rules={[
                  { required: true, message: "Izvēlieties skolēna klasi!" },
                ]}
              >
                <Select>{this.renderClassOptions()}</Select>
              </Form.Item>
              {this.state.passwordChange ? this.renderPasswordFields() : null}
              <Form.Item>
                <Button
                  className="edit-room-button"
                  onClick={() => {
                    this.setState({
                      passwordChange: !this.state.passwordChange,
                    });
                  }}
                >
                  {this.state.passwordChange
                    ? "Nemainīt paroli"
                    : "Mainīt paroli"}
                </Button>
              </Form.Item>
              <Form.Item>
                <Button
                  className="edit-room-button"
                  htmlType="submit"
                  type="primary"
                >
                  Rediģēt skolēna profila datus
                </Button>
              </Form.Item>
              <Button
                className="delete-room-button"
                danger
                type="primary"
                onClick={() => this.setModalVisible(true)}
              >
                Dzēst skolēna profilu
              </Button>
            </Form>
          </div>
        </Content>
      </Layout>
    );
  }
}

export default withRouter(StudentProfileEdit);
