import {
  Layout,
  Form,
  Input,
  Button,
  Spin,
  message,
  Alert,
  Select,
  InputNumber,
  DatePicker,
} from "antd";
import { Component } from "react";
import { send } from "../lib/api";
import { withRouter } from "react-router-dom";
import { acquire } from "../lib/api";

const { Content } = Layout;
const { Option } = Select;

class NewConsultation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      authorized: false,
      schoolId: this.props.match.params.id,
      loaded: false,
      teachers: [],
      teacherPick: false,
      rooms: [],
    };
  }

  // Konsultācijas datu nosūtīšana uz serveri
  sendConsultationData = (values) => {
    send(`school/${this.state.schoolId}/consultation`, values)
      .then((res) => {
        message.success("Konsultācijas ieraksts saglabāts!");
        this.props.history.push(`/school/${this.state.schoolId}/consultations`);
      })
      .catch((err) => {
        message.error("Sistēmas kļūda! Pamēģiniet vēlreiz.");
      });
  };

  // Telpu datu attēlošana iekš "Select" komponentes
  renderRoomData = () => {
    return this.state.rooms.map((room) => {
      return (
        <Option value={room.id}>
          {room.name + " [" + room.seat_count + "]"}
        </Option>
      );
    });
  };

  // Skolotāju datu attēlošana iekš "Select" komponentes
  renderTeacherData = () => {
    return this.state.teachers.map((teacher) => {
      return (
        <Option value={teacher.id}>
          {teacher.user.name + " " + teacher.user.surname}
        </Option>
      );
    });
  };

  // Komponentes ielādes brīdī tiek pārbaudītas lietotāja tiesības un
  // iegūti dati par telpām un skolotājiem. 
  componentDidMount() {
    acquire(`school/${this.state.schoolId}/consultations/new`)
      .then((res) => {
        this.setState({ teacherPick: res.data.pick_teacher });

        acquire(`school/${this.state.schoolId}/consultation/rooms`)
          .then((res) => {
            this.setState({ rooms: res.data.data });

            if (this.state.teacherPick) {
              acquire(`school/${this.state.schoolId}/consultation/teachers`)
                .then((res) => {
                  this.setState({
                    teachers: res.data.data,
                    authorized: true,
                    loaded: true,
                  });
                })
                .catch((err) => {
                  this.setState({ authorized: false, loaded: true });
                });
            } else {
              this.setState({ authorized: true, loaded: true });
            }
          })
          .catch((err) => {
            this.setState({ authorized: false, loaded: true });
          });
      })
      .catch((err) => {
        this.setState({ authorized: false, loaded: true });
      });
  }

  render() {
    // Ja dati nav ielādēti, tad tiek attēlots "Spin" elements
    if (!this.state.loaded) {
      return (
        <Layout className="new-edit-class-content">
          <Content>
            <div className="edit-class-form-div">
              <Spin />
            </div>
          </Content>
        </Layout>
      );
    }
    // Ja lietotājam nav tiesības redzēt konkrēto resursu, tad tiek attēlots "Alert" brīdinājums
    if (!this.state.authorized) {
      return (
        <Layout className="class-list-layout">
          <Content>
            <Alert
              message="Nav piekļuves"
              description="Jums nav piekļuves šiem datiem vai funkcionalitātei!"
              type="error"
              showIcon
            />
          </Content>
        </Layout>
      );
    }
    return (
      <Layout className="new-edit-class-content">
        <Content>
          <div className="new-class-form-div">
            <Form
              labelCol={{ span: 6 }}
              wrapperCol={{ span: 24 }}
              onFinish={this.sendConsultationData}
              className="new-room-form"
            >
              {this.state.teacherPick ? (
                <Form.Item
                  name="teacher"
                  label="Skolotājs"
                  rules={[
                    {
                      required: true,
                      message: "Izvēlieties skolotāju!",
                    },
                  ]}
                >
                  <Select
                    showSearch
                    placeholder="Izvēlieties skolotāju"
                    optionFilterProp="children"
                    filterOption={(input, option) =>
                      option.children
                        .toLowerCase()
                        .indexOf(input.toLowerCase()) >= 0
                    }
                  >
                    {this.renderTeacherData()}
                  </Select>
                </Form.Item>
              ) : null}

              <Form.Item
                name="topic"
                label="Konsultācijas tēma"
                rules={[
                  { required: true, message: "Ievadiet konsultācijas tēmu!" },
                ]}
              >
                <Input placeholder="Tēma" />
              </Form.Item>
              <Form.Item
                name="room"
                label="Telpa"
                rules={[
                  {
                    required: true,
                    message:
                      "Izvēlieties telpu, kurā norisināsies konsultācija!",
                  },
                ]}
              >
                <Select
                  showSearch
                  placeholder="Izvēlieties kabinetu"
                  optionFilterProp="children"
                  filterOption={(input, option) =>
                    option.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {this.renderRoomData()}
                </Select>
              </Form.Item>
              <Form.Item
                name="available_seats"
                label="Pieejamo vietu skaits"
                rules={[
                  {
                    type: "number",
                    min: 1,
                    max: 10000,
                    message:
                      "Pieejamo vietu skaitam jābūt robežās no 1 līdz 10000!",
                  },
                  {
                    message: "Norādiet pieejamo vietu skaitu!",
                    required: true,
                  },
                ]}
              >
                <InputNumber />
              </Form.Item>
              <Form.Item
                name="start_date"
                label="Datums un laiks"
                rules={[
                  {
                    required: true,
                    message: "Norādiet konsultācijas datumu!",
                  },
                ]}
              >
                <DatePicker
                  placeholder="Sākuma laiks"
                  showTime
                  format="DD-MM-YYYY HH:mm"
                />
              </Form.Item>
              <Form.Item>
                <Button
                  className="new-class-button"
                  htmlType="submit"
                  type="primary"
                >
                  Izveidot konsultācijas ierakstu
                </Button>
              </Form.Item>
            </Form>
          </div>
        </Content>
      </Layout>
    );
  }
}

export default withRouter(NewConsultation);
