import {
  Layout,
  Form,
  Input,
  Button,
  message,
  Alert,
  Spin,
  Modal,
} from "antd";
import { Component } from "react";
import { acquire, edit, remove } from "../lib/api";
import { withRouter } from "react-router-dom";

const { Content } = Layout;

class SchoolAdminProfileEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newStudentUser: false,
      authorized: false,
      admin: {},
      adminId: this.props.match.params.id,
      passwordChange: false,
    };
  }

  // Modālā loga parādīšana vai paslēpšana
  setModalVisible = (value) => {
    this.setState({ modalVisible: value });
  };

  // Datu nosūtīšana uz serveri
  editUser = (values) => {
    edit(`admins/${this.state.adminId}`, values)
      .then((res) => {
        message.success("Skolas administratora ieraksts saglabāts!");
        this.props.history.push(
          `/school/${this.state.admin.school_id}/admins`
        );
      })
      .catch((err) => {
        if(err.response.status !== 422) {
          message.error("Sistēmas kļūda! Pamēģiniet vēlreiz!");
        }
        else {
          message.error("Lietotājvārds jau aizņemts!");
        }
      });
  };

  // Dzēšanas pieprasījuma nosūtīšana
  deleteProfile = () => {
    remove(`admins/${this.state.adminId}`)
      .then((res) => {
        if (res.status === 200) {
          message.success("Administratora profils dzēsts");
          this.props.history.push(`/school/${res.data.schoolId}/admins`);
        }
      })
      .catch((err) => {
        message.error("Sistēmas kļūda! Pamēģiniet vēlreiz!");
      });
  };

  // Attēlo paroles laukus, ja lietotājs ir nospiedis attiecīgo pogu
  renderPasswordFields = () => {
    return (
      <>
        <Form.Item
          name="password"
          label="Parole"
          rules={[
            {
              required: true,
              message: "Ievadiet lietotāja paroli!",
            },
          ]}
          hasFeedback
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          name="confirm_password"
          label="Atkārtot paroli"
          dependencies={["password"]}
          hasFeedback

          // Ant design piemērs kā izveidot paroles lauka validāciju
          rules={[
            {
              required: true,
              message: "Apstipriniet paroli!",
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue("password") === value) {
                  return Promise.resolve();
                }

                return Promise.reject(
                  new Error("Ievadītās paroles nav vienādas!")
                );
              },
            }),
          ]}
        >
          <Input.Password />
        </Form.Item>
      </>
    );
  };

  // Komponentes ielādes brīdī tiek iegūti jau esošie dati par kontu
  componentDidMount() {
    acquire(`admins/${this.state.adminId}/edit`)
      .then((res) => {
        this.setState({
          authorized: true,
          loaded: true,
          admin: res.data.data,
        });
      })
      .catch((err) => {
        this.setState({ authorized: false, loaded: true });
      });
  }

  render() {
    // Ja dati vēl nav ielādēti, tad tiek attēlots "Spin" ielādes elements
    if (!this.state.loaded) {
      return (
        <Layout className="new-student-content">
          <Content>
            <div className="new-student-form-div">
              <Spin />
            </div>
          </Content>
        </Layout>
      );
    }
    // Ja lietotājs nav autorizēts redzēt konkrēto saturu, tad par to tiek brīdināts ar "Alert" elementu
    if (!this.state.authorized) {
      return (
        <Layout className="school-list-layout">
          <Content>
            <Alert
              message="Nav piekļuves"
              description="Jums nav piekļuves šiem datiem vai funkcionalitātei!"
              type="error"
              showIcon
            />
          </Content>
        </Layout>
      );
    }
    return (
      <Layout className="new-edit-room-content">
        <Content>
          <Modal
            title="Vai dzēst skolas administratora profilu?"
            onCancel={() => this.setModalVisible(false)}
            centered
            visible={this.state.modalVisible}
            footer={[
              <Button onClick={() => this.setModalVisible(false)}>
                Atcelt
              </Button>,
              <Button type="primary" danger onClick={this.deleteProfile}>
                Dzēst
              </Button>,
            ]}
          >
            Šo darbību nevar atsaukt! Tiks dzēsti arī ar šo profilu saistītie
            dati! Šīs darbības rezultātā netiks dzēsts lietotāja konts.
          </Modal>
          <div className="edit-room-form-div">
            <Form
              labelCol={{ span: 9 }}
              wrapperCol={{ span: 24 }}
              onFinish={this.editUser}
              className="edit-school-form"
              initialValues={{
                username: this.state.admin.user.username,
                name: this.state.admin.user.name,
                surname: this.state.admin.user.surname,
              }}
            >
              <Form.Item
                name="username"
                label="Lietotājvārds"
                rules={[
                  { required: true, message: "Ievadiet lietotājvārdu!" },
                  {
                    pattern: "^[a-z0-9_]{5,255}$",
                    message:
                      "Lietotājvārds nesatur atstarpes, lielos burtus un ir vismaz 5 simbolus garš!",
                  },
                ]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                name="name"
                label="Vārds"
                rules={[{ required: true, message: "Ievadiet administratora vārdu!" }]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                name="surname"
                label="Uzvārds"
                rules={[
                  { required: true, message: "Ievadiet administratora uzvārdu!" },
                ]}
              >
                <Input />
              </Form.Item>

              {this.state.passwordChange ? this.renderPasswordFields() : null}
              <Form.Item>
                <Button
                  className="edit-room-button"
                  onClick={() => {
                    this.setState({
                      passwordChange: !this.state.passwordChange,
                    });
                  }}
                >
                  {this.state.passwordChange
                    ? "Nemainīt paroli"
                    : "Mainīt paroli"}
                </Button>
              </Form.Item>
              <Form.Item>
                <Button
                  className="edit-room-button"
                  htmlType="submit"
                  type="primary"
                >
                  Rediģēt administratora profila datus
                </Button>
              </Form.Item>
              <Button
                className="delete-room-button"
                danger
                type="primary"
                onClick={() => this.setModalVisible(true)}
              >
                Dzēst administratora profilu
              </Button>
            </Form>
          </div>
        </Content>
      </Layout>
    );
  }
}

export default withRouter(SchoolAdminProfileEdit);
