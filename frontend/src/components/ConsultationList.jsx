import {
  Layout,
  Button,
  Form,
  Input,
  message,
  Collapse,
  Spin,
  Alert,
  Typography,
  Pagination,
} from "antd";
import { Component } from "react";
import {
  PlusOutlined,
  SearchOutlined,
  FilterOutlined,
} from "@ant-design/icons";
import { acquire, send } from "../lib/api";
import { withRouter, Link } from "react-router-dom";
import moment from "moment";

const { Content } = Layout;
const { Panel } = Collapse;
const { Text } = Typography;

class ConsultationList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      consultations: [],
      loaded: false,
      authorized: false,
      current: 1,
      total: null,
      searched: null,
      schoolId: this.props.match.params.id,
      access: null,
      allConsultations: 0,
    };
  }

  // Lapas mainīšanas funkcija
  pageChange = (page) => {
    this.setState({ current: page }, () => {
      this.getConsultationData();
    });
  };

  // Konsultāciju saraksta attēlošana
  renderConsultationData = () => {
    if (this.state.consultations.length !== 0) {
      return this.state.consultations.map((consultation) => {
        return (
          <Collapse key={consultation.id} className="school-collapse-item">
            <Panel
              header={
                <>
                  <Text>{`${consultation.teacher.user.name} ${consultation.teacher.user.surname}`}</Text>
                  <br />
                  <Text strong>{` [${consultation.topic}]`}</Text>
                </>
              }
            >
              <div className="consultation-additional-info">
                <span>
                  <Text strong>Telpa:</Text> {consultation.room.name}
                </span>
                <span>
                  <Text strong>Vietu skaits:</Text>{" "}
                  {consultation.attendant_count}
                </span>
                <span>
                  <Text strong>Datums un laiks:</Text>{" "}
                  {moment(consultation.date).format("DD.MM.YYYY HH:mm")}
                </span>
              </div>
              <div className="school-config-buttons">
                <Button>
                  <Link to={`/consultation/${consultation.id}/appointments`}>
                    Apskatīt apmeklējumu pieteikumus
                  </Link>
                </Button>
                {this.state.access == "personal" ? (
                  consultation.edit ? (
                    <Button>
                      <Link to={`/consultation/${consultation.id}/edit`}>
                        Rediģēt konsultāciju ierakstu
                      </Link>
                    </Button>
                  ) : null
                ) : this.state.access == "all" ? (
                  <Button>
                    <Link to={`/consultation/${consultation.id}/edit`}>
                      Rediģēt konsultāciju ierakstu
                    </Link>
                  </Button>
                ) : null}
              </div>
            </Panel>
          </Collapse>
        );
      });
    } else {
      return (
        <Text className="schools-not-found-text">
          Nav atrasta neviena konsultācija
        </Text>
      );
    }
  };

  // Konsultāciju datu ielāde. Tiek veikta pārbaude, vai lietotājs neatrodas meklēšanas stāvoklī
  getConsultationData = () => {
    if (this.state.searched) {
      this.searchConsultation(this.state.searched, false);
    } else {
      acquire(`school/${this.state.schoolId}/consultations`, {
        page: this.state.current,
        allConsultations: this.state.allConsultations,
      })
        .then((res) => {
          this.setState({
            consultations: res.data.data.data,
            total: res.data.data.total,
            loaded: true,
            authorized: true,
            access: res.data.access,
          });
        })
        .catch((err) => {
          if (err.response.status !== 403) {
            message.error("Sistēmas kļūda! Pamēģiniet vēlreiz.");
          }
          this.setState({ loaded: true });
        });
    }
  };

  // Konsultācijas meklēšana. Dati no nodotās formas tiek pievienoti vaicājumam uz serveri.
  // Tiek veikta pārbaude, vai lietotājs maina tikai lapu vai arī veic jaunu meklēšanu (resetToOne).
  searchConsultation = (data, resetToOne = true) => {
    if (!data?.consultation_name) {
      this.setState({ searched: null });
      this.getConsultationData();
    } else {
      if (resetToOne) {
        this.setState({ current: 1 });
      }
      this.setState({ searched: data });
      send(`school/${this.state.schoolId}/consultations/search`, {
        ...data,
        page: this.state.current,
      })
        .then((res) => {
          this.setState({
            consultations: res.data.data.data,
            total: res.data.data.total,
          });
        })
        .catch((err) => {
          message.error("Sistēmas kļūda! Pamēģiniet vēlreiz.");
        });
    }
  };

  // Komponentes ielādes brīdī tiek veikta attiecīgās konsultācijas datu ielāde
  componentDidMount() {
    this.getConsultationData();
  }

  render() {
    // Ja komponente ir beigusi datu ielādes procesu un lietotājs nav autorizēts, tad par to tiek brīdināts
    if (!this.state.authorized && this.state.loaded) {
      return (
        <Layout className="school-list-layout">
          <Content>
            <Alert
              message="Nav piekļuves"
              description="Jums nav piekļuves šiem datiem vai funkcionalitātei!"
              type="error"
              showIcon
            />
          </Content>
        </Layout>
      );
    } else {
      return (
        <Layout className="school-list-layout">
          <Content>
            <div className="consultation-list-head">
              <Button
                disabled={
                  ["all", "personal"].includes(this.state.access) ? false : true
                }
                type="primary"
                onClick={() =>
                  this.props.history.push(
                    `/school/${this.state.schoolId}/consultations/new`
                  )
                }
              >
                Izveidot jaunu konsultāciju <PlusOutlined />
              </Button>
              {/* Poga, kas ļauj lietotājam izvēlēties, vai rādīt tikai nākotnes un šodienas ierakstus vai visus*/}
              <Button
                disabled={this.state.searched ? true : false}
                type="primary"
                onClick={() => {
                  this.setState(
                    {
                      allConsultations: this.state.allConsultations ? 0 : 1,
                    },
                    () => this.getConsultationData()
                  );
                }}
              >
                {this.state.allConsultations
                  ? "Rādīt nākotnes konsultācijas"
                  : "Rādīt visas konsultācijas"}
                <FilterOutlined />
              </Button>
              <Form layout="inline" onFinish={this.searchConsultation}>
                <Form.Item name="consultation_name">
                  <Input placeholder="Meklēt konsultāciju" />
                </Form.Item>
                <Form.Item>
                  <Button type="primary" htmlType="submit">
                    <SearchOutlined />
                  </Button>
                </Form.Item>
              </Form>
            </div>
            {this.state.loaded ? (
              <>
                <div className="school-list-body">
                  {this.renderConsultationData()}
                </div>
                <Pagination
                  current={this.state.current}
                  onChange={this.pageChange}
                  total={this.state.total}
                  defaultPageSize={8}
                />
              </>
            ) : (
              <div className="school-list-spin-div">
                <Spin />
              </div>
            )}
          </Content>
        </Layout>
      );
    }
  }
}

export default withRouter(ConsultationList);
