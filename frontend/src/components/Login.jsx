import { Component } from "react";
import { Layout, Typography, message, Form, Input, Button } from "antd";
import "../css/style.css";
import { login, send } from "../lib/api";
import { isLoggedIn, setLoginCookie } from "../lib/auth";
import { withRouter, Redirect } from "react-router-dom";

const { Content } = Layout;
const { Link } = Typography;

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      forgot: false,
    };
  }

  // Pieslēgšanāš datu nosūtīšana uz serveri
  sendLogin = (values) => {
    login("sanctum/csrf-cookie", values)
      .then((res) => {
        send("login", values)
          .then((res) => {
            setLoginCookie();
            this.props.history.push("/profiles");
          })
          .catch((err) => {
            this.displayLoginFailed();
          });
      })
      .catch((err) => {
        this.displayLoginError();
      });
  };

  // Kļūdu paziņojumu attēlošana
  displayLoginFailed = () => {
    message.error("Lietotājvārds vai parole nav pareiza!");
  };

  displayLoginError = () => {
    message.error("Notikusi sistēmas kļūda. Lūdzu mēģiniet vēlreiz!");
  };

  render() {
    if (isLoggedIn()) {
      return <Redirect to={this.props.location.pathname} />;
    }
    return (
      <Layout>
        <Content className="login-screen-content">
          <div className="login-screen-div">
            <Form
              layout="vertical"
              name="basic"
              autoComplete="off"
              onFinish={this.sendLogin}
            >
              <Form.Item
                label="Lietotājvārds"
                name="username"
                rules={[
                  {
                    required: true,
                    message: "Lūdzu ievadiet lietotājvārdu!",
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                label="Parole"
                name="password"
                rules={[
                  {
                    required: true,
                    message: "Lūdzu ievadiet paroli!",
                  },
                ]}
              >
                <Input.Password />
              </Form.Item>

              <Form.Item>
                <Button
                  type="primary"
                  className="login-button"
                  htmlType="submit"
                >
                  Pieslēgties
                </Button>
              </Form.Item>
            </Form>
          </div>
        </Content>
      </Layout>
    );
  }
}

export default withRouter(Login);
