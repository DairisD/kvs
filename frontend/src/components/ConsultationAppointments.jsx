import { Component } from "react";
import { withRouter } from "react-router-dom";
import {
  Layout,
  Spin,
  Pagination,
  Button,
  Input,
  Form,
  message,
  Collapse,
  Typography,
  Alert,
  Modal,
} from "antd";
import {
  PlusOutlined,
  SearchOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
import { acquire, send, remove } from "../lib/api";

const { Content } = Layout;
const { Panel } = Collapse;
const { Text } = Typography;

class ConsultationAppointments extends Component {
  constructor(props) {
    super(props);
    this.state = {
      appointments: [],
      loaded: false,
      authorized: false,
      current: 1,
      total: null,
      searched: null,
      modalVisible: false,
      consultationId: this.props.match.params.id,
      deleteable: null,
      alreadyExists: false,
    };
  }

  // Funkcija, kas nodrošina pārvietošanos pa lapām un secīgu datu ielādi
  pageChange = (page) => {
    this.setState({ current: page }, () => {
      this.getAppointmentsData();
    });
  };

  // Pieteikumu datu attēlošana sarakstā
  renderAppointmentsData = () => {
    if (this.state.appointments.length !== 0) {
      return this.state.appointments.map((a) => {
        return (
          <Collapse key={a.id} className="room-collapse-item">
            <Panel
              header={[
                <Text strong>{`${a.student.user?.name} ${
                  a.student.user?.surname
                } [${a.student.class?.name ?? "nav klases"}]`}</Text>,
              ]}
            >
              <div className="room-config-buttons">
                <Text strong>Pieteikuma iemesls: </Text>
                <Text>{a.reason} </Text>
                {this.state.delete == "personal" ? (
                  a.edit ? (
                    <Button
                      type="danger"
                      className="delete-appointment-button"
                      onClick={() => {
                        this.setModalVisible(true);
                        this.setState({ deleteable: a.id });
                      }}
                    >
                      Dzēst pieteikumu <DeleteOutlined />
                    </Button>
                  ) : null
                ) : (
                  <Button
                    type="danger"
                    className="delete-appointment-button"
                    onClick={() => {
                      this.setModalVisible(true);
                      this.setState({ deleteable: a.id });
                    }}
                  >
                    Dzēst pieteikumu <DeleteOutlined />
                  </Button>
                )}
              </div>
            </Panel>
          </Collapse>
        );
      });
    } else {
      return (
        <Text className="schools-not-found-text">
          Nav atrasts neviens pieteikums
        </Text>
      );
    }
  };

  // Datu ieguve par pieteikumiem. Tiek ņemta vērā pašreizējā lapa. Lapas numurs tiek pievienots vaicājumā uz serveri.
  getAppointmentsData = () => {
    if (this.state.searched) {
      this.searchAppointment(this.state.searched, false);
    } else {
      acquire(`consultation/${this.state.consultationId}/appointments`, {
        page: this.state.current,
      })
        .then((res) => {
          if (res.status === 200) {
            this.setState({
              appointments: res.data.data.data,
              total: res.data.data.total,
              loaded: true,
              authorized: true,
              delete: res.data.delete,
              alreadyExists: res.data.appointment_exists,
            });
          }
        })
        .catch((err) => {
          if (err.status !== 403) {
            message.error("Sistēmas kļūda! Pamēģiniet vēlreiz.");
          }
          this.setState({ loaded: true });
        });
    }
  };

  // Pieteikuma dzēšana
  deleteAppointment = (id) => {
    remove(`appointment/${id}`)
      .then((res) => {
        if (res.status === 200) {
          message.success("Pieteikums dzēsts!");
          this.getAppointmentsData();
          this.setState({ deleteable: null, modalVisible: false });
        }
      })
      .catch((err) => {
        message.error("Sistēmas kļūda! Pamēģiniet vēlreiz!");
      });
  };

  // Modālā loga atvēršanas / aizvēršanas funkcija
  setModalVisible = (value) => {
    this.setState({ modalVisible: value });
  };

  // Pieteikuma meklēšana. Dati no nodotās formas tiek pievienoti vaicājumam uz serveri.
  // Tiek veikta pārbaude, vai lietotājs maina tikai lapu vai arī veic jaunu meklēšanu (resetToOne).
  searchAppointment = (data, resetToOne = true) => {
    if (!data?.appointment_name) {
      this.setState({ searched: null });
      this.getAppointmentsData();
    } else {
      if (resetToOne) {
        this.setState({ current: 1 });
      }
      this.setState({ searched: data });
      send(`consultation/${this.state.consultationId}/appointments/search`, {
        ...data,
        page: this.state.current,
      })
        .then((res) => {
          if (res.status === 200) {
            this.setState({
              appointments: res.data.data.data,
              total: res.data.data.total,
            });
          }
        })
        .catch((err) => {
          message.error("Sistēmas kļūda! Pamēģiniet vēlreiz.");
        });
    }
  };

  // Komponenetes ielādes laikā tiek iegūti dati par pieteikumiem
  componentDidMount() {
    this.getAppointmentsData();
  }

  render() {
    // Ja lietotājs nav autorizēts redzēt konkrēto resursu, tad tiek attēlots paziņojums par to
    if (!this.state.authorized && this.state.loaded) {
      return (
        <Layout className="class-list-layout">
          <Content>
            <Alert
              message="Nav piekļuves"
              description="Jums nav piekļuves šiem datiem vai funkcionalitātei!"
              type="error"
              showIcon
            />
          </Content>
        </Layout>
      );
    } else {
      return (
        <Layout className="class-list-layout">
          <Content>
            <Modal
              title="Vai dzēst konsultācijas pierakstu?"
              onCancel={() => this.setModalVisible(false)}
              centered
              visible={this.state.modalVisible}
              footer={[
                <Button
                  onClick={() => {
                    this.setModalVisible(false);
                    this.setState({ deleteable: null });
                  }}
                >
                  Atcelt
                </Button>,
                <Button
                  type="primary"
                  danger
                  onClick={() => this.deleteAppointment(this.state.deleteable)}
                >
                  Dzēst
                </Button>,
              ]}
            >
              Šo darbību nevar atsaukt! Tiks dzēsts konsultācijas pieraksts!
            </Modal>
            <div className="class-list-head">
              <Button
                type="primary"
                disabled={this.state.alreadyExists}
                onClick={() =>
                  this.props.history.push(
                    `/consultation/${this.state.consultationId}/appointments/new`
                  )
                }
              >
                Pieteikties konsultācijai <PlusOutlined />
              </Button>
              <Form layout="inline" onFinish={this.searchAppointment}>
                <Form.Item name="appointment_name">
                  <Input placeholder="Meklēt pieteikumu" />
                </Form.Item>
                <Form.Item>
                  <Button type="primary" htmlType="submit">
                    <SearchOutlined />
                  </Button>
                </Form.Item>
              </Form>
            </div>
            {this.state.loaded ? (
              <>
                <div>{this.renderAppointmentsData()}</div>
                <Pagination
                  current={this.state.current}
                  onChange={this.pageChange}
                  total={this.state.total}
                  defaultPageSize={8}
                />
              </>
            ) : (
              <div className="class-list-spin-div">
                <Spin />
              </div>
            )}
          </Content>
        </Layout>
      );
    }
  }
}

export default withRouter(ConsultationAppointments);
