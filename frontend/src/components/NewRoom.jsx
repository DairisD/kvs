import {
  Layout,
  Form,
  Input,
  Button,
  Spin,
  message,
  Alert,
  InputNumber,
} from "antd";
import { Component } from "react";
import { send } from "../lib/api";
import { withRouter } from "react-router-dom";
import { acquire } from "../lib/api";

const { Content } = Layout;

class NewRoom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      authorized: false,
      school: this.props.match.params.id,
      loaded: false,
    };
  }

  // Telpas datu nosūtīšana uz serveri
  sendRoomData = (values) => {
    send(`school/${this.state.school}/rooms`, values)
      .then((res) => {
        message.success("Telpas ieraksts saglabāts!");
        this.props.history.push(
          `/school/${this.state.school}/rooms`
        );
      })
      .catch((err) => {
        if (err.response.status === 422 || err.response.status === 404) {
          message.error(err.response.data.message);
        } else {
          message.error("Sistēmas kļūda! Pamēģiniet vēlreiz!");
        }
      });
  };

  // Tiesību pārbaude komponentes ielādes brīdī
  componentDidMount() {
    acquire(`school/${this.state.school}/rooms/new`)
      .then((res) => {
        this.setState({ authorized: true, loaded: true });
      })
      .catch((err) => {
        this.setState({ authorized: false, loaded: true });
      });
  }

  render() {
    // Ja dati nav ielādēti, tad tiek attēlots "Spin" elements
    if (!this.state.loaded) {
      return (
        <Layout className="new-edit-room-content">
          <Content>
            <div className="edit-room-form-div">
              <Spin />
            </div>
          </Content>
        </Layout>
      );
    }
    // Ja lietotājam nav tiesības redzēt konkrēto resursu, tad tiek attēlots "Alert" brīdinājums
    if (!this.state.authorized) {
      return (
        <Layout className="room-list-layout">
          <Content>
            <Alert
              message="Nav piekļuves"
              description="Jums nav piekļuves šiem datiem vai funkcionalitātei!"
              type="error"
              showIcon
            />
          </Content>
        </Layout>
      );
    }
    return (
      <Layout className="new-edit-room-content">
        <Content>
          <div className="new-room-form-div">
            <Form
              labelCol={{ span: 6 }}
              wrapperCol={{ span: 24 }}
              onFinish={this.sendRoomData}
              className="new-room-form"
            >
              <Form.Item
                name="name"
                label="Telpas nosaukums"
                rules={[
                  { required: true, message: "Ievadiet telpas nosaukumu!" },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                name="room_size"
                label="Sēdvietu skaits"
                rules={[
                  {
                    type: "number",
                    min: 1,
                    max: 10000,
                    message: "Sēdvietu skaitam jābūt robežās no 1 līdz 10000!",
                  },
                  {
                    message: "Norādiet sēdvietu skaitu!",
                    required: true,
                  },
                ]}
              >
                <InputNumber />
              </Form.Item>
              <Form.Item>
                <Button
                  className="new-room-button"
                  htmlType="submit"
                  type="primary"
                >
                  Izveidot telpas ierakstu
                </Button>
              </Form.Item>
            </Form>
          </div>
        </Content>
      </Layout>
    );
  }
}

export default withRouter(NewRoom);
