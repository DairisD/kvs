import { Layout, Form, Input, Button, Spin, message, Alert } from "antd";
import { Component } from "react";
import { send } from "../lib/api";
import { withRouter } from "react-router-dom";
import { acquire } from "../lib/api";

const { Content } = Layout;

class NewClass extends Component {
  constructor(props) {
    super(props);
    this.state = {
      authorized: false,
      school: this.props.match.params.id,
      loaded: false,
    };
  }

  // Datu nosūtīšana serverim
  sendRoomData = (values) => {
    send(`school/${this.state.school}/classes`, values)
      .then((res) => {
        message.success("Klases ieraksts saglabāts!");
        this.props.history.push(`/school/${this.state.school}/classes`);
      })
      .catch((err) => {
        if (err.response.status === 422 || err.response.status === 404) {
          message.error(err.response.data.message);
        } else {
          message.error("Sistēmas kļūda! Pamēģiniet vēlreiz!");
        }
      });
  };

  // Tiek pārbaudītas tiesības, vai konkrētais lietotājs šajā skolā drīkst veidot jaunu klasi
  componentDidMount() {
    acquire(`school/${this.state.school}/classes/new`)
      .then((res) => {
        this.setState({ authorized: true, loaded: true });
      })
      .catch((err) => {
        this.setState({ authorized: false, loaded: true });
      });
  }

  render() {
    // Ja dati nav ielādēti, tad tiek attēlots "Spin" elements
    if (!this.state.loaded) {
      return (
        <Layout className="new-edit-class-content">
          <Content>
            <div className="edit-class-form-div">
              <Spin />
            </div>
          </Content>
        </Layout>
      );
    }
    // Ja lietotājam nav tiesības redzēt konkrēto resursu, tad tiek attēlots "Alert" brīdinājums
    if (!this.state.authorized) {
      return (
        <Layout className="class-list-layout">
          <Content>
            <Alert
              message="Nav piekļuves"
              description="Jums nav piekļuves šiem datiem vai funkcionalitātei!"
              type="error"
              showIcon
            />
          </Content>
        </Layout>
      );
    }
    return (
      <Layout className="new-edit-class-content">
        <Content>
          <div className="new-class-form-div">
            <Form
              labelCol={{ span: 6 }}
              wrapperCol={{ span: 24 }}
              onFinish={this.sendRoomData}
              className="new-room-form"
            >
              <Form.Item
                name="name"
                label="Klases nosaukums"
                rules={[
                  { required: true, message: "Ievadiet klases nosaukumu!" },
                ]}
              >
                <Input />
              </Form.Item>
              <Form.Item>
                <Button
                  className="new-class-button"
                  htmlType="submit"
                  type="primary"
                >
                  Izveidot klases ierakstu
                </Button>
              </Form.Item>
            </Form>
          </div>
        </Content>
      </Layout>
    );
  }
}

export default withRouter(NewClass);
