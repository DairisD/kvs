import { Component } from "react";
import { withRouter, Link } from "react-router-dom";
import {
  Layout,
  Spin,
  Pagination,
  Button,
  Input,
  Form,
  message,
  Collapse,
  Typography,
  Alert,
} from "antd";
import { PlusOutlined, SearchOutlined } from "@ant-design/icons";
import { acquire, send } from "../lib/api";

const { Content } = Layout;
const { Panel } = Collapse;
const { Text } = Typography;

class ClassList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      classes: [],
      loaded: false,
      authorized: false,
      current: 1,
      total: null,
      searched: null,
      schoolId: this.props.match.params.id,
    };
  }

  // Funkcija, kas nodrošina pārvietošanos pa lapām un secīgu datu ielādi
  pageChange = (page) => {
    this.setState({ current: page }, () => {
      this.getClassData();
    });
  };

  // Klašu datu attēlošana sarakstā
  renderClassData = () => {
    if (this.state.classes.length !== 0) {
      return this.state.classes.map((c) => {
        return (
          <Collapse key={c.id} className="room-collapse-item">
            <Panel header={[<Text strong>{c.name}</Text>]}>
              <div className="room-config-buttons">
                <Button>
                  <Link to={"/class/" + c.id + "/edit"}>
                    Pārvaldīt klases datus
                  </Link>
                </Button>
                <Button>
                  <Link to={"/class/" + c.id + "/students"}>
                    Pārvaldīt klases skolēnu datus
                  </Link>
                </Button>
              </div>
            </Panel>
          </Collapse>
        );
      });
    } else {
      return (
        <Text className="schools-not-found-text">
          Nav atrasta neviena klase
        </Text>
      );
    }
  };

  // Datu ieguve par klasēm. Tiek ņemta vērā pašreizējā lapa. Lapas numurs tiek pievienots vaicājumā uz serveri.
  getClassData = () => {
    if (this.state.searched) {
      this.searchRoom(this.state.searched, false);
    } else {
      acquire(`school/${this.state.schoolId}/classes`, {
        page: this.state.current,
      })
        .then((res) => {
          if (res.status === 200) {
            this.setState({
              classes: res.data.data.data,
              total: res.data.data.total,
              loaded: true,
              authorized: true,
            });
          }
        })
        .catch((err) => {
          if (err.response.status !== 403) {
            message.error("Sistēmas kļūda! Pamēģiniet vēlreiz.");
          }
          this.setState({ loaded: true });
        });
    }
  };

  // Klases meklēšana. Dati no nodotās formas tiek pievienoti vaicājumam uz serveri.
  // Tiek veikta pārbaude, vai lietotājs maina tikai lapu vai arī veic jaunu meklēšanu (resetToOne).
  searchClass = (data, resetToOne = true) => {
    if (!data?.class_name) {
      this.setState({ searched: null });
      this.getClassData();
    } else {
      if (resetToOne) {
        this.setState({ current: 1 });
      }
      this.setState({ searched: data });
      send(`school/${this.state.schoolId}/classes/search`, {
        ...data,
        page: this.state.current,
      })
        .then((res) => {
          if (res.status === 200) {
            this.setState({
              classes: res.data.data.data,
              total: res.data.data.total,
            });
          }
        })
        .catch((err) => {
          if (err.response.status === 500) {
            message.error("Sistēmas kļūda! Pamēģiniet vēlreiz.");
          }
        });
    }
  };

  // Komponentes ielādes laikā tiek iegūti dati par klasēm
  componentDidMount() {
    this.getClassData();
  }

  render() {
    // Ja lietotājs nav autorizēts redzēt konkrēto resursu, tad tiek attēlots paziņojums par to
    if (!this.state.authorized && this.state.loaded) {
      return (
        <Layout className="class-list-layout">
          <Content>
            <Alert
              message="Nav piekļuves"
              description="Jums nav piekļuves šiem datiem vai funkcionalitātei!"
              type="error"
              showIcon
            />
          </Content>
        </Layout>
      );
    } else {
      return (
        <Layout className="class-list-layout">
          <Content>
            <div className="class-list-head">
              <Button
                type="primary"
                onClick={() =>
                  this.props.history.push(
                    `/school/${this.state.schoolId}/classes/new`
                  )
                }
              >
                Izveidot jaunu klasi <PlusOutlined />
              </Button>
              <Form layout="inline" onFinish={this.searchClass}>
                <Form.Item name="class_name">
                  <Input placeholder="Meklēt klasi" />
                </Form.Item>
                <Form.Item>
                  <Button type="primary" htmlType="submit">
                    <SearchOutlined />
                  </Button>
                </Form.Item>
              </Form>
            </div>
            {this.state.loaded ? (
              <>
                <div>{this.renderClassData()}</div>
                <Pagination
                  current={this.state.current}
                  onChange={this.pageChange}
                  total={this.state.total}
                  defaultPageSize={8}
                />
              </>
            ) : (
              <div className="class-list-spin-div">
                <Spin />
              </div>
            )}
          </Content>
        </Layout>
      );
    }
  }
}

export default withRouter(ClassList);
