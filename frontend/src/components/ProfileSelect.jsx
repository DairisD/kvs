import { Layout, Typography, message, Spin } from "antd";
import { Component } from "react";
import { withRouter } from "react-router-dom";
import { UserOutlined } from "@ant-design/icons";
import { acquire, send } from "../lib/api";

const { Content } = Layout;
const { Text } = Typography;

// Konstante, kas ar " key -> value" principu attēlo servera atgriezto lomu nosaukumus
const roleNames = {
  teacher: "SKOLOTĀJS",
  system_admin: "SISTĒMAS ADMINISTRATORS",
  school_admin: "SKOLAS ADMINISTRATORS",
  student: "SKOLĒNS",
};

class ProfileSelect extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profiles: [],
      loaded: false,
    };
  }

  // Komponentes ielādes brīdī tiek iegūti dati par lietotājam esošajiem profiliem
  componentDidMount() {
    this.getProfileData();

    // "Callback" funkcija, kas visas sistēmas tvērumā saglabās lietotāja profila datus stāvoklī
    this.props.setUserCallback({
      profileId: null,
      permissions: [],
      role: null,
    });
  }

  // Profila datu iegūšanas funkcija
  getProfileData = () => {
    acquire("profiles")
      .then((res) => {
        if (res.status === 200) {
          this.setState({ profiles: res.data.data, loaded: true });
        }
      })
      .catch((err) => {
        message.error("Sistēmas kļūda! Pamēģiniet vēlreiz.");
      });
  };

  // Profilu attēlošanas funkcija
  renderProfiles = () => {
    var profileMap = this.state.profiles.map((profile) => {
      return (
        <div
          className="profile-select-entry"
          onClick={() => this.selectProfile(profile["id"])}
          key={profile["id"]}
        >
          <table>
            <tbody>
              <tr>
                <td>
                  <Text strong>Loma:</Text>
                </td>
                {profile.school_id ? (
                  <td>
                    <Text strong>Skola:</Text>
                  </td>
                ) : null}
                {profile.class_id ? (
                  <td>
                    <Text strong>Klase:</Text>
                  </td>
                ) : null}
              </tr>
              <tr>
                <td>
                  <Text>{roleNames[profile.role]}</Text>
                </td>
                {profile.school_id ? (
                  <td>
                    <Text>{profile.school.name}</Text>
                  </td>
                ) : null}
                {profile.class_id ? (
                  <td>
                    <Text>{profile.class.name}</Text>
                  </td>
                ) : null}
              </tr>
            </tbody>
          </table>
          <UserOutlined />
        </div>
      );
    });
    return profileMap;
  };

  // Funkcija, kas tiek izsaukta brīdī, kad lietotājs izvēlas profilu
  selectProfile = (profileId) => {
    send("profileSelect", { id: profileId })
      .then((res) => {
        if (res.status === 200) {
          // "Callback" funkcija, kas visas sistēmas tvērumā saglabā datus par izvēlēto profilu
          this.props.setUserCallback({
            ...this.props.user,
            profileId: profileId,
            permissions: res.data.data.permissions,
            role: res.data.data.role,
            school_id: res.data.data.school_id,
            class_id: res.data.data.class_id
          });
          message.success("Profils nomainīts!");
        } else if (res.status === 403) {
          message.error("Nav piekļuves!");
        }
      })
      .catch((err) => {
        if (err.response.status === 403) {
          message.error("Nav piekļuves");
        } else {
          message.error("Sistēmas kļūda! Pamēģiniet vēlreiz.");
        }
      });
  };

  render() {
    return (
      <Layout>
        <Content className="profile-select-content">
          <h1>Izvēlieties profilu</h1>
          {this.state.loaded ? this.renderProfiles() : <Spin />}
        </Content>
      </Layout>
    );
  }
}

export default withRouter(ProfileSelect);
