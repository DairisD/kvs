import { Layout, Form, Input, Button, message, Alert, Spin } from "antd";
import { Component } from "react";
import { send, acquire } from "../lib/api";
import { withRouter } from "react-router-dom";

const { Content } = Layout;

class NewClassStudent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newStudentUser: false,
      authorized: false,
      classId: this.props.match.params.id,
    };
  }

  // Nomaina saturu pogai, kura attēlo laukus atbilstoši lietotāja izvēlei par jauna konta izveidi
  renderAccountButton = () => {
    return this.state.newStudentUser
      ? "Lietot esošu kontu"
      : "Veidot jaunu skolēna kontu";
  };

  // Formas lauku attēlošana
  renderFormFields = () => {
    // Gadījums, kad tiek veidots jauns konts skolēnam
    return this.state.newStudentUser ? (
      <>
        <Form.Item
          name="username"
          label="Lietotājvārds"
          rules={[
            { required: true, message: "Ievadiet lietotājvārdu!" },
            {
              pattern: "^[a-z0-9_]{5,255}$",
              message:
                "Lietotājvārds nesatur atstarpes, lielos burtus un ir vismaz 5 simbolus garš!",
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          name="name"
          label="Vārds"
          rules={[{ required: true, message: "Ievadiet vārdu!" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="surname"
          label="Uzvārds"
          rules={[{ required: true, message: "Ievadiet uzvārdu!" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="password"
          label="Parole"
          rules={[
            {
              required: true,
              message: "Ievadiet lietotāja paroli!",
            },
          ]}
          hasFeedback
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          name="confirm_password"
          label="Atkārtot paroli"
          dependencies={["password"]}
          hasFeedback
          rules={[
            {
              required: true,
              message: "Apstipriniet paroli!",
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue("password") === value) {
                  return Promise.resolve();
                }

                return Promise.reject(
                  new Error("Ievadītās paroles nav vienādas!")
                );
              },
            }),
          ]}
        >
          <Input.Password />
        </Form.Item>
      </>
    ) : (
      // Gadījums, kad tiek veidots jauns profils lietotājam
      <Form.Item
        name="username"
        label="Lietotajvārds"
        rules={[{ required: true, message: "Ievadiet lietotājvārdu!" }]}
      >
        <Input />
      </Form.Item>
    );
  };

  // Skolēna datu nosūtīšana uz serveri
  sendStudentData = (values) => {
    send(`class/${this.state.classId}/students`, {
      ...values,
      newUser: this.state.newStudentUser,
      class: this.state.classId,
    })
      .then((res) => {
        if (res.status === 200) {
          message.success("Skolēna ieraksts saglabāts!");
          this.props.history.push(`/class/${this.state.classId}/students`);
        }
      })
      .catch((err) => {
        if(err.response.status !== 422) {
          message.error("Sistēmas kļūda! Pamēģiniet vēlreiz!");
        }
        else {
          message.error("Lietotājvārds jau aizņemts!");
        }
      });
  };

  // Tiesību pārbaude par jauna skolēna ieraksta izveidošanu
  componentDidMount() {
    acquire(`class/${this.state.classId}/students/new`)
      .then((res) => {
        this.setState({ authorized: true, loaded: true });
      })
      .catch((err) => {
        this.setState({ authorized: false, loaded: true });
      });
  }

  render() {
    // Ja dati nav ielādēti, tad tiek attēlots "Spin" elements
    if (!this.state.loaded) {
      return (
        <Layout className="new-edit-student-content">
          <Content>
            <div className="edit-student-form-div">
              <Spin />
            </div>
          </Content>
        </Layout>
      );
    }
    // Ja lietotājam nav tiesības redzēt konkrēto resursu, tad tiek attēlots "Alert" brīdinājums
    if (!this.state.authorized) {
      return (
        <Layout className="student-list-layout">
          <Content>
            <Alert
              message="Nav piekļuves"
              description="Jums nav piekļuves šiem datiem vai funkcionalitātei!"
              type="error"
              showIcon
            />
          </Content>
        </Layout>
      );
    }
    return (
      <Layout className="new-edit-student-content">
        <Content>
          <div className="new-student-form-div">
            <Form
              labelCol={{ span: 6 }}
              wrapperCol={{ span: 24 }}
              onFinish={this.sendStudentData}
              className="new-student-form"
            >
              <Button
                className="new-student-button"
                onClick={() => {
                  this.setState({ newStudentUser: !this.state.newStudentUser });
                }}
              >
                {this.renderAccountButton()}
              </Button>
              {this.renderFormFields()}
              <Form.Item>
                <Button
                  className="new-student-button"
                  htmlType="submit"
                  type="primary"
                >
                  Izveidot skolēna ierakstu
                </Button>
              </Form.Item>
            </Form>
          </div>
        </Content>
      </Layout>
    );
  }
}

export default withRouter(NewClassStudent);
