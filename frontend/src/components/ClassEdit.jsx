import { Component } from "react";
import { withRouter } from "react-router-dom";
import { Layout, Form, Input, Button, message, Alert, Spin, Modal } from "antd";
import { edit, acquire, remove } from "../lib/api";

const { Content } = Layout;

class ClassEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      class: this.props.match.params.id,
      classData: {},
      loaded: false,
      modalVisible: false,
      authorized: true,
    };
  }

  // Atjaunoto datu nosūtīšana
  editClass = (data) => {
    edit(`classes/${this.state.class}`, data)
      .then((res) => {
        if (res.status === 200) {
          message.success("Klases dati veiksmīgi nomainīti!");
          this.props.history.push(`/school/${res.data.schoolId}/classes`);
        }
      })
      .catch((err) => {
        if (err.response.status === 422 || err.response.status === 404) {
          message.error(err.response.data.message);
        } else {
          message.error("Sistēmas kļūda! Pamēģiniet vēlreiz!");
        }
      });
  };

  // Modālā loga aizvēršanas / atvēršanas funkcija
  setModalVisible = (value) => {
    this.setState({ modalVisible: value });
  };

  // Klases dzēšanas funkcija
  deleteClass = () => {
    remove(`classes/${this.state.class}`)
      .then((res) => {
        if (res.status === 200) {
          message.success("Klases ieraksts dzēsts");
          this.props.history.push(`/school/${res.data.schoolId}/classes`);
        }
      })
      .catch((err) => {
        message.error("Sistēmas kļūda! Pamēģiniet vēlreiz!");
      });
  };

  // Ielādes laikā tiek iegūti dati par klasi
  componentDidMount() {
    acquire(`classes/${this.state.class}/edit`)
      .then((res) => {
        this.setState({ classData: res.data.data, loaded: true });
      })
      .catch((err) => {
        this.setState({ authorized: false, loaded: true });
      });
  }

  render() {
    // Ja dati vēl nav ielādēti, tad tiek attēlots "Spin" ielādes elements
    if (!this.state.loaded) {
      return (
        <Layout className="new-edit-class-content">
          <Content>
            <div className="edit-class-form-div">
              <Spin />
            </div>
          </Content>
        </Layout>
      );
    }
    // Ja lietotājs nav autorizēts redzēt konkrēto saturu, tad par to tiek brīdināts ar "Alert" elementu
    if (!this.state.authorized) {
      return (
        <Layout className="class-list-layout">
          <Content>
            <Alert
              message="Nav piekļuves"
              description="Jums nav piekļuves šiem datiem vai funkcionalitātei!"
              type="error"
              showIcon
            />
          </Content>
        </Layout>
      );
    }
    return (
      <Layout className="new-edit-class-content">
        <Content>
          <Modal
            title="Vai dzēst telpas ierakstu?"
            onCancel={() => this.setModalVisible(false)}
            centered
            visible={this.state.modalVisible}
            footer={[
              <Button onClick={() => this.setModalVisible(false)}>
                Atcelt
              </Button>,
              <Button type="primary" danger onClick={this.deleteClass}>
                Dzēst
              </Button>,
            ]}
          >
            Šo darbību nevar atsaukt! Tiks dzēsti arī ar šo klasi saistītie
            dati!
          </Modal>
          <div className="edit-class-form-div">
            <Form
              labelCol={{ span: 9 }}
              wrapperCol={{ span: 24 }}
              onFinish={this.editClass}
              className="edit-class-form"
              initialValues={{ class_name: this.state.classData.name }}
            >
              <Form.Item
                name="class_name"
                label="Jauns klases nosaukums"
                rules={[
                  { required: true, message: "Ievadiet klases nosaukumu!" },
                ]}
              >
                <Input />
              </Form.Item>
              <Form.Item>
                <Button
                  className="edit-class-button"
                  htmlType="submit"
                  type="primary"
                >
                  Rediģēt klases datus
                </Button>
              </Form.Item>
              <Button
                className="delete-room-button"
                danger
                type="primary"
                onClick={() => this.setState({ modalVisible: true })}
              >
                Dzēst klases ierakstu
              </Button>
            </Form>
          </div>
        </Content>
      </Layout>
    );
  }
}

export default withRouter(ClassEdit);
