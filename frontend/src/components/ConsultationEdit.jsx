import {
  Layout,
  Form,
  Input,
  Button,
  message,
  Alert,
  Spin,
  Modal,
  Select,
  InputNumber,
  DatePicker,
} from "antd";
import { Component } from "react";
import { acquire, edit, remove } from "../lib/api";
import { withRouter } from "react-router-dom";
import moment from 'moment';

const { Content } = Layout;
const { Option } = Select;

class ConsultationEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      authorized: false,
      consultation: {},
      consultationId: this.props.match.params.id,
      rooms: [],
    };
  }

  // Modālā loga atvēršanas / aizvēršanas funkcija
  setModalVisible = (value) => {
    this.setState({ modalVisible: value });
  };

  // Atjaunoto datu nosūtīšana uz serveri
  editConsultation = (values) => {
    edit(`consultation/${this.state.consultationId}`, values)
      .then((res) => {
        message.success("Konsultācijas ieraksts saglabāts!");
        this.props.history.push(
          `/school/${this.state.consultation.teacher.school_id}/consultations`
        );
      })
      .catch((err) => {
        message.error("Sistēmas kļūda! Pamēģiniet vēlreiz!");
      });
  };

  // Konsultācijas dzēšanas pieprasījuma nosūtīšana
  deleteConsultation = () => {
    remove(`consultation/${this.state.consultationId}`)
      .then((res) => {
        if (res.status === 200) {
          message.success("Konsultācijas ieraksts dzēsts");
          this.props.history.push(`/school/${res.data.schoolId}/consultations`);
        }
      })
      .catch((err) => {
        message.error("Sistēmas kļūda! Pamēģiniet vēlreiz!");
      });
  };

  // Telpas datu ielāde attiecīgā formātā, lai tos varētu attēlot iekš "Select" elementa
  renderRoomData = () => {
    return this.state.rooms.map((room) => {
      return (
        <Option value={room.id}>
          {room.name + " [" + room.seat_count + "]"}
        </Option>
      );
    });
  };

  // Komponentes ielādes laikā tiek iegūti dati par konkrēto konsultāciju un telpām.
  componentDidMount() {
    acquire(`consultation/${this.state.consultationId}/edit`)
      .then((res) => {
        this.setState({
          consultation: res.data.data,
        });

        acquire(
          `school/${this.state.consultation.teacher.school_id}/consultation/rooms`
        )
          .then((res) => {
            this.setState({
              authorized: true,
              loaded: true,
              rooms: res.data.data,
            });
          })
          .catch((err) => {
            this.setState({ authorized: false, loaded: true });
          });
      })
      .catch((err) => {
        this.setState({ authorized: false, loaded: true });
      });
  }

  render() {
    // Ja dati vēl nav ielādēti, tad tiek attēlots "Spin" ielādes elements
    if (!this.state.loaded) {
      return (
        <Layout className="new-student-content">
          <Content>
            <div className="new-student-form-div">
              <Spin />
            </div>
          </Content>
        </Layout>
      );
    }
    // Ja dati ir ielādēti, bet lietotājs nav autorizēts tos redzēt, tad tiek sniegts paziņojums par to
    if (!this.state.authorized) {
      return (
        <Layout className="school-list-layout">
          <Content>
            <Alert
              message="Nav piekļuves"
              description="Jums nav piekļuves šiem datiem vai funkcionalitātei!"
              type="error"
              showIcon
            />
          </Content>
        </Layout>
      );
    }
    return (
      <Layout className="new-edit-room-content">
        <Content>
          <Modal
            title="Vai dzēst konsultācijas ierakstu?"
            onCancel={() => this.setModalVisible(false)}
            centered
            visible={this.state.modalVisible}
            footer={[
              <Button onClick={() => this.setModalVisible(false)}>
                Atcelt
              </Button>,
              <Button type="primary" danger onClick={this.deleteConsultation}>
                Dzēst
              </Button>,
            ]}
          >
            Šo darbību nevar atsaukt! Tiks dzēsti arī ar šo konsultāciju saistītie dati!
          </Modal>
          <div className="edit-room-form-div">
            <Form
              labelCol={{ span: 9 }}
              wrapperCol={{ span: 24 }}
              onFinish={this.editConsultation}
              className="edit-school-form"
              initialValues={{
                available_seats: this.state.consultation.attendant_count,
                room: this.state.consultation.room.id,
                topic: this.state.consultation.topic,
                start_date: moment(this.state.consultation.date)
              }}
            >
              <Form.Item
                name="topic"
                label="Konsultācijas tēma"
                rules={[
                  { required: true, message: "Ievadiet konsultācijas tēmu!" },
                ]}
              >
                <Input placeholder="Tēma"/>
              </Form.Item>
              <Form.Item
                name="room"
                label="Telpa"
                rules={[
                  {
                    required: true,
                    message:
                      "Izvēlieties telpu, kurā norisināsies konsultācija!",
                  },
                ]}
              >
                <Select
                  showSearch
                  placeholder="Izvēlieties kabinetu"
                  optionFilterProp="children"
                  filterOption={(input, option) =>
                    option.children
                      .toLowerCase()
                      .indexOf(input.toLowerCase()) >= 0
                  }
                >
                  {this.renderRoomData()}
                </Select>
              </Form.Item>
              <Form.Item
                name="available_seats"
                label="Pieejamo vietu skaits"
                rules={[
                  {
                    type: "number",
                    min: 1,
                    max: 10000,
                    message:
                      "Pieejamo vietu skaitam jābūt robežās no 1 līdz 10000!",
                  },
                  {
                    message: "Norādiet pieejamo vietu skaitu!",
                    required: true,
                  },
                ]}
              >
                <InputNumber />
              </Form.Item>
              <Form.Item
                name="start_date"
                label="Datums un laiks"
                rules={[
                  {
                    required: true,
                    message: "Norādiet konsultācijas datumu!",
                  },
                ]}
              >
                <DatePicker
                  placeholder="Sākuma laiks"
                  showTime
                  format="DD-MM-YYYY HH:mm"
                />
              </Form.Item>
              <Form.Item>
                <Button
                  className="edit-room-button"
                  htmlType="submit"
                  type="primary"
                >
                  Rediģēt konsultācijas datus
                </Button>
              </Form.Item>
              <Button
                className="delete-room-button"
                danger
                type="primary"
                onClick={() => this.setModalVisible(true)}
              >
                Dzēst konsulatācijas ierakstu
              </Button>
            </Form>
          </div>
        </Content>
      </Layout>
    );
  }
}

export default withRouter(ConsultationEdit);
