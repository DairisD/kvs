import {
  Layout,
  Form,
  Input,
  Button,
  Spin,
  message,
  Alert,
} from "antd";
import { Component } from "react";
import { send } from "../lib/api";
import { withRouter } from "react-router-dom";
import { acquire } from "../lib/api";

const { Content } = Layout;

class NewAppointment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      authorized: false,
      consultationId: this.props.match.params.id,
      loaded: false,
    };
  }

  // Jaunā pieteikuma datu nosūtīšana uz serveri
  sendAppointmentData = (values) => {
    send(`consultation/${this.state.consultationId}/appointments`, values)
      .then((res) => {
        message.success("Pieteikšanās konsultācijai veiksmīga!");
        this.props.history.push(`/consultation/${this.state.consultationId}/appointments`);
      })
      .catch((err) => {
        if (err.response.status === 422) {
          message.error(err.response.data.message);
        }
        else {
          message.error("Sistēmas kļūda! Pamēģiniet vēlreiz.");
        }
      });
  };

  // Pieprasījums serverim, kurš apstiprina vai autentificētais lietotājs konkrētajai konsultācijai drīkst pieteikties
  componentDidMount() {
    acquire(`consultation/${this.state.consultationId}/appointments/new`)
      .then((res) => {
        this.setState({ authorized: true, loaded: true });
      })
      .catch((err) => {
        this.setState({ authorized: false, loaded: true });
      });
  }

  render() {
    // Ja dati nav ielādēti, tad tiek attēlots "Spin" elements
    if (!this.state.loaded) {
      return (
        <Layout className="new-edit-class-content">
          <Content>
            <div className="edit-class-form-div">
              <Spin />
            </div>
          </Content>
        </Layout>
      );
    }
    // Ja lietotājam nav tiesības redzēt konkrēto resursu, tad tiek attēlots "Alert" brīdinājums
    if (!this.state.authorized) {
      return (
        <Layout className="class-list-layout">
          <Content>
            <Alert
              message="Nav piekļuves"
              description="Jums nav piekļuves šiem datiem vai funkcionalitātei!"
              type="error"
              showIcon
            />
          </Content>
        </Layout>
      );
    }
    return (
      <Layout className="new-edit-class-content">
        <Content>
          <div className="new-class-form-div">
            <Form
              labelCol={{ span: 6 }}
              wrapperCol={{ span: 24 }}
              onFinish={this.sendAppointmentData}
              className="new-room-form"
            >
              <Form.Item
                name="reason"
                label="Apmeklējuma iemesls"
                rules={[
                  { required: true, message: "Ievadiet apmeklējuma iemeslu!" },
                ]}
              >
                <Input placeholder="Iemesls" />
              </Form.Item>
              <Form.Item>
                <Button
                  className="new-class-button"
                  htmlType="submit"
                  type="primary"
                >
                  Pieteikties konsultācijai
                </Button>
              </Form.Item>
            </Form>
          </div>
        </Content>
      </Layout>
    );
  }
}

export default withRouter(NewAppointment);
