import { Layout, Form, Button, Input, Spin, Modal, message, Alert } from "antd";
import { Component } from "react";
import { withRouter } from "react-router-dom";
import { edit, remove, acquire } from "../lib/api";

const { Content } = Layout;

class SchoolEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      schoolId: this.props.match.params.id,
      schoolName: null,
      loaded: false,
      modalVisible: false,
    };
  }

  // Atjaunoto datu nosūtīšana
  renameSchool = (data) => {
    edit(`school/${this.state.schoolId}`, data)
      .then((res) => {
        if (res.status === 200) {
          message.success("Skolas nosaukums veiksmīgi nomainīts!");
          this.props.history.push("/schools");
        }
      })
      .catch((err) => {
        if (err.response.status === 422 || err.response.status === 404) {
          message.error(err.response.data.message);
        } else {
          message.error("Sistēmas kļūda! Pamēģiniet vēlreiz!");
        }
      });
  };

  // Modālā loga aizvēršanas / atvēršanas funkcija
  setModalVisible = (value) => {
    this.setState({ modalVisible: value });
  };

  // Ielādes laikā tiek iegūti dati par skolu
  componentDidMount() {
    acquire(`school/${this.state.schoolId}/edit`)
      .then((res) => {
        this.setState({
          schoolName: res.data.data.name,
          loaded: true,
          authorized: true,
        });
      })
      .catch((err) => {
        if (err.response.status !== 403) {
          this.setState({ authorized: true, loaded: false });
          message.error("Notikusi sistēmas kļūda. Lūdzu mēģiniet vēlreiz!");
          return;
        }
        this.setState({ loaded: true, authorized: false });
      });
  }

  // Skolas dzēšanas funkcija
  deleteSchool = () => {
    remove(`school/${this.state.schoolId}`)
      .then((res) => {
        if (res.status === 200) {
          message.success("Skolas ieraksts dzēsts");
          this.props.history.push("/schools");
        }
      })
      .catch((err) => {
        message.error("Sistēmas kļūda! Pamēģiniet vēlreiz!");
      });
  };

  render() {
    // Ja dati vēl nav ielādēti, tad tiek attēlots "Spin" ielādes elements
    if (!this.state.loaded) {
      return (
        <Layout className="new-student-content">
          <Content>
            <div className="new-student-form-div">
              <Spin />
            </div>
          </Content>
        </Layout>
      );
    }
    // Ja lietotājs nav autorizēts redzēt konkrēto saturu, tad par to tiek brīdināts ar "Alert" elementu
    if (!this.state.authorized) {
      return (
        <Layout className="school-list-layout">
          <Content>
            <Alert
              message="Nav piekļuves"
              description="Jums nav piekļuves šiem datiem vai funkcionalitātei!"
              type="error"
              showIcon
            />
          </Content>
        </Layout>
      );
    }
    return (
      <Layout className="new-edit-school-content">
        <Content>
          <Modal
            title="Vai dzēst skolas ierakstu?"
            onCancel={() => this.setModalVisible(false)}
            centered
            visible={this.state.modalVisible}
            footer={[
              <Button onClick={() => this.setModalVisible(false)}>
                Atcelt
              </Button>,
              <Button type="primary" danger onClick={this.deleteSchool}>
                Dzēst
              </Button>,
            ]}
          >
            Šo darbību nevar atsaukt! Tiks dzēsti arī klašu, profilu un
            konsultāciju dati!
          </Modal>
          <div className="edit-school-form-div">
            <Form
              labelCol={{ span: 9 }}
              wrapperCol={{ span: 24 }}
              onFinish={this.renameSchool}
              className="edit-school-form"
              initialValues={{ school_name: this.state.schoolName }}
            >
              <Form.Item
                name="school_name"
                label="Jauns skolas nosaukums"
                rules={[
                  { required: true, message: "Ievadiet skolas nosaukumu!" },
                ]}
              >
                <Input />
              </Form.Item>
              <Form.Item>
                <Button
                  className="edit-school-button"
                  htmlType="submit"
                  type="primary"
                >
                  Mainīt skolas nosaukumu
                </Button>
              </Form.Item>
              <Button
                className="delete-school-button"
                danger
                type="primary"
                onClick={() => this.setState({ modalVisible: true })}
              >
                Dzēst skolas ierakstu
              </Button>
            </Form>
          </div>
        </Content>
      </Layout>
    );
  }
}

export default withRouter(SchoolEdit);
