import Cookies from "universal-cookie";

const cookie = new Cookies();

// Tiek pārbaudīts vai lietotāja pārlūkā eksistē sīkdatne, kas norāda uz to, ka viņš ir autorizējies.
export const isLoggedIn = () => {
  return !!cookie.get("loggedInKvs", { path: "/" });
};

// Autorizācijas sīkdatne pārlūkā
export const setLoginCookie = () => {
  if (!cookie.get("loggedInKvs")) {
    cookie.set("loggedInKvs", true, { maxAge: 7200, path: "/" });
  }
};

// Sīkdatnes noņēmšana lietotāja pārlūkam.
export const logout = () => {
  cookie.remove("loggedInKvs", { path: "/" });
  window.localStorage.clear();
};

// Tiek pārbaudīts vai lietotājam eksistē konkrētā loma.
export const hasRole = (user, roles) => {
  if (roles) {
    if (roles === "all") {
      return true;
    } else if (user) {
      return roles.indexOf(user.role) !== -1;
    }
  } else {
    return false;
  }
};
