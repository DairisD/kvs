import { UserSwitchOutlined, BulbOutlined, UserOutlined, HomeOutlined } from "@ant-design/icons";
import ProfileSelect from "../components/ProfileSelect";
import SchoolList from "../components/SchoolList";
import NewSchool from "../components/NewSchool";
import SchoolEdit from "../components/SchoolEdit";
import ClassList from "../components/ClassList";
import RoomList from "../components/RoomList";
import RoomEdit from "../components/RoomEdit";
import NewRoom from "../components/NewRoom";
import NewClass from "../components/NewClass";
import ClassEdit from "../components/ClassEdit";
import ClassStudents from "../components/ClassStudents";
import NewClassStudent from "../components/NewClassStudent";
import NewSchoolTeacher from "../components/NewSchoolTeacher";
import StudentProfileEdit from "../components/StudentProfileEdit";
import TeacherProfileEdit from "../components/TeacherProfileEdit";
import SchoolTeacherList from "../components/SchoolTeacherList";
import ConsultationList from "../components/ConsultationList";
import ConsultationEdit from "../components/ConsultationEdit";
import NewConsultation from "../components/NewConsultation";
import ConsultationAppointments from "../components/ConsultationAppointments";
import NewAppointment from "../components/NewAppointment";
import AccountEdit from "../components/AccountEdit";
import SchoolAdminsList from "../components/SchoolAdminsList";
import SchoolAdminProfileEdit from "../components/SchoolAdminProfileEdit";
import NewSchoolAdmin from "../components/NewSchoolAdmin";
import { BsDoorClosed } from "react-icons/bs";
import { SiGoogleclassroom } from "react-icons/si";
import { FaUniversity } from "react-icons/fa";
import { CgProfile } from "react-icons/cg";


const routes = [
  {
    path: "/profiles",
    component: ProfileSelect,
    menuName: "Profila izvēle",
    icon: <UserSwitchOutlined />,
    display_roles: "all"
  },
  {
    path: "/schools/new",
    component: NewSchool,
  },
  {
    path: "/schools",
    component: SchoolList,
    menuName: "Skolas",
    icon: <HomeOutlined />,
    display_roles: ["system_admin"],
  },
  {
    path: "/school/:id/edit",
    component: SchoolEdit,
    id: true,
  },
  {
    path: "/school/:id/rooms/new",
    component: NewRoom,
    id: true
  },
  {
    path: "/school/:id/rooms",
    component: RoomList,
    menuName: "Telpas",
    icon: <BsDoorClosed/>,
    display_roles: ["school_admin"],
    id: true
  },
  {
    path: "/school/:id/classes/new",
    component: NewClass,
    id: true
  },
  {
    path: "/school/:id/classes",
    component: ClassList,
    menuName: "Klases",
    icon: <SiGoogleclassroom/>,
    display_roles: ["school_admin"],
    id: true
  },
  {
    path: "/school/:id/teachers/new",
    component: NewSchoolTeacher,
    id: true
  },
  {
    path: "/school/:id/teachers",
    component: SchoolTeacherList,
    menuName: "Skolotāji",
    icon: <FaUniversity/>,
    display_roles: ["school_admin"],
    id: true
  },
  {
    path: "/school/:id/admins/new",
    component: NewSchoolAdmin,
    id: true
  },
  {
    path: "/school/:id/admins",
    component: SchoolAdminsList,
    id: true
  },
  {
    path: "/school/:id/consultations/new",
    component: NewConsultation,
    id: true
  },
  {
    path: "/school/:id/consultations",
    component: ConsultationList,
    menuName: "Konsultācijas",
    icon: <BulbOutlined />,
    display_roles: ["school_admin", "teacher", "student"],
    id: true
  },
  {
    path: "/account/edit",
    component: AccountEdit,
    menuName: "Konts",
    icon: <CgProfile />,
    display_roles: "all",
  },
  {
    path: "/room/:id/edit",
    component: RoomEdit,
    id: true
  },
  {
    path: "/class/:id/edit",
    component: ClassEdit,
    id: true
  },
  {
    path: "/class/:id/students/new",
    component: NewClassStudent,
    id: true
  },
  {
    path: "/class/:id/students",
    component: ClassStudents,
    id: true
  },
  {
    path: "/student/:id/edit",
    component: StudentProfileEdit,
    id: true
  },
  {
    path: "/teacher/:id/edit",
    component: TeacherProfileEdit,
    id: true
  },
  {
    path: "/consultation/:id/edit",
    component: ConsultationEdit,
    id: true
  },
  {
    path: "/consultation/:id/appointments/new",
    component: NewAppointment,
    id: true
  },
  {
    path: "/consultation/:id/appointments",
    component: ConsultationAppointments,
    id: true
  },
  {
    path: "/admin/:id/edit",
    component: SchoolAdminProfileEdit,
    id: true
  },
];

export default routes;
