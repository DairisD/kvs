import axios from "axios";


// Funkcija, kas izsauc GET vaicājumu uz serveri.
export const acquire = (source, params = null, version = `v1`) => {
  var host = "";
  if (window.location.hostname === `localhost`) {
    host = `http://localhost:5000`;
  }

  return axios.get(`${host}/api/${version}/${source}`, {
    headers: {
      Accept: "application/json",
    },
    params: params
  });
};

// Funkcija, kas izsauc POST vaicājumu uz serveri.
export const send = (source, data = null, version = "v1") => {
  var host = "";
  if (window.location.hostname === `localhost`) {
    host = `http://localhost:5000`;
  }

  return axios.post(`${host}/api/${version}/${source}`, data, {
    headers: { Accept: "application/json" },
  });
};

// Funkcija lietotāja datu nosūtīšanai uz serveri
export const login = (tokenSource) => {
  var host = "";
  if (window.location.hostname === `localhost`) {
    host = `http://localhost:5000`;
  }

  return axios.get(`${host}/${tokenSource}`, {
    headers: { Accept: "application/json" },
  });
};

// Funkcija, kas izsauc DELETE vaicājumu uz serveri.
export const remove = (source, data = null, version = "v1") => {
  var host = "";
  if (window.location.hostname === `localhost`) {
    host = `http://localhost:5000`;
  }

  return axios.delete(`${host}/api/${version}/${source}`, {
    headers: { Accept: "application/json" },
    data: data,
  });
};

// Funkcija, kas izsauc PUT vaicājumu uz serveri.
export const edit = (source, data = null, version = "v1") => {
  var host = "";
  if (window.location.hostname === `localhost`) {
    host = `http://localhost:5000`;
  }

  return axios.put(`${host}/api/${version}/${source}`, data, {
    headers: { Accept: "application/json" },
  });
};
