import { Component, createElement } from "react";
import Login from "./components/Login";
import { hasRole, isLoggedIn, logout } from "./lib/auth";
import { send } from "./lib/api";
import { Switch, withRouter, Route, Redirect, Link } from "react-router-dom";
import routes from "./lib/routes";
import axios from "axios";
import { Menu, Layout, message } from "antd";
import { LogoutOutlined } from "@ant-design/icons";

const { Content, Footer, Sider } = Layout;

axios.defaults.withCredentials = true;

// Servera atbildes pārtveršana. Ja tiek identificēts tas, ka lietotājs nav autentificēts, tad
// tiek noņemta autentifikācijas sīkdatne 

axios.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    if (error.response.status === 401) {
      logout();
    }
    return Promise.reject(error);
  }
);

class App extends Component {
  constructor(props) {
    super(props);
    this.state = JSON.parse(window.localStorage.getItem("state")) || {
      collapsed: false,
      user: {},
      loggedState: true,
    };
  }

  // Funkcija, kas apstrādā rīkjoslas bīdīšanu
  onCollapse = () => {
    this.setState({ collapsed: !this.state.collapsed });
  };

  // Lai nepazustu sistēmas stāvoklis pēc lapas pārlādes, tas tiek saglabāts lokālajā pārlūka glabātuvē
  setState = (state) => {
    window.localStorage.setItem("state", JSON.stringify(state));
    super.setState(state);
  };

  // Funkcija, kas tiek padota kā "prop" vērtība citām komponentēm
  setUser = (data) => {
    this.setState({ user: data });
  };

  // Lietotāja atteikšanāš funkcija
  logout = () => {
    send("logout").then((res) => {
      if (res.status === 200) {
        logout();
        this.setState({ loggedState: !this.state.loggedState });
        this.props.history.push("/");
      }
    }).catch((err) => {
      message.error("Sistēmas kļūda! Lūdzu mēģiniet vēlreiz!")
    });
  };

  render() {
    console.log(isLoggedIn());

    // Ja lietotājs nav autentificējies, tad tiek attēlots autentifikācijas skats
    if (!isLoggedIn()) {
      return <Login setUserCallback={this.setUser} />;
    } else {
      return (
        <Layout style={{ minHeight: "100vh" }}>
          <Sider
            collapsible
            collapsed={this.state.collapsed}
            onCollapse={this.onCollapse}
          >
            <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
              
              {/* Tiek pārstaigāts datnes routes.jsx saturs un attēlotas attiecīgās saites */}
              {routes.map((route, i) => {
                if (
                  route.menuName &&
                  hasRole(this.state.user, route.display_roles)
                ) {
                  return (
                    <Menu.Item key={i} icon={route.icon}>
                      <Link
                        to={
                          route.id
                            ? route.path.replace(
                                ":id",
                                this.state.user.school_id
                              )
                            : route.path
                        }
                      >
                        {route.menuName}
                      </Link>
                    </Menu.Item>
                  );
                }
                return null;
              })}
              <Menu.Item
                key={-1}
                onClick={this.logout}
                icon={<LogoutOutlined />}
              >
                Atteikties
              </Menu.Item>
            </Menu>
          </Sider>
          <Layout>
            <Content style={{ padding: "20px" }}>
              <Switch>
                <Route exact path="/">
                  <Redirect to="/profiles" />
                </Route>
                {/* React router saišu imolementācija */}
                {routes.map((route, i) => {
                  return (
                    <Route key={i} path={route.path}>
                      {createElement(route.component, {
                        userData: this.state.user,
                        setUserCallback: this.setUser,
                      })}
                    </Route>
                  );
                })}
              </Switch>
            </Content>
          </Layout>
        </Layout>
      );
    }
  }
}

export default withRouter(App);
