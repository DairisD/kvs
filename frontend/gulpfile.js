'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass')(require('sass'));

function styles() {
  return gulp.src('./src/scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./src/css'));
};

function watch() {
    gulp.watch('./src/scss/**/*.scss', gulp.series('styles'));
}

exports.styles = styles;
exports.watch = watch;