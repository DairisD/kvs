<?php

use App\Http\Controllers\AccountController;
use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RoomController;
use App\Http\Controllers\SchoolController;
use App\Http\Controllers\SchoolClassController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\ConsultationController;
use App\Http\Controllers\SchoolAdminController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('v1')->group(function () {
    Route::post('login', [AuthController::class, "login"]);
    Route::post('logout', [AuthController::class, "logout"]);
});

Route::middleware('auth:sanctum')->prefix('v1')->group(function () {
    Route::post('profileSelect', [ProfileController::class, "selectProfile"]);
    Route::post('schools/search', [SchoolController::class, "searchSchools"]);
    Route::get("admins/{id}/edit", [SchoolAdminController::class, "edit"]);

    Route::prefix('school')->group(function () {
        Route::get("/{id}/rooms", [RoomController::class, "index"]);
        Route::post("/{id}/rooms/search", [RoomController::class, "searchRooms"]);
        Route::get("/{id}/rooms/new", [RoomController::class, "create"]);
        Route::post("/{id}/rooms", [RoomController::class, "store"]);
        Route::get("/{id}/classes", [SchoolClassController::class, "index"]);
        Route::get("/{id}/studentClasses", [SchoolClassController::class, "indexForProfiles"]);
        Route::post("/{id}/classes/search", [SchoolClassController::class, "searchClasses"]);
        Route::get("/{id}/classes/new", [SchoolClassController::class, "create"]);
        Route::post("/{id}/classes", [SchoolClassController::class, "store"]);
        Route::get("/{id}/teachers", [TeacherController::class, "index"]);
        Route::post("/{id}/teachers/search", [TeacherController::class, "searchTeachers"]);
        Route::get("/{id}/teachers/new", [TeacherController::class, "create"]);
        Route::post("/{id}/teachers", [TeacherController::class, "store"]);
        Route::get("/{id}/consultations", [ConsultationController::class, "index"]);
        Route::post("/{id}/consultations/search", [ConsultationController::class, "searchConsultations"]);
        Route::get("/{id}/consultation/rooms", [RoomController::class, "indexForConsultation"]);
        Route::get("/{id}/consultation/teachers", [TeacherController::class, "indexForConsultation"]);
        Route::get("/{id}/consultations/new", [ConsultationController::class, "create"]);
        Route::post("/{id}/consultation", [ConsultationController::class, "store"]);
        Route::get("/{id}/admins", [SchoolAdminController::class, "index"]);
        Route::get("/{id}/admins/new", [SchoolAdminController::class, "create"]);
        Route::post("/{id}/admins", [SchoolAdminController::class, "store"]);
        Route::post("/{id}/admins/search", [SchoolAdminController::class, "searchAdmins"]);
    });


    Route::prefix('account')->group(function () {
        Route::get("/edit", [AccountController::class, "edit"]);
        Route::put("/edit", [AccountController::class, "update"]);
    });

    Route::prefix('consultation')->group(function () {
        Route::get("/{id}/appointments", [AppointmentController::class, "index"]);
        Route::post("/{id}/appointments/search", [AppointmentController::class, "searchAppointments"]);
        Route::get("/{id}/appointments/new", [AppointmentController::class, "create"]);
        Route::post("/{id}/appointments", [AppointmentController::class, "store"]);
    });

    Route::prefix('class')->group(function () {
        Route::get("/{id}/students", [StudentController::class, "index"]);
        Route::post("/{id}/students/search", [StudentController::class, "searchStudents"]);
        Route::get("/{id}/students/new", [StudentController::class, "create"]);
        Route::post("/{id}/students", [StudentController::class, "store"]);
    });

    Route::resource('profiles', ProfileController::class, ["only" => ["index"]]);
    Route::resource('rooms', RoomController::class, ["only" => ["edit", "update", "destroy"]]);
    Route::resource('school', SchoolController::class, ["only" => ["index", "create", "store", "edit", "update", "destroy"]]);
    Route::resource("classes", SchoolClassController::class, ["only" => ["edit", "update", "destroy"]]);
    Route::resource("students", StudentController::class, ["only" => ["edit", "update", "destroy"]]);
    Route::resource("teachers", TeacherController::class, ["only" => ["edit", "update", "destroy"]]);
    Route::resource("consultation", ConsultationController::class, ["only" => ["edit", "update", "destroy"]]);
    Route::resource("appointment", AppointmentController::class, ["only" => ["destroy"]]);
    Route::resource("admins", SchoolAdminController::class, ["only" => ['edit', 'update', 'destroy']]);
});
