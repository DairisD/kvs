<?php

namespace App\Http\Controllers;

use App\Models\SchoolClass;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Auth;
use App\Models\UserSchool;
use Illuminate\Support\Facades\DB;

class SchoolClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        if (session("profileId")) {
            $profile = UserSchool::find(session("profileId"));
            if ($profile->user_id == Auth::id()) {
                if ($profile->hasPermissionTo("edit all class records") || ($profile->hasPermissionTo("edit school class records") && $profile->school_id == $id)) {
                    try {
                        $classes = SchoolClass::where("school_id", $id)->paginate(8);
                        return response()->json(["message" => "success", "data" => $classes], 200);
                    } catch (Exception $e) {
                        return response()->json(["message" => "error"], 500);
                    }
                }
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));
                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit all class records")
                        || ($profile->hasPermissionTo("edit school class records")
                            && $profile->school_id == $id)
                    ) {
                        return response()->json(["message" => "success"], 200);
                    }
                }
            } catch (Exception $e) {
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $data = $request->all();
        if (session("profileId")) {
            DB::beginTransaction();
            try {
                $profile = UserSchool::find(session("profileId"));
                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit all class records")
                        || ($profile->hasPermissionTo("edit school class records")
                            && $profile->school_id == $id)
                    ) {

                        if (SchoolClass::where("name", $data["name"])->where('school_id', '=', $id)->first()) {
                            return response()->json(["message" => "Klase ar šādu nosaukumu jau eksistē"], 422);
                        }

                        $class = new SchoolClass([
                            "name" => $data["name"], "school_id" => $id
                        ]);
                        $class->save();
                        DB::commit();
                        return response()->json(["message" => "success"], 200);
                    }
                }
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));
                $class = SchoolClass::find($id);
                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit all class records")
                        || ($profile->hasPermissionTo("edit school class records")
                            && $profile->school_id == $class->school_id)
                    ) {
                        return response()->json(["message" => "success", "data" => $class], 200);
                    }
                }
            } catch (Exception $e) {
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        if (session("profileId")) {
            DB::beginTransaction();
            try {
                $profile = UserSchool::find(session("profileId"));
                $class = SchoolClass::find($id);
                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit all class records")
                        || ($profile->hasPermissionTo("edit school class records")
                            && $profile->school_id == $class->school_id)
                    ) {
                        if (SchoolClass::where("name", $data["class_name"])->where('id', '<>', $id)->where('school_id', '=', $class->school_id)->first()) {
                            return response()->json(["message" => "Klase ar šādu nosaukumu jau eksistē"], 422);
                        }

                        $class->name = $data["class_name"];
                        $class->save();
                        DB::commit();
                        return response()->json(["message" => "success", "schoolId" => $class->school_id], 200);
                    }
                }
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (session("profileId")) {
            DB::beginTransaction();
            try {
                $profile = UserSchool::find(session("profileId"));
                if ($profile->user_id == Auth::id()) {
                    $classToDelete = SchoolClass::find($id);
                    $school = $classToDelete->school_id;
                    if ($profile->hasPermissionTo("edit all class records") || ($profile->hasPermissionTo("edit school class records") && $profile->school_id == $classToDelete->school_id)) {
                        $classToDelete->delete();
                        DB::commit();
                        return response()->json(["message" => "success", "schoolId" => $school], 200);
                    }
                }
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => 'Unauthorized'], 403);
    }


    public function searchClasses(Request $request, $id)
    {
        $data = $request->all();
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));
                if ($profile->user_id == Auth::id()) {
                    if ($profile->hasPermissionTo("edit all class records") || ($profile->hasPermissionTo("edit school class records") && $profile->school_id == $id)) {
                        // Tiek atgriezts saraksts ar klasēm, kuru nosaukums atbilst meklētajai frāzei
                        $classes = SchoolClass::where("school_id", $id)->where("name", "ilike", "%" . $data["class_name"] . "%")->paginate(8);
                        return response()->json(["message" => "success", "data" => $classes], 200);
                    }
                }
            } catch (Exception $e) {
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    // Funkcija, kas atgriež sarakstu ar klasēm, kurš tiks lietots skolēna profila izveides un labošanas laikā.
    public function indexForProfiles($id)
    {
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));
                if ($profile->user_id == Auth::id()) {
                    if ($profile->hasPermissionTo("edit all student profiles") || ($profile->hasPermissionTo("edit school student profiles") && $profile->school_id == $id)) {
                        $classes = SchoolClass::where("school_id", $id)->get();
                        return response()->json(["message" => "success", "data" => $classes], 200);
                    }
                }
            } catch (Exception $e) {
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }
}
