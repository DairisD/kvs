<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exception;
use App\Models\SchoolClass;
use App\Models\UserSchool;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Builder;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));
                $class = SchoolClass::find($id);
                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit all student profiles")
                        || ($profile->hasPermissionTo("edit school student profiles")
                            && $profile->school_id == $class->school_id)
                    ) {
                        // Skolēniem nav nepieciešama lomu pārbaude, jo skolēni ir vienīgā lietotāju grupa, kur profilā būs norādīts klases id
                        $students = UserSchool::where("class_id", $id)->with('user')->get();
                        return response()->json(["message" => "success", "data" => $students], 200);
                    }
                }
            } catch (Exception $e) {
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));
                $class = SchoolClass::find($id);
                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit all student profiles")
                        || ($profile->hasPermissionTo("edit school student profiles")
                            && $profile->school_id == $class->school_id)
                    ) {
                        return response()->json(["message" => "success"], 200);
                    }
                }
            } catch (Exception $e) {
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        if (session("profileId")) {
            $profile = UserSchool::find(session("profileId"));
            $class = SchoolClass::find($id);
            if ($profile->user_id == Auth::id()) {
                if (
                    $profile->hasPermissionTo("edit all student profiles")
                    || ($profile->hasPermissionTo("edit school student profiles")
                        && $profile->school_id == $class->school_id)
                ) {
                    DB::beginTransaction();
                    try {

                        $data = $request->all();

                        // Formā saņemto datu validācija
                        if (array_key_exists("newUser", $data)) {
                            if ($data["newUser"]) {
                                $data = $request->validate([
                                    "username" => "required|alpha_dash|max:255",
                                    "name" => "required|max:255",
                                    "surname" => "required|max:255",
                                    "password" => "required|max:255",
                                    "newUser" => "required|boolean"
                                ]);

                                $usernameCheck = User::where('username', $data["username"])->get();

                                // Brīdinājums gadījumā, ja lietotājvārds jau ir aizņemts
                                if (!$usernameCheck->isEmpty()) {
                                    return response()->json(["message" => "Username taken"], 422);
                                }
                            } else {
                                $data = $request->validate([
                                    "username" => "required|alpha_dash|max:255",
                                    "newUser" => "required|boolean"
                                ]);
                            }
                        } else {
                            return response()->json(["message" => "error"], 500);
                        }

                        // Gadījums, ja netiek veidots jauns konts
                        if (!$data["newUser"]) {
                            $dbUser = User::where("username", $data["username"])->first();
                            if (!$dbUser) {
                                return response()->json(["message" => "Lietotājs nav atrasts"], 404);
                            } else {
                                $newProfile = new UserSchool(["school_id" => $class->school_id, "class_id" => $id]);
                                $newProfile->assignRole("student");
                                $dbUser->userSchools()->save($newProfile);
                            }
                            // Tiek veidots jauns konts ar atbilstošu profilu
                        } else {
                            if (User::where("username", $data["username"])->first()) {
                                return response()->json(["message" => "Lietotājvārds ir aizņemts"], 422);
                            } else {
                                $dbUser = new User(["username" => $data["username"], "name" => $data["name"], "surname" => $data["surname"], "password" => Hash::make($data["password"])]);
                                $dbUser->save();
                                $newProfile = new UserSchool(["school_id" => $class->school_id, "class_id" => $id]);
                                $newProfile->assignRole("student");
                                $dbUser->userSchools()->save($newProfile);
                            }
                        }
                        DB::commit();
                        return response()->json(["message" => "Success"], 200);
                    } catch (Exception $e) {
                        DB::rollBack();
                        return response()->json(["message" => "Error"], 500);
                    }
                }
            }
        }
        return response()->json(["message" => 'Unauthorized'], 403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));
                $editableProfile = UserSchool::where("id", $id)->with('user')->first();
                if ($profile->user_id == Auth::id()) {
                    if ($profile->hasPermissionTo("edit all student profiles") || ($profile->hasPermissionTo("edit school student profiles") && $profile->school_id == $editableProfile->school_id)) {
                        if ($editableProfile->hasRole("student")) {
                            return response()->json(["message" => "success", "data" => $editableProfile], 200);
                        } else {
                            return response()->json(["message" => "error"], 422);
                        }
                    }
                }
            } catch (Exception $e) {
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (session("profileId")) {
            try {
                DB::beginTransaction();
                $profile = UserSchool::find(session("profileId"));
                $editableProfile = UserSchool::find($id);
                $editableAccount = User::find($editableProfile->user_id);

                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit all student profiles")
                        || ($profile->hasPermissionTo("edit school student profiles")
                            && $profile->school_id == $editableProfile->school_id)
                    ) {

                        // Skolēna rediģēto datu validācija
                        $data = $request->validate([
                            "username" => "required|alpha_dash|max:255",
                            "name" => "required|max:255",
                            "surname" => "required|max:255",
                            "className" => "required|numeric",
                            "password" => "nullable"
                        ]);

                        $editableAccount->username = $data["username"];
                        $editableAccount->name = $data["name"];
                        $editableAccount->surname = $data["surname"];
                        $editableProfile->class_id = $data["className"];

                        if (array_key_exists("password", $data)) {
                            $editableAccount->password = Hash::make($data["password"]);
                        }

                        // Aizņemta lietotājvārda pārbaude
                        $usernameCheck = User::where('id', '<>', $editableAccount->id)->where('username', $data["username"])->get();

                        if (!$usernameCheck->isEmpty()) {
                            return response()->json(["message" => "username taken"], 422);
                        }

                        $editableProfile->save();
                        $editableAccount->save();

                        DB::commit();
                        return response()->json(["message" => "success", "classId" => $editableProfile->class_id], 200);
                    }
                }
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (session("profileId")) {
            DB::beginTransaction();
            try {
                $profile = UserSchool::find(session("profileId"));
                $profileToDelete = UserSchool::find($id);

                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit all student profiles")
                        || ($profile->hasPermissionTo("edit school student profiles")
                            && $profile->school_id == $profileToDelete->school_id)
                    ) {

                        $classId = $profileToDelete->class_id;
                        $profileToDelete->delete();

                        DB::commit();
                        return response()->json(["message" => "success", "classId" => $classId], 200);
                    }
                }
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    // Skolēnu meklēšana pēc konkrētas frāzes
    public function searchStudents(Request $request, $id)
    {
        $data = $request->all();
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));
                $class = SchoolClass::find($id);

                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit all student profiles")
                        || ($profile->hasPermissionTo("edit school student profiles")
                            && $profile->school_id == $class->school_id)
                    ) {
                        // Skolēnu meklēšana pēc lietotāja norādītās frāzes
                        $students = DB::table('users')->join('user_schools', 'users.id', '=', 'user_schools.user_id')
                            ->where('class_id', '=', $class->id)
                            ->where(DB::raw("CONCAT(name, ' ', surname)"), "ilike", "%" . $data["student_name"] . "%")->get();

                        $students->mapWithKeys(function ($item, $key) {
                            return $item->user = ["id" => $item->user_id, "username" => $item->username, "name" => $item->name, "surname" => $item->surname];
                        });

                        return response()->json(["message" => "success", "data" => $students], 200);
                    }
                }
            } catch (Exception $e) {
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }
}
