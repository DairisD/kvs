<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $attr = $request->validate([
            'username' => 'required|string',
            'password' => 'required|string'
        ]);

        if (!Auth::attempt($attr)) {
            return response()->json(["message" => "Wrong credentials"], 401);
        }

        return response()->json(["message" => "Success"], 200);
    }

    public function logout() {
        Auth::guard('web')->logout();
        return response()->json(['message' => 'Success'], 200);
    }
}
