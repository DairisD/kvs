<?php

namespace App\Http\Controllers;

use App\Models\School;
use App\Models\UserSchool;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));
                $school = School::find($id);
                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit all teacher profiles")
                        || ($profile->hasPermissionTo("edit school teacher profiles")
                            && $profile->school_id == $id)
                    ) {
                        $teachers = UserSchool::where("school_id", $id)->role("teacher")->with('user')->get();
                        return response()->json(["message" => "success", "data" => $teachers], 200);
                    }
                }
            } catch (Exception $e) {
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));
                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit all teacher profiles")
                        || ($profile->hasPermissionTo("edit school teacher profiles")
                            && $profile->school_id == $id)
                    ) {
                        return response()->json(["message" => "success"], 200);
                    }
                }
            } catch (Exception $e) {
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        if (session("profileId")) {
            $profile = UserSchool::find(session("profileId"));
            if ($profile->user_id == Auth::id()) {
                if (
                    $profile->hasPermissionTo("edit all teacher profiles")
                    || ($profile->hasPermissionTo("edit school teacher profiles")
                        && $profile->school_id == $id)
                ) {
                    DB::beginTransaction();
                    try {

                        $data = $request->all();

                        //  Gadījums, ja tiek veidots jauns konts
                        if (array_key_exists("newUser", $data)) {
                            if ($data["newUser"]) {
                                $data = $request->validate([
                                    "username" => "required|alpha_dash|max:255",
                                    "name" => "required|max:255",
                                    "surname" => "required|max:255",
                                    "password" => "required|max:255",
                                    "newUser" => "required|boolean"
                                ]);

                                $usernameCheck = User::where('username', $data["username"])->get();
        
                                if(!$usernameCheck->isEmpty()) {
                                    return response()->json(["message" => "Username taken"], 422);
                                }
                                // Pretējā gadīujumā vajadzīgs tikai lietotājvārds no formas
                            } else {
                                $data = $request->validate([
                                    "username" => "required|alpha_dash|max:255",
                                    "newUser" => "required|boolean"
                                ]);
                            }
                        } else {
                            return response()->json(["message" => "error"], 500);
                        }

                        //  Ja netiek veidots jauns konts, tad jau esošam kontam tiek pievienots jauns profils 
                        if (!$data["newUser"]) {
                            $dbUser = User::where("username", $data["username"])->first();
                            if (!$dbUser) {
                                return response()->json(["message" => "Lietotājs nav atrasts"], 404);
                            } else {
                                $newProfile = new UserSchool(["school_id" => $id]);
                                $newProfile->assignRole("teacher");
                                $dbUser->userSchools()->save($newProfile);
                            }
                        } else {
                            if (User::where("username", $data["username"])->first()) {
                                return response()->json(["message" => "Lietotājvārds ir aizņemts"], 422);
                            } else {
                                $dbUser = new User(["username" => $data["username"], "name" => $data["name"], "surname" => $data["surname"], "password" => Hash::make($data["password"])]);
                                $dbUser->save();
                                $newProfile = new UserSchool(["school_id" => $id]);
                                $newProfile->assignRole("teacher");
                                $dbUser->userSchools()->save($newProfile);
                            }
                        }
                        DB::commit();
                        return response()->json(["message" => "Success"], 200);
                    } catch (Exception $e) {
                        DB::rollBack();
                        return response()->json(["message" => "Error"], 500);
                    }
                }
            }
        }
        return response()->json(["message" => 'Unauthorized'], 403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));
                $editableProfile = UserSchool::where("id", $id)->with('user')->first();
                if ($profile->user_id == Auth::id()) {
                    if ($profile->hasPermissionTo("edit all teacher profiles") || ($profile->hasPermissionTo("edit school teacher profiles") && $profile->school_id == $editableProfile->school_id)) {
                        if ($editableProfile->hasRole("teacher")) {
                            return response()->json(["message" => "success", "data" => $editableProfile], 200);
                        } else {
                            return response()->json(["message" => "error"], 422);
                        }
                    }
                }
            } catch (Exception $e) {
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (session("profileId")) {
            try {
                DB::beginTransaction();
                $profile = UserSchool::find(session("profileId"));
                $editableProfile = UserSchool::find($id);
                $editableAccount = User::find($editableProfile->user_id);

                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit all teacher profiles")
                        || ($profile->hasPermissionTo("edit school teacher profiles")
                            && $profile->school_id == $editableProfile->school_id)
                    ) {

                        $data = $request->all();

                        // Atjaunojamā skolotāja datu validācija
                        $data = $request->validate([
                            "username" => "required|alpha_dash|max:255",
                            "name" => "required|max:255",
                            "surname" => "required|max:255",
                            "password" => "nullable|max:255"
                        ]);

                        // Aizņemta lietotājvārda pārbaude
                        $usernameCheck = User::where('id', '<>', $editableAccount->id)->where('username', $data["username"])->get();

                        if (!$usernameCheck->isEmpty()) {
                            return response()->json(["message" => "username taken"], 422);
                        }

                        $editableAccount->username = $data["username"];
                        $editableAccount->name = $data["name"];
                        $editableAccount->surname = $data["surname"];

                        if (array_key_exists("password", $data)) {
                            $editableAccount->password = Hash::make($data["password"]);
                        }

                        $editableAccount->save();

                        DB::commit();
                        return response()->json(["message" => "success", "classId" => $editableProfile->class_id], 200);
                    }
                }
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (session("profileId")) {
            DB::beginTransaction();
            try {
                $profile = UserSchool::find(session("profileId"));
                $profileToDelete = UserSchool::find($id);

                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit all teacher profiles")
                        || ($profile->hasPermissionTo("edit school teacher profiles")
                            && $profile->school_id == $profileToDelete->school_id)
                    ) {

                        $schoolId = $profileToDelete->school_id;
                        $profileToDelete->delete();

                        DB::commit();
                        return response()->json(["message" => "success", "schoolId" => $schoolId], 200);
                    }
                }
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    // Funkcija, kas atgriež visus skolotājus, kuru vārds un uzvārds atbilst meklētajai frāzei
    public function searchTeachers(Request $request, $id)
    {
        $data = $request->all();
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));

                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit all teacher profiles")
                        || ($profile->hasPermissionTo("edit school teacher profiles")
                            && $profile->school_id == $id)
                    ) {

                        // JOIN vaicājums, kurš atrod visus skolotāju ierakstus, kur vārda un uzvāŗda apvienojums atbilst meklētajai frāzei
                        $teachers = DB::table('users')
                            ->selectRaw("users.name as name, users.surname, users.username, roles.name as role_name, users.id, user_schools.school_id, user_schools.id as user_id")
                            ->join('user_schools', 'users.id', '=', 'user_schools.user_id')
                            ->join('model_has_roles', 'user_schools.id', '=', 'model_has_roles.model_id')
                            ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
                            ->where('roles.name', '=', 'teacher')
                            ->where('school_id', '=', $id)
                            ->where(DB::raw("CONCAT(users.name, ' ', users.surname)"), "ilike", "%" . $data["student_name"] . "%")
                            ->get();

                        $teachers->mapWithKeys(function ($item, $key) {
                            return $item->user = ["id" => $item->user_id, "username" => $item->username, "name" => $item->name, "surname" => $item->surname];
                        });

                        return response()->json(["message" => "success", "data" => $teachers], 200);
                    }
                }
            } catch (Exception $e) {
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    // Skolotāju saraksts konsultācijas izveides formai
    public function indexForConsultation($id)
    {
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));
                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit all consultation records")
                        || ($profile->hasPermissionTo("edit school consultation records")
                            && $profile->school_id == $id)
                    ) {
                        $teachers = UserSchool::where("school_id", $id)->role("teacher")->with('user')->get();
                        return response()->json(["message" => "success", "data" => $teachers], 200);
                    }
                }
            } catch (Exception $e) {
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }
}
