<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserSchool;
use Illuminate\Support\Facades\Auth;
use App\Models\Room;
use Exception;
use Illuminate\Support\Facades\DB;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index($id)
    {
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));

                if ($profile->user_id == Auth::id()) {
                    if ($profile->hasPermissionTo("edit all room records") || ($profile->hasPermissionTo("edit school room records") && $profile->school_id == $id)) {
                        $rooms = Room::where("school_id", $id)->paginate(8);
                        return response()->json(["message" => "success", "data" => $rooms], 200);
                    }
                }
            } catch (Exception $e) {
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));
                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit all room records")
                        || ($profile->hasPermissionTo("edit school room records")
                            && $profile->school_id == $id)
                    ) {
                        return response()->json(["message" => "success"], 200);
                    }
                }
            } catch (Exception $e) {
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $data = $request->all();
        if (session("profileId")) {
            DB::beginTransaction();
            try {
                $profile = UserSchool::find(session("profileId"));
                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit all room records")
                        || ($profile->hasPermissionTo("edit school room records")
                            && $profile->school_id == $id)
                    ) {
                        if (Room::where("name", $data["name"])->where('school_id', '=', $id)->first()) {
                            return response()->json(["message" => "Telpa ar šādu nosaukumu jau eksistē"], 422);
                        }

                        $room = new Room(["name" => $data["name"], "seat_count" => $data["room_size"], "school_id" => $id]);
                        $room->save();
                        DB::commit();
                        return response()->json(["message" => "success"], 200);
                    }
                }
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));
                $room = Room::find($id);
                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit all room records")
                        || ($profile->hasPermissionTo("edit school room records")
                            && $profile->school_id == $room->school_id)
                    ) {
                        return response()->json(["message" => "success", "data" => $room], 200);
                    }
                }
            } catch (Exception $e) {
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        if (session("profileId")) {
            DB::beginTransaction();
            try {
                $profile = UserSchool::find(session("profileId"));
                $room = Room::find($id);
                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit all room records")
                        || ($profile->hasPermissionTo("edit school room records")
                            && $profile->school_id == $room->school_id)
                    ) {
                        if (Room::where("name", $data["room_name"])->where('school_id', '=', $room->school_id)->where("id", "<>", $id)->first()) {
                            return response()->json(["message" => "Telpa ar šādu nosaukumu jau eksistē"], 422);
                        }

                        $room->name = $data["room_name"];
                        $room->seat_count = $data["room_size"];
                        $room->save();
                        DB::commit();
                        return response()->json(["message" => "success", "schoolId" => $room->school_id], 200);
                    }
                }
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (session("profileId")) {
            DB::beginTransaction();
            try {
                $profile = UserSchool::find(session("profileId"));
                if ($profile->user_id == Auth::id()) {
                    $roomToDelete = Room::find($id);
                    $school = $roomToDelete->school_id;
                    if ($profile->hasPermissionTo("edit all room records") || ($profile->hasPermissionTo("edit school room records") && $profile->school_id == $roomToDelete->school_id)) {
                        $roomToDelete->delete();
                        DB::commit();
                        return response()->json(["message" => "success", "schoolId" => $school], 200);
                    }
                }
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => 'Unauthorized'], 403);
    }

    // Telpu meklēšana
    public function searchRooms(Request $request, $id)
    {
        $data = $request->all();
        if (session("profileId")) {
            $profile = UserSchool::find(session("profileId"));
            if ($profile->user_id == Auth::id()) {
                if ($profile->hasPermissionTo("edit all room records") || ($profile->hasPermissionTo("edit school room records") && $profile->school_id == $id)) {
                    try {
                        // Tiek atgriezta kolekcija ar telpām, kurām nosaukums atbilst meklētajai frāzei
                        $rooms = Room::where("school_id", $id)->where("name", "ilike", "%" . $data["room_name"] . "%")->paginate(8);
                        return response()->json(["message" => "success", "data" => $rooms], 200);
                    } catch (Exception $e) {
                        return response()->json(["message" => "error"], 500);
                    }
                }
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    // Metode, kura atgriež telpu sarakstu, lai to varētu attēlot iekš konsultāciju izveides un labošanas formas.
    public function indexForConsultation($id)
    {
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));
                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit all room records")
                        || ($profile->hasPermissionTo("edit school room records")
                            && $profile->school_id == $id)
                        || ($profile->hasPermissionTo("edit personal consultation records")
                            && $profile->school_id == $id)
                    ) {
                        $rooms = Room::where("school_id", $id)->get();
                        return response()->json(["message" => "success", "data" => $rooms], 200);
                    }
                }
            } catch (Exception $e) {
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }
}
