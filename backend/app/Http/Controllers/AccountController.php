<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\UserSchool;
use Illuminate\Database\Eloquent\Builder;

class AccountController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    //  Tiek iegūta informācija par lietotāju, kurš šobrīd ir pieslēdzies sistēmai.
    //  Šī informācija tiek attēlota formā. Var labot tikai paroli, ja lietotājam nav sistēmas administratora profils.
    public function edit()
    {
        try {
            $editableUser = User::find(Auth::id());
            $adminProfiles = DB::table('user_schools')
                ->select('user_schools.id')
                ->join('model_has_roles', "user_schools.id", "=", 'model_has_roles.model_id')
                ->join('roles', "model_has_roles.role_id", "=", "roles.id")
                ->where('roles.name', '=', 'system_admin')
                ->where('user_schools.user_id', '=', Auth::id())
                ->get();

            return response()->json(["message" => "success", "data" => $editableUser, "editName" => !$adminProfiles->isEmpty()], 200);
        } catch (Exception $e) {
            return response()->json(["message" => "error"], 500);
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    // Konta datu atjaunošana datubāzē.
    public function update(Request $request)
    {

        DB::beginTransaction();
        try {
            // Tiek iegūti dati par piesaistītajiem sistēmas administratora profiliem
            $editableUser = User::find(Auth::id());
            $adminProfiles = DB::table('user_schools')
                ->select('user_schools.id')
                ->join('model_has_roles', "user_schools.id", "=", 'model_has_roles.model_id')
                ->join('roles', "model_has_roles.role_id", "=", "roles.id")
                ->where('roles.name', '=', 'system_admin')
                ->where('user_schools.user_id', '=', Auth::id())
                ->get();

            $data = $request->all();
            if ($data["passwordChange"]) {
                $data = $request->validate([
                    "username" => "required|alpha_dash|max:255",
                    "name" => "required|max:255",
                    "surname" => "required|max:255",
                    "password" => "required|max:255",
                    "passwordChange" => "required"
                ]);
            } else {
                $data = $request->validate([
                    "username" => "required|alpha_dash|max:255",
                    "name" => "required|max:255",
                    "surname" => "required|max:255",
                    "passwordChange" => "required"
                ]);
            }

            // Ieraksti par kontiem ar tādu pašu lietotājvārdu
            $usernameCheck = User::where('id', '<>', Auth::id())->where('username', $data["username"])->get();

            // Ja eksistē konti ar tādu lietotājvārdu, tad lietotājs par to tiek brīdināts
            if (!$usernameCheck->isEmpty()) {
                return response()->json(["message" => "username taken"], 422);
            }

            // Ja eksistē sistēmas administratora profils, tad tiek nomainīts vārds, uzvārds, lietotājvārds
            if (!$adminProfiles->isEmpty()) {
                $editableUser->username = $data["username"];
                $editableUser->name = $data["name"];
                $editableUser->surname = $data["surname"];
            }

            // Paroles maiņa
            if ($data["passwordChange"]) {
                $editableUser->password = Hash::make($data["password"]);
            }

            $editableUser->save();
            DB::commit();

            return response()->json(["message" => "success", "data" => $editableUser, "editName" => !$adminProfiles->isEmpty()], 200);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->json(["message" => "error"], 500);
        }
        return response()->json(["message" => "unauthorized"], 403);
    }
}
