<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserSchool;
use Illuminate\Support\Facades\Auth;
use App\Models\Consultation;
use Carbon\Carbon;
use DateTime;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class ConsultationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id)
    {
        if (session("profileId")) {
            try {
                $data = $request->all();
                $profile = UserSchool::find(session("profileId"));
                if ($profile->user_id == Auth::id()) {

                    // Gadījums, ja lietotājs ir administrators.
                    if (
                        $profile->hasPermissionTo("edit all consultation records")
                        || ($profile->hasPermissionTo("edit school consultation records")
                            && $profile->school_id == $id)
                    ) {
                        $consultations = Consultation::with(['teacher.user', 'room'])->whereHas('room', function (Builder $query) use ($id) {
                            $query->whereHas('school', function (Builder $roomQuery) use ($id) {
                                $roomQuery->where('school_id', $id);
                            })->with('room');
                        });

                        // Tiek veikta salīdzināšana, ja lietotājs ir izvēlējies rādīt tikai šodien vai vēlāk notiekošās konsultācijas
                        if (!$data["allConsultations"]) {
                            $consultations = $consultations->where('date', '>=', Carbon::now()->toDateString());
                        }

                        $consultations = $consultations->paginate(8);
                        return response()->json(["message" => "success", "data" => $consultations, "access" => "all"], 200);

                        // Pārējie gadījumi, kur profilam piesaistītā skola atbilst konsultācijā norādītajai
                    } else if (
                        $profile->school_id == $id
                    ) {
                        $consultations = Consultation::with(['teacher.user', 'room'])->whereHas('room', function (Builder $query) use ($id) {
                            $query->whereHas('school', function (Builder $roomQuery) use ($id) {
                                $roomQuery->where('school_id', $id);
                            })->with('room');
                        });

                        if (!$data["allConsultations"]) {
                            $consultations = $consultations->where('date', '>=', Carbon::now()->toDateString());
                        }

                        $consultations = $consultations->paginate(8);

                        // Īpašība, kas lietotāja pusē attēlos dzēšanas pogu
                        $consultations->mapWithKeys(function ($item, $key) use ($profile) {
                            $item->edit = ($item->teacher->id == $profile->id);

                            return $item;
                        });

                        return response()->json([
                            "message" => "success",
                            "data" => $consultations,
                            "access" => ($profile->hasPermissionTo("edit personal consultation records") ? "personal" : "none")
                        ], 200);
                    }
                }
            } catch (Exception $e) {
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    //  Tiesību pārbaude konsultāciju izveidei
    public function create($id)
    {
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));
                if ($profile->user_id == Auth::id()) {

                    // Atbildē tiek norādīts vai var izvēlēties konsultācijas skolotāju
                    if (
                        $profile->hasPermissionTo("edit all consultation records")
                        || ($profile->hasPermissionTo("edit school consultation records")
                            && $profile->school_id == $id)
                    ) {
                        return response()->json(["message" => "success", "pick_teacher" => true], 200);
                    } else if (
                        $profile->hasPermissionTo("edit personal consultation records")
                        && $profile->school_id == $id
                    ) {
                        // Skolotajs konsultācijā nevar izvēlēties citu skolotāju
                        return response()->json(["message" => "success", "pick_teacher" => false], 200);
                    }
                }
            } catch (Exception $e) {
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));
                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit all consultation records")
                        || ($profile->hasPermissionTo("edit school consultation records")
                            && $profile->school_id == $id)
                    ) {
                        DB::beginTransaction();

                        $data = $request->all();

                        $data = $request->validate([
                            "available_seats" => "required|integer|max:10000",
                            "room" => "required|integer",
                            "start_date" => "required|max:255",
                            "teacher" => "required|max:255",
                            "topic" => "required|max:255"
                        ]);

                        $newConsultation = new Consultation(
                            [
                                "teacher_id" => $data["teacher"],
                                "room_id" => $data["room"],
                                "date" => $data["start_date"],
                                "attendant_count" => $data["available_seats"],
                                "topic" => $data["topic"]
                            ]
                        );

                        $newConsultation->save();

                        DB::commit();
                        return response()->json(["message" => "Success"], 200);
                    } else if (
                        $profile->hasPermissionTo("edit personal consultation records")
                        && $profile->school_id == $id
                    ) {
                        DB::beginTransaction();

                        $data = $request->all();

                        $data = $request->validate([
                            "available_seats" => "required|integer|max:10000",
                            "room" => "required|integer",
                            "start_date" => "required|max:255",
                            "topic" => "required|max:255"
                        ]);

                        // Ja konsultāciju veido skolotājs, tad kā skolotāja id tiek ielikts lietotāja profila id
                        $newConsultation = new Consultation(
                            [
                                "teacher_id" => $profile->id,
                                "room_id" => $data["room"],
                                "date" => $data["start_date"],
                                "attendant_count" => $data["available_seats"],
                                "topic" => $data["topic"]
                            ]
                        );
                        $newConsultation->save();

                        DB::commit();

                        return response()->json(["message" => "Success"], 200);
                    }
                }
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(["message" => "Error"], 500);
            }
        }
        return response()->json(["message" => 'Unauthorized'], 403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));
                $editableConsultation = Consultation::where("id", $id)->with(['teacher', 'room'])->first();
                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit all consultation records")
                        || ($profile->hasPermissionTo("edit school consultation records")
                            && $profile->school_id == $editableConsultation->teacher->school_id)
                        || ($profile->hasPermissionTo("edit personal consultation records")
                            && $profile->id == $editableConsultation->teacher_id)
                    ) {
                        return response()->json(["message" => "success", "data" => $editableConsultation], 200);
                    }
                }
            } catch (Exception $e) {
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        if (session("profileId")) {
            DB::beginTransaction();
            try {
                $profile = UserSchool::find(session("profileId"));
                $consultation = Consultation::where("id", $id)->with(['teacher', 'room'])->first();
                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit all consultation records")
                        || ($profile->hasPermissionTo("edit school consultation records")
                            && $profile->school_id == $consultation->teacher->school_id)
                        || ($profile->hasPermissionTo("edit personal consultation records")
                            && $profile->id == $consultation->teacher_id)
                    ) {
                        // Konsultācijai netiek mainīts piesaistītais skolotājs
                        $consultation->topic = $data["topic"];
                        $consultation->room_id = $data["room"];
                        $consultation->attendant_count = $data["available_seats"];
                        $consultation->date = $data["start_date"];
                        $consultation->save();
                        DB::commit();
                        return response()->json(["message" => "success", "schoolId" => $consultation->teacher->school_id], 200);
                    }
                }
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (session("profileId")) {
            DB::beginTransaction();
            try {
                $profile = UserSchool::find(session("profileId"));
                if ($profile->user_id == Auth::id()) {
                    $consultationToDelete = Consultation::with('teacher')->where("id", $id)->first();
                    $school = $consultationToDelete->teacher->school_id;
                    if (
                        $profile->hasPermissionTo("edit all consultation records")
                        || ($profile->hasPermissionTo("edit school consultation records")
                            && $profile->school_id == $consultationToDelete->teacher->school_id)
                        || ($profile->hasPermissionTo("edit personal consultation records")
                            && $profile->id == $consultationToDelete->teacher_id)
                    ) {
                        $consultationToDelete->delete();
                        DB::commit();
                        return response()->json(["message" => "success", "schoolId" => $school], 200);
                    }
                }
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => 'Unauthorized'], 403);
    }


    // Konsultāciju meklēšana
    public function searchConsultations(Request $request, $id)
    {
        $data = $request->all();
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));

                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit all consultation records")
                        || ($profile->hasPermissionTo("edit school consultation records")
                            && $profile->school_id == $id)
                    ) {
                        // Tiek iegūti dati par konsultācijām. kurām lietotāja vārds, uzvārds vai konsultācijas tēma atbilst meklētajai frāzei
                        $consultations = DB::table('consultations')
                            ->selectRaw("rooms.name as room_name, rooms.seat_count, consultations.date, consultations.attendant_count, consultations.topic, user_schools.school_id, user_schools.class_id, users.name, users.surname")
                            ->join('rooms', 'rooms.id', '=', 'consultations.room_id')
                            ->join('user_schools', 'user_schools.id', '=', 'consultations.teacher_id')
                            ->join('users', 'user_schools.user_id', '=', 'users.id')
                            ->join('model_has_roles', 'user_schools.id', '=', 'model_has_roles.model_id')
                            ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
                            ->where(function ($query) use ($data) {
                                $query->where(DB::raw("CONCAT(users.name, ' ', users.surname)"), "ilike", "%" . $data["consultation_name"] . "%")
                                    ->orWhere("consultations.topic", "ilike", "%" . $data["consultation_name"] . "%");
                            })
                            ->where('rooms.school_id', '=', $id)
                            ->paginate(8);

                        // Iegūto datu pārveidošana lietotāja pusē apstrādājamā formātā
                        $consultations->mapWithKeys(function ($item, $key) {
                            $item->teacher = [
                                "school_id" => $item->school_id,
                                "class_id" => $item->class_id,
                                "user" => [
                                    "name" => $item->name,
                                    "surname" => $item->surname
                                ],
                            ];

                            $item->room = [
                                "name" => $item->room_name,
                                "seat_count" => $item->seat_count
                            ];

                            return $item;
                        });

                        return response()->json(["message" => "success", "data" => $consultations], 200);
                    } else {
                        $consultations = DB::table('consultations')
                            ->selectRaw("rooms.name as room_name, rooms.seat_count, consultations.date, consultations.attendant_count, consultations.topic, user_schools.id as teacher_id, user_schools.school_id, user_schools.class_id, users.name, users.surname")
                            ->join('rooms', 'rooms.id', '=', 'consultations.room_id')
                            ->join('user_schools', 'user_schools.id', '=', 'consultations.teacher_id')
                            ->join('users', 'user_schools.user_id', '=', 'users.id')
                            ->join('model_has_roles', 'user_schools.id', '=', 'model_has_roles.model_id')
                            ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
                            ->where(function ($query) use ($data) {
                                $query->where(DB::raw("CONCAT(users.name, ' ', users.surname)"), "ilike", "%" . $data["consultation_name"] . "%")
                                    ->orWhere("consultations.topic", "ilike", "%" . $data["consultation_name"] . "%");
                            })
                            ->where('rooms.school_id', '=', $id)
                            ->paginate(8);

                        $consultations->mapWithKeys(function ($item, $key) use ($profile) {
                            $item->teacher = [
                                "school_id" => $item->school_id,
                                "class_id" => $item->class_id,
                                "user" => [
                                    "name" => $item->name,
                                    "surname" => $item->surname
                                ],
                            ];

                            $item->room = [
                                "name" => $item->room_name,
                                "seat_count" => $item->seat_count
                            ];

                            // Ja lietotājs, nav administrators, tad tiek pielikta īpašība, 
                            // kas attēlos dzēšanas pogu, ja lietotājs ir izveidojis konsultāciju.
                            $item->edit = ($item->teacher_id == $profile->id);

                            return $item;
                        });

                        return response()->json(["message" => "success", "data" => $consultations], 200);
                    }
                }
            } catch (Exception $e) {
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }
}
