<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Auth;
use App\Models\UserSchool;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class SchoolAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // Saraksts ar skolas administratoriem
    public function index($id)
    {
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));
                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit school admin accounts")
                    ) {
                        $admins = UserSchool::where("school_id", $id)->role("school_admin")->with('user')->get();
                        return response()->json(["message" => "success", "data" => $admins], 200);
                    }
                }
            } catch (Exception $e) {
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));
                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit school admin accounts")
                    ) {
                        return response()->json(["message" => "success"], 200);
                    }
                }
            } catch (Exception $e) {
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        if (session("profileId")) {
            $profile = UserSchool::find(session("profileId"));
            if ($profile->user_id == Auth::id()) {
                if (
                    $profile->hasPermissionTo("edit school admin accounts")
                ) {
                    DB::beginTransaction();
                    try {

                        $data = $request->all();

                        // Ja tiek veidots jauns konts, tad tiek veikta plašāka datu validācija
                        if (array_key_exists("newUser", $request->all())) {
                            if ($data["newUser"]) {
                                $data = $request->validate([
                                    "username" => "required|alpha_dash|max:255",
                                    "name" => "required|max:255",
                                    "surname" => "required|max:255",
                                    "password" => "required|max:255",
                                    "newUser" => "required|boolean"
                                ]);

                                // Tiek pārbaudīts, vai lietotājvārds jau nav aizņemts
                                $usernameCheck = User::where('username', $data["username"])->get();

                                if (!$usernameCheck->isEmpty()) {
                                    return response()->json(["message" => "Username taken"], 422);
                                }
                            } else {
                                // 
                                $data = $request->validate([
                                    "username" => "required|alpha_dash|max:255",
                                    "newUser" => "required|boolean"
                                ]);
                            }
                        } else {
                            return response()->json(["message" => "Missing value"], 422);
                        }

                        // Ja tiek piesaistīts jauns profils jau esošam kontam, tad tiek veikta pārbaude vai tāds konts eksistē
                        if (!$data["newUser"]) {
                            $dbUser = User::where("username", $data["username"])->first();
                            if (!$dbUser) {
                                return response()->json(["message" => "Lietotājs nav atrasts"], 404);
                            } else {
                                $newProfile = new UserSchool(["school_id" => $id]);
                                $newProfile->assignRole("school_admin");
                                $dbUser->userSchools()->save($newProfile);
                            }
                        } else {
                            if (User::where("username", $data["username"])->first()) {
                                return response()->json(["message" => "Lietotājvārds ir aizņemts"], 422);
                            } else {
                                $dbUser = new User(["username" => $data["username"], "name" => $data["name"], "surname" => $data["surname"], "password" => Hash::make($data["password"])]);
                                $dbUser->save();
                                $newProfile = new UserSchool(["school_id" => $id]);
                                $newProfile->assignRole("school_admin");
                                $dbUser->userSchools()->save($newProfile);
                            }
                        }
                        DB::commit();
                        return response()->json(["message" => "Success"], 200);
                    } catch (Exception $e) {
                        DB::rollBack();
                        return response()->json(["message" => "Error"], 500);
                    }
                }
            }
        }
        return response()->json(["message" => 'Unauthorized'], 403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));
                $editableProfile = UserSchool::where("id", $id)->with('user')->first();
                if ($profile->user_id == Auth::id()) {
                    if ($profile->hasPermissionTo("edit school admin accounts")) {
                        if ($editableProfile->hasRole("school_admin")) {
                            return response()->json(["message" => "success", "data" => $editableProfile], 200);
                        } else {
                            return response()->json(["message" => "error"], 422);
                        }
                    }
                }
            } catch (Exception $e) {
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (session("profileId")) {
            try {
                DB::beginTransaction();
                $profile = UserSchool::find(session("profileId"));
                $editableProfile = UserSchool::find($id);
                $editableAccount = User::find($editableProfile->user_id);

                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit school admin accounts")
                    ) {
                        // Formas datu pārbaude
                        $data = $request->validate([
                            "username" => "required|alpha_dash|max:255",
                            "name" => "required|max:255",
                            "surname" => "required|max:255",
                            "password" => "nullable"
                        ]);

                        // Tiek pārbaudīts vai eksistē cits lietotājs ar norādīto e-pastu
                        $usernameCheck = User::where('id', '<>', $editableAccount->id)->where('username', $data["username"])->get();

                        if (!$usernameCheck->isEmpty()) {
                            return response()->json(["message" => "username taken"], 422);
                        }


                        $editableAccount->username = $data["username"];
                        $editableAccount->name = $data["name"];
                        $editableAccount->surname = $data["surname"];

                        if (array_key_exists("password", $data)) {
                            $editableAccount->password = Hash::make($data["password"]);
                        }

                        $editableAccount->save();

                        DB::commit();
                        return response()->json(["message" => "success", "schoolId" => $editableProfile->school_id], 200);
                    }
                }
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (session("profileId")) {
            DB::beginTransaction();
            try {
                $profile = UserSchool::find(session("profileId"));
                $profileToDelete = UserSchool::find($id);

                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit school admin accounts")
                    ) {

                        $schoolId = $profileToDelete->school_id;
                        $profileToDelete->delete();

                        DB::commit();
                        return response()->json(["message" => "success", "schoolId" => $schoolId], 200);
                    }
                }
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    // Funkcija, kas atgriež visus skolas administratorus, kuru vārds un uzvārds atbilst meklētajai frāzei
    public function searchAdmins(Request $request, $id)
    {
        $data = $request->all();
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));

                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit school admin accounts")
                    ) {

                        // JOIN vaicājums, kurš atrod visus skolotāju ierakstus, kur vārda un uzvāŗda apvienojums atbilst meklētajai frāzei
                        $admins = DB::table('users')
                            ->selectRaw("users.name as name, users.surname, users.username, roles.name as role_name, users.id, user_schools.school_id, user_schools.id as user_id")
                            ->join('user_schools', 'users.id', '=', 'user_schools.user_id')
                            ->join('model_has_roles', 'user_schools.id', '=', 'model_has_roles.model_id')
                            ->join('roles', 'roles.id', '=', 'model_has_roles.role_id')
                            ->where('school_id', '=', $id)
                            ->where('roles.name', '=', 'school_admin')
                            ->where(DB::raw("CONCAT(users.name, ' ', users.surname)"), "ilike", "%" . $data["student_name"] . "%")
                            ->get();

                        $admins->mapWithKeys(function ($item, $key) {
                            return $item->user = ["id" => $item->user_id, "username" => $item->username, "name" => $item->name, "surname" => $item->surname];
                        });

                        return response()->json(["message" => "success", "data" => $admins], 200);
                    }
                }
            } catch (Exception $e) {
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }
}
