<?php

namespace App\Http\Controllers;

use App\Models\School;
use Illuminate\Http\Request;
use App\Models\UserSchool;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class SchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (session("profileId")) {
            $profile = UserSchool::find(session("profileId"));
            if ($profile->user_id == Auth::id()) {
                if ($profile->hasPermissionTo("edit school records")) {
                    try {
                        $schools = School::paginate(8);
                        return response()->json(["message" => "success", "data" => $schools], 200);
                    } catch (Exception $e) {
                        return response()->json(["message" => "error"], 500);
                    }
                }
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }


    public function create() {
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));
                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit school records")
                    ) {
                        return response()->json(["message" => "success"], 200);
                    }
                }
            } catch (Exception $e) {
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (session("profileId")) {
            $profile = UserSchool::find(session("profileId"));
            if ($profile->user_id == Auth::id()) {
                if ($profile->hasPermissionTo("edit school records")) {
                    DB::beginTransaction();
                    try {

                        $data = $request->all();

                        // Datu validācija formas datiem. Tiek pārbaudīts, vai tiek veidots jauns konts skolas administratoram.
                        if (array_key_exists("newUser", $request->all())) {
                            if ($data["newUser"]) {
                                $data = $request->validate([
                                    "school_name" => "required|max:255",
                                    "username" => "required|alpha_dash|max:255",
                                    "name" => "required|max:255",
                                    "surname" => "required|max:255",
                                    "password" => "required|max:255",
                                    "newUser" => "required|boolean"
                                ]);
                            } else {
                                $data = $request->validate([
                                    "school_name" => "required|max:255",
                                    "username" => "required|alpha_dash|max:255",
                                    "newUser" => "required|boolean"
                                ]);
                            }
                        } else {
                            // Ja trūkst jauna lietotāja boolean vērtība, tad par to tiek brīdināts lietotājs.
                            return response()->json(["message" => "Missing value"], 422);
                        }

                        // Gadījumā, ja skola ar identisku nosaukumu jau eksistē, tad tiek atgriezts paziņojums lietotājam un skola netiek saglabāta.
                        if (School::where("name", $data["school_name"])->first()) {
                            return response()->json(["message" => "Skola ar šādu nosaukumu jau eksistē"], 422);
                        }

                        // Gadījums, kad eksistējošam kontam tiek pievienots jauns profils
                        if (!$data["newUser"]) {
                            $dbUser = User::where("username", $data["username"])->first();
                            if (!$dbUser) {
                                return response()->json(["message" => "Lietotājs nav atrasts"], 404);
                            } else {
                                $newSchool = new School(["name" => $data["school_name"]]);
                                $newSchool->save();
                                $newProfile = new UserSchool(["school_id" => $newSchool->id]);
                                $newProfile->assignRole("school_admin");
                                $dbUser->userSchools()->save($newProfile);
                            }
                            // Tiek veidots jauns konts
                        } else {
                            if (User::where("username", $data["username"])->first()) {
                                return response()->json(["message" => "Lietotājvārds jau aizņemts"], 422);
                            } else {
                                $dbUser = new User(["username" => $data["username"], "name" => $data["name"], "surname" => $data["surname"], "password" => Hash::make($data["password"])]);
                                $newSchool = new School(["name" => $data["school_name"]]);
                                $newSchool->save();
                                $dbUser->save();
                                $newProfile = new UserSchool(["school_id" => $newSchool->id]);
                                $newProfile->assignRole("school_admin");
                                $dbUser->userSchools()->save($newProfile);
                            }
                        }
                        DB::commit();
                        return response()->json(["message" => "Success"], 200);
                    } catch (Exception $e) {
                        DB::rollBack();
                        return response()->json(["message" => "Error"], 500);
                    }
                }
            }
        }
        return response()->json(["message" => 'Unauthorized'], 403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));
                $school = School::find($id);
                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit school records")
                    ) {
                        return response()->json(["message" => "success", "data" => $school], 200);
                    }
                }
            } catch (Exception $e) {
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        if (session("profileId")) {
            $profile = UserSchool::find(session("profileId"));
            if ($profile->user_id == Auth::id()) {
                if ($profile->hasPermissionTo("edit school records")) {
                    DB::beginTransaction();
                    try {
                        //  Tiek pārbaudīts vai jau eksistē skola ar identisku nosaukumu
                        if (School::where("name", $data["school_name"])->where('id', '<>', $id)->first()) {
                            return response()->json(["message" => "Skola ar šādu nosaukumu jau eksistē"], 422);
                        }

                        $school = School::find($id);
                        $school->name = $data["school_name"];
                        $school->save();
                        DB::commit();
                        return response()->json(["message" => "success"], 200);
                    } catch (Exception $e) {
                        DB::rollBack();
                        return response()->json(["message" => "error"], 500);
                    }
                }
            }
        }
        return response()->json(["message" => 'Unauthorized'], 403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (session("profileId")) {
            DB::beginTransaction();
            try {
                $profile = UserSchool::find(session("profileId"));
                if ($profile->user_id == Auth::id()) {
                    if ($profile->hasPermissionTo("edit school records")) {
                        $schoolToDelete = School::find($id);
                        $schoolToDelete->delete();
                        DB::commit();
                        return response()->json(["message" => "success"], 200);
                    }
                }
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => 'Unauthorized'], 403);
    }

    // Skolu meklēšana pēc konkrētas frāzes
    public function searchSchools(Request $request)
    {
        $data = $request->all();

        if (session("profileId")) {
            $profile = UserSchool::find(session("profileId"));
            if ($profile->user_id == Auth::id()) {
                if ($profile->hasPermissionTo("edit school records")) {
                    try {
                        // Tiek atgrieztas skolas, kurām nosaukums atbilst meklētajai frāzei
                        $schools = School::where("name", "ilike", "%" . $data["school_name"] . "%")->paginate(8);
                        return response()->json(["message" => "success", "data" => $schools], 200);
                    } catch (Exception $e) {
                        return response()->json(["message" => "error"], 500);
                    }
                }
            }
        }
        return response()->json(["message" => 'Unauthorized'], 403);
    }
}
