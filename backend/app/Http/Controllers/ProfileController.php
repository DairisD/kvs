<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\UserSchool;
use Exception;
use Illuminate\Database\Eloquent\Builder;

class ProfileController extends Controller
{

    // Lietotāja profilu iegūšana
    public function index()
    {
        session(["profileId" => null]);

        $profiles = UserSchool::with("user", "roles", "school", "class")->where('user_id', Auth::id())->get();

        // 
        $profiles = $profiles->map(function ($p) {
            // Tiek pārveidota struktūra, lai to varētu ērti apstrādāt lietotāja pusē.
            $p->role = $p->roles["0"]->name;
            return $p;
        });

        return response()->json(["message" => "Success", "data" => $profiles], 200);
    }

    // Profila izvēles funkcija
    public function selectProfile(Request $request)
    {
        try {
            $data = $request->all();

            $profile = UserSchool::where("id", $data["id"]);

            if ($profile->get()->isNotEmpty()) {
                if ($profile->first()->user_id === Auth::id()) {

                    // Izvēlētā profila ID tiek saglabāts sesijas glabātuvē
                    session(["profileId" => $data["id"]]);

                    // Profila tiesību ieguve
                    $permissions = $profile->first()->getAllPermissions()->map(function ($item, $key) {
                        return $item->name;
                    });

                    // Lietotāja lomas iegūšana
                    $role = $profile->first()->getRoleNames()[0];
                    return response()->json(
                        [
                            'message' => 'Success',
                            "data" => [
                                "permissions" => $permissions,
                                "role" => $role,
                                "school_id" => $profile->first()->school_id,
                                "class_id" => $profile->first()->class_id
                            ]
                        ],
                        200
                    );
                }
            }
            return response()->json(["message" => 'Unauthorized'], 403);
        } catch (Exception $e) {
            return response()->json(["message" => 'Error'], 500);
        }
    }
}
