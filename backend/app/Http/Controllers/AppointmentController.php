<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Consultation;
use Exception;
use Illuminate\Support\Facades\Auth;
use App\Models\UserSchool;
use App\Models\Appointment;
use Illuminate\Support\Facades\DB;


class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // Konkrētas konsultācijas ierakstu iegūšana
    public function index(Request $request, $id)
    {
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));
                if ($profile->user_id == Auth::id()) {

                    // Tiek iegūts jau esošs pieraksts, ja tāds ir
                    $existingAppointment = Appointment::where("student_id", $profile->id)->where("consultation_id", $id)->first();
                    $consultation = Consultation::with('room')->where('id', $id)->first();
                    if (
                        $profile->hasPermissionTo("edit all appointment records")
                        || ($profile->hasPermissionTo("edit school appointment records")
                            && $profile->school_id == $consultation->room->school_id)
                    ) {

                        // Pieteikumu iegūšana, ja lietotājs ir administrators (sistēmas, skolas)
                        $appointments = Appointment::with(['student.user', 'student.class'])->where('consultation_id', $id)->paginate(8);
                        return response()->json(['message' => "success", "data" => $appointments, "delete" => "all", "appointment_exists" => !!$existingAppointment]);
                    } else if ($profile->hasPermissionTo("edit personal appointment records") && $profile->school_id == $consultation->room->school_id) {

                        // Pieteikumu iegūšana, ja lietotājs ir skolotājs vai skolēns
                        $appointments = Appointment::with(['student.user', 'student.class'])->where('consultation_id', $id)->paginate(8);
                        $appointments->mapWithKeys(function ($appointment, $key) use ($profile) {
                            $appointment->edit = $appointment->student_id == $profile->id;
                            return $appointment;
                        });

                        return response()->json(['message' => "success", "data" => $appointments, "delete" => "personal", "appointment_exists" => !!$existingAppointment]);
                    }
                }
            } catch (Exception $e) {
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    // Tiesību pārbaude lietotājam, kurš vēlas izveidot jaunu konsultāciju
    public function create($id)
    {
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));
                $consultation = Consultation::with('teacher')->where("id", $id)->first();
                if ($profile->user_id == Auth::id()) {
                    if (
                        $profile->hasPermissionTo("edit all appointment records")
                        || ($profile->hasPermissionTo("edit school appointment records")
                            && $profile->school_id == $consultation->teacher->school_id)
                        || ($profile->hasPermissionTo("edit personal appointment records")
                            && $profile->school_id == $consultation->teacher->school_id)
                    ) {
                        return response()->json(["message" => "success"], 200);
                    }
                }
            } catch (Exception $e) {
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request, $id)
    {
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));
                $consultation = Consultation::with('teacher')->where('id', $id)->first();
                if ($profile->user_id == Auth::id()) {
                    $existingAppointment = Appointment::where("student_id", $profile->id)->where("consultation_id", $id)->first();
                    if (!!!$existingAppointment) {
                        if (
                            $profile->hasPermissionTo("edit all appointment records")
                            || ($profile->hasPermissionTo("edit school appointment records")
                                && $profile->school_id == $consultation->teacher->school_id)
                            || ($profile->hasPermissionTo("edit personal appointment records")
                                && $profile->school_id == $consultation->teacher->school_id)
                        ) {

                            //  Ja neeksistē pieteikums uz konkrēto konsultāciju no konkrētā lietotāja,
                            //  tad tiek izveidots jauns pieteikuma ieraksts.
                            DB::beginTransaction();

                            $data = $request->all();

                            $data = $request->validate([
                                "reason" => "required|max:255"
                            ]);

                            $newAppointment = new Appointment(
                                [
                                    "consultation_id" => $id,
                                    "student_id" => $profile->id,
                                    "reason" => $data["reason"],
                                ]
                            );

                            $newAppointment->save();

                            DB::commit();
                            return response()->json(["message" => "Success"], 200);
                        }
                    } else {
                        return response()->json(["message" => "Konsultācijas pieteikums jau eksistē!"], 422);
                    }
                }
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(["message" => "Error"], 500);
            }
        }
        return response()->json(["message" => 'Unauthorized'], 403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (session("profileId")) {
            DB::beginTransaction();
            try {
                $profile = UserSchool::find(session("profileId"));
                if ($profile->user_id == Auth::id()) {

                    // Dzēšanamais pieraksts
                    $appointmentToDelete = Appointment::with(['student', 'consultation.room'])->where("id", $id)->first();
                    if (
                        $profile->hasPermissionTo("edit all appointment records")
                        || ($profile->hasPermissionTo("edit school appointment records")
                            && $profile->school_id == $appointmentToDelete->consultation->room->school_id)
                        || ($profile->hasPermissionTo("edit personal appointment records")
                            && $profile->id == $appointmentToDelete->student->id)
                    ) {
                        $appointmentToDelete->delete();
                        DB::commit();
                        return response()->json(["message" => "success"], 200);
                    }
                }
            } catch (Exception $e) {
                DB::rollBack();
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => 'Unauthorized'], 403);
    }

    // Pierakstu meklēšana pēc iemesla vai personas vārda un uzvārda
    public function searchAppointments(Request $request, $id)
    {
        $data = $request->all();
        if (session("profileId")) {
            try {
                $profile = UserSchool::find(session("profileId"));
                $consultation = Consultation::with('teacher')->where('id', $id)->first();
                if ($profile->user_id == Auth::id()) {

                    // Gadījums, ja pierakstu meklē administratori
                    if (
                        $profile->hasPermissionTo("edit all appointment records")
                        || ($profile->hasPermissionTo("edit school appointment records")
                            && $profile->school_id == $consultation->teacher->school_id)
                    ) {

                        // Tiek iegūti pieraksti, kuri ir piesaistīti konkrētajai konsultācijai un satur lietotāja vārdā,
                        //  uzvārdā vai iemeslā meklējamo frāzi
                        $appointments = DB::table('appointments')
                            ->selectRaw("appointments.id as id, appointments.consultation_id, appointments.student_id, appointments.reason, users.name as name, users.surname as surname, classes.name as class_name")
                            ->join('consultations', 'consultations.id', '=', 'appointments.consultation_id')
                            ->join('user_schools', 'user_schools.id', '=', 'appointments.student_id')
                            ->join('classes', 'classes.id', '=', 'user_schools.class_id')
                            ->join('users', 'user_schools.user_id', '=', 'users.id')
                            ->where(function ($query) use ($data) {
                                $query->where(DB::raw("CONCAT(users.name, ' ', users.surname)"), "ilike", "%" . $data["appointment_name"] . "%")
                                    ->orWhere("appointments.reason", "ilike", "%" . $data["appointment_name"] . "%");
                            })
                            ->where('consultations.id', '=', $id)
                            ->paginate(8);

                        // Dati tiek pārveidoti klienta pusē apstrādājamā konstrukcijā
                        $appointments->mapWithKeys(function ($item, $key) {
                            $item->student = [
                                "user" => [
                                    "name" => $item->name,
                                    "surname" => $item->surname
                                ],
                                "class" => [
                                    "name" => $item->class_name
                                ]
                            ];

                            return $item;
                        });

                        return response()->json(["message" => "success", "data" => $appointments, "delete" => "all"], 200);

                        // Ja peirakstu meklē skolēns vai skolotājs
                    } else if ($profile->school_id == $consultation->teacher->school_id) {
                        $appointments = DB::table('appointments')
                            ->selectRaw("appointments.id as id, appointments.consultation_id, appointments.student_id, appointments.reason, users.name as name, users.surname as surname, classes.name as class_name")
                            ->join('consultations', 'consultations.id', '=', 'appointments.consultation_id')
                            ->join('user_schools', 'user_schools.id', '=', 'appointments.student_id')
                            ->join('classes', 'classes.id', '=', 'user_schools.class_id')
                            ->join('users', 'user_schools.user_id', '=', 'users.id')
                            ->where(function ($query) use ($data) {
                                $query->where(DB::raw("CONCAT(users.name, ' ', users.surname)"), "ilike", "%" . $data["appointment_name"] . "%")
                                    ->orWhere("appointments.reason", "ilike", "%" . $data["appointment_name"] . "%");
                            })
                            ->paginate(8);

                        $appointments->mapWithKeys(function ($item, $key) use ($profile) {
                            $item->student = [
                                "user" => [
                                    "name" => $item->name,
                                    "surname" => $item->surname
                                ],
                                "class" => [
                                    "name" => $item->class_name
                                ]
                            ];

                            // Tiek pieveinota īpašība pierakstam, kas klienta pusē attēlos dzēšanas pogu.
                            $item->edit = ($item->student_id == $profile->id);

                            return $item;
                        });

                        return response()->json(["message" => "success", "data" => $appointments, "delete" => "personal"], 200);
                    }
                }
            } catch (Exception $e) {
                return response()->json(["message" => "error"], 500);
            }
        }
        return response()->json(["message" => "unauthorized"], 403);
    }
}
