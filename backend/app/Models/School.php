<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    use HasFactory;

    protected $table = 'schools';

    protected $fillable = ["name"];

    public function rooms() {
        return $this->hasMany(Room::class);
    }

    public function consultations() {
        return $this->hasMany(Consultation::class);
    }

    public function schoolClasses() {
        return $this->hasMany(SchoolClass::class);
    }

    public function userSchools() {
        return $this->hasMany(UserSchool::class);
    }
}
