<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Consultation extends Model
{
    use HasFactory;

    protected $table = 'consultations';

    protected $fillable= ["teacher_id", "room_id", "date", "attendant_count", "topic"];

    public function teacher() {
        return $this->belongsTo(UserSchool::class);
    }

    public function appointments() {
        return $this->hasMany(Appointment::class);
    }

    public function room() {
        return $this->belongsTo(Room::class);
    }
}
