<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    use HasFactory;

    protected $table = 'appointments';

protected $fillable = ["student_id", "consultation_id", "reason"];

    public function consultation() {
        return $this->belongsTo(Consultation::class);
    }

    public function student() {
        return $this->belongsTo(UserSchool::class);
    }
}
