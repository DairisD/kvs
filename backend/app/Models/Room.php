<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    use HasFactory;

    protected $table = 'rooms';

    protected $fillable = [
        'name',
        'school_id',
        'seat_count',
    ];

    public function school() {
        return $this->belongsTo(School::class);
    }

    public function consultations() {
        return $this->hasMany(Consultation::class);
    }
}
