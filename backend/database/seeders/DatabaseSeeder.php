<?php

namespace Database\Seeders;

use App\Models\Appointment;
use App\Models\Consultation;
use App\Models\Room;
use App\Models\School;
use App\Models\SchoolClass;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\PermissionRegistrar;
use App\Models\User;
use App\Models\UserSchool;
use Illuminate\Support\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        Permission::create(['name' => 'edit school records']);
        Permission::create(['name' => 'edit school admin accounts']);
        Permission::create(['name' => 'edit all teacher profiles']);
        Permission::create(['name' => 'edit school teacher profiles']);
        Permission::create(['name' => 'edit all student profiles']);
        Permission::create(['name' => 'edit school student profiles']);
        Permission::create(['name' => 'edit all room records']);
        Permission::create(['name' => 'edit school room records']);
        Permission::create(['name' => 'edit all consultation records']);
        Permission::create(['name' => 'edit school consultation records']);
        Permission::create(['name' => 'edit personal consultation records']);
        Permission::create(['name' => 'edit all appointment records']);
        Permission::create(['name' => 'edit school appointment records']);
        Permission::create(['name' => 'edit personal appointment records']);
        Permission::create(['name' => 'edit all class records']);
        Permission::create(['name' => 'edit school class records']);


        $role1 = Role::create(['name' => 'system_admin']);
        $role1->givePermissionTo(Permission::all());

        $role2 = Role::create(['name' => 'school_admin']);
        $role2->givePermissionTo('edit school teacher profiles');
        $role2->givePermissionTo('edit school student profiles');
        $role2->givePermissionTo('edit school room records');
        $role2->givePermissionTo('edit school consultation records');
        $role2->givePermissionTo('edit school appointment records');
        $role2->givePermissionTo('edit school class records');

        $role3 = Role::create(['name' => 'teacher']);
        $role3->givePermissionTo('edit personal consultation records');
        $role3->givePermissionTo('edit personal appointment records');

        $role4 = Role::create(['name' => 'student']);
        $role4->givePermissionTo('edit personal appointment records');

        $user = new User;
        $user->username = 'admin';
        $user->surname = 'Admin';
        $user->name = "Test";
        $user->password = Hash::make('password');

        $user1 = new User;
        $user1->username = 'admin1';
        $user1->surname = 'Lielais';
        $user1->name = "Ansis";
        $user1->password = Hash::make('password');

        $user2 = new User;
        $user2->username = 'teacher';
        $user2->surname = 'Admin';
        $user2->name = "Test";
        $user2->password = Hash::make('password');

        $user3 = new User;
        $user3->username = 'student';
        $user3->surname = 'Admin';
        $user3->name = "Test";
        $user3->password = Hash::make('password');

        $user->save();
        $user1->save();
        $user2->save();
        $user3->save();

        $userSchool = $user->userSchools()->create(['user_id' => $user->id]);
        $userSchool->save();
        $userSchool->assignRole("system_admin");

        $school_1 = new School(["name" => "Āgenskalna Valsts ģimnāzija"]);
        $school_1->save();

        $school_2 = new School(["name" => "Rīgas Valsts 1. ģimnāzija"]);
        $school_2->save();

        $school_3 = new School(["name" => "Rīgas Valsts vācu ģimnāzija"]);
        $school_3->save();

        $school_4 = new School(["name" => "Jelgavas ģimnāzija"]);
        $school_4->save();

        $school_5 = new School(["name" => "Iļģuciema vidusskola"]);
        $school_5->save();

        $school_6 = new School(["name" => "Rīgas 96. vidusskola"]);
        $school_6->save();

        $school_7 = new School(["name" => "Dobles Valsts ģimnāzija"]);
        $school_7->save();

        $school_8 = new School(["name" => "Bukaišu skola"]);
        $school_8->save();

        $school_9 = new School(["name" => "Rīgas Valsts 3. ģimnāzija"]);
        $school_9->save();

        $school_10 = new School(["name" => "Krāslavas Valsts ģimnāzija"]);
        $school_10->save();

        $school_11 = new School(["name" => "Valmieras Valsts ģimnāzija"]);
        $school_11->save();

        $school_12 = new School(["name" => "Rīgas Valsts Klasiskā ģimnāzija"]);
        $school_12->save();

        $school_13 = new School(["name" => "Rēzeknes Valsts 1. ģimnāzija"]);
        $school_13->save();

        $school_14 = new School(["name" => "Cēsu Valsts ģimnāzija"]);
        $school_14->save();

        $school_15 = new School(["name" => "Rīgas 10. vidusskola"]);
        $school_15->save();

        $school_16 = new School(["name" => "Rīgas 40. vidusskola"]);
        $school_16->save();

        $school_17 = new School(["name" => "Pumpuru vidusskola"]);
        $school_17->save();

        $school_18 = new School(["name" => "Ziemļvalstu ģimnāzija"]);
        $school_18->save();

        $school_19 = new School(["name" => "Ventspils 4. vidusskola"]);
        $school_19->save();

        $class1 = $school_1->schoolClasses()->create(["name" => "10.1"]);

        $userSchool1 = new UserSchool();
        $userSchool1->school_id = $school_1->id;
        $userSchool1->user_id = $user2->id;
        $userSchool1->save();
        $userSchool1->assignRole("teacher");
        
        $userSchool2 = new UserSchool();
        $userSchool2->school_id = $school_1->id;
        $userSchool2->class_id = $class1->id;
        $userSchool2->user_id = $user3->id;
        $userSchool2->save();
        $userSchool2->assignRole("student");

        $userSchool3 = new UserSchool();
        $userSchool3->school_id = $school_2->id;
        $userSchool3->user_id = $user1->id;
        $userSchool3->save();
        $userSchool3->assignRole("school_admin");

        $userSchool4 = new UserSchool();
        $userSchool4->school_id = $school_2->id;
        $userSchool4->user_id = $user->id;
        $userSchool4->save();
        $userSchool4->assignRole("teacher");

        $room1 = new Room(["name" => "13. auditorija", "seat_count" => 110]);
        $room1->school_id = $school_1->id;
        $room1->save();

        $room2 = new Room(["name" => "10. kabinets", "seat_count" => 30]);
        $room2->school_id = $school_1->id;
        $room2->save();

        $room3 = new Room(["name" => "201. kabinets", "seat_count" => 28]);
        $room3->school_id = $school_1->id;
        $room3->save();

        $room4 = new Room(["name" => "Aktu zāle", "seat_count" => 140]);
        $room4->school_id = $school_1->id;
        $room4->save();

        $room5 = new Room(["name" => "220. kabinets", "seat_count" => 31]);
        $room5->school_id = $school_2->id;
        $room5->save();

        $room6 = new Room(["name" => "16. auditorija", "seat_count" => 72]);
        $room6->school_id = $school_2->id;
        $room6->save();

        $room7 = new Room(["name" => "Konferenču telpa", "seat_count" => 25]);
        $room7->school_id = $school_2->id;
        $room7->save();

        $room8 = new Room(["name" => "21. kabinets", "seat_count" => 15]);
        $room8->school_id = $school_2->id;
        $room8->save();

        $room9 = new Room(["name" => "26. kabinets", "seat_count" => 2]);
        $room9->school_id = $school_2->id;
        $room9->save();

        $room10 = new Room(["name" => "201. kabinets", "seat_count" => 8]);
        $room10->school_id = $school_2->id;
        $room10->save();

        $room11 = new Room(["name" => "200. kabinets", "seat_count" => 7]);
        $room11->school_id = $school_2->id;
        $room11->save();

        $room12 = new Room(["name" => "167. kabinets", "seat_count" => 99]);
        $room12->school_id = $school_2->id;
        $room12->save();

        $room13 = new Room(["name" => "19. auditorija", "seat_count" => 14]);
        $room13->school_id = $school_2->id;
        $room13->save();

        $room14 = new Room(["name" => "7. kabinets", "seat_count" => 19]);
        $room14->school_id = $school_2->id;
        $room14->save();

        $room15 = new Room(["name" => "25. kabinets", "seat_count" => 19]);
        $room15->school_id = $school_2->id;
        $room15->save();

        $room16 = new Room(["name" => "401. kabinets", "seat_count" => 20]);
        $room16->school_id = $school_2->id;
        $room16->save();

        $room17 = new Room(["name" => "405. kabinets", "seat_count" => 75]);
        $room17->school_id = $school_2->id;
        $room17->save();

        $room18 = new Room(["name" => "455. kabinets", "seat_count" => 12]);
        $room18->school_id = $school_2->id;
        $room18->save();

        $room19 = new Room(["name" => "699. kabinets", "seat_count" => 2]);
        $room19->school_id = $school_2->id;
        $room19->save();

        $class1 = new SchoolClass(["name" => "10a"]);
        $class1->school_id = $school_2->id;
        $class1->save();

        $class2 = new SchoolClass(["name" => "10b"]);
        $class2->school_id = $school_2->id;
        $class2->save();

        $class3 = new SchoolClass(["name" => "10c"]);
        $class3->school_id = $school_2->id;
        $class3->save();
        
        $class4 = new SchoolClass(["name" => "11a"]);
        $class4->school_id = $school_2->id;
        $class4->save();

        $class5 = new SchoolClass(["name" => "11b"]);
        $class5->school_id = $school_2->id;
        $class5->save();

        $class6 = new SchoolClass(["name" => "11c"]);
        $class6->school_id = $school_2->id;
        $class6->save();

        $class7 = new SchoolClass(["name" => "7a"]);
        $class7->school_id = $school_2->id;
        $class7->save();

        $class8 = new SchoolClass(["name" => "8b"]);
        $class8->school_id = $school_2->id;
        $class8->save();

        $class9 = new SchoolClass(["name" => "8c"]);
        $class9->school_id = $school_2->id;
        $class9->save();

        $class10 = new SchoolClass(["name" => "7b"]);
        $class10->school_id = $school_2->id;
        $class10->save();

        $class11 = new SchoolClass(["name" => "7c"]);
        $class11->school_id = $school_2->id;
        $class11->save();

        $student1 = new User(["name" => "Jānis", "surname" => "Bērziņš", "username" => "student1", "password" => Hash::make("password")]);
        $student1->save();

        $student2 = new User(["name" => "Maija", "surname" => "Ozola", "username" => "student2", "password" => Hash::make("password")]);
        $student2->save();

        $student3 = new User(["name" => "Elizabete", "surname" => "Caune", "username" => "student3", "password" => Hash::make("password")]);
        $student3->save();

        $student4 = new User(["name" => "Andris", "surname" => "Augškalns", "username" => "student4", "password" => Hash::make("password")]);
        $student4->save();

        $student5 = new User(["name" => "Dainis", "surname" => "Kalniņš", "username" => "student5", "password" => Hash::make("password")]);
        $student5->save();

        $student6 = new User(["name" => "Ričards", "surname" => "Dzirne", "username" => "student6", "password" => Hash::make("password")]);
        $student6->save();

        $student7 = new User(["name" => "Kristaps", "surname" => "Birznieks", "username" => "student7", "password" => Hash::make("password")]);
        $student7->save();

        $student8 = new User(["name" => "Rūdolfs", "surname" => "Sproģis", "username" => "student8", "password" => Hash::make("password")]);
        $student8->save();

        $student9 = new User(["name" => "Amanda", "surname" => "Pumpure", "username" => "student9", "password" => Hash::make("password")]);
        $student9->save();

        $student10 = new User(["name" => "Līga", "surname" => "Lūse", "username" => "student10", "password" => Hash::make("password")]);
        $student10->save();

        $student11 = new User(["name" => "Emīlija", "surname" => "Skudra", "username" => "student11", "password" => Hash::make("password")]);
        $student11->save();

        $student12 = new User(["name" => "Madara", "surname" => "Žigure", "username" => "student12", "password" => Hash::make("password")]);
        $student12->save();

        $studentProfile1 = new UserSchool(["user_id" => $student1->id, "school_id" => $school_2->id, "class_id" => $class1->id]);
        $studentProfile1->assignRole("student");
        $studentProfile1->save();

        $studentProfile2 = new UserSchool(["user_id" => $student2->id, "school_id" => $school_2->id, "class_id" => $class1->id]);
        $studentProfile2->assignRole("student");
        $studentProfile2->save();

        $studentProfile3 = new UserSchool(["user_id" => $student3->id, "school_id" => $school_2->id, "class_id" => $class1->id]);
        $studentProfile3->assignRole("student");
        $studentProfile3->save();

        $studentProfile4 = new UserSchool(["user_id" => $student4->id, "school_id" => $school_2->id, "class_id" => $class1->id]);
        $studentProfile4->assignRole("student");
        $studentProfile4->save();

        $studentProfile5 = new UserSchool(["user_id" => $student5->id, "school_id" => $school_2->id, "class_id" => $class1->id]);
        $studentProfile5->assignRole("student");
        $studentProfile5->save();

        $studentProfile6 = new UserSchool(["user_id" => $student6->id, "school_id" => $school_2->id, "class_id" => $class1->id]);
        $studentProfile6->assignRole("student");
        $studentProfile6->save();

        $studentProfile7 = new UserSchool(["user_id" => $student7->id, "school_id" => $school_2->id, "class_id" => $class1->id]);
        $studentProfile7->assignRole("student");
        $studentProfile7->save();

        $studentProfile8 = new UserSchool(["user_id" => $student8->id, "school_id" => $school_2->id, "class_id" => $class1->id]);
        $studentProfile8->assignRole("student");
        $studentProfile8->save();

        $studentProfile9 = new UserSchool(["user_id" => $student9->id, "school_id" => $school_2->id, "class_id" => $class1->id]);
        $studentProfile9->assignRole("student");
        $studentProfile9->save();

        $studentProfile10 = new UserSchool(["user_id" => $student10->id, "school_id" => $school_2->id, "class_id" => $class1->id]);
        $studentProfile10->assignRole("student");
        $studentProfile10->save();

        $studentProfile11 = new UserSchool(["user_id" => $student11->id, "school_id" => $school_2->id, "class_id" => $class1->id]);
        $studentProfile11->assignRole("student");
        $studentProfile11->save();

        $studentProfile12 = new UserSchool(["user_id" => $student12->id, "school_id" => $school_2->id, "class_id" => $class1->id]);
        $studentProfile12->assignRole("student");
        $studentProfile12->save();

        $teacher1 = new User(["username" => "teacher1", "name" => "Jānis", "surname" => "Slikšāns", "password" => Hash::make("password")]);
        $teacher1->save();

        $teacher2 = new User(["username" => "teacher2", "name" => "Umberts", "surname" => "Ragainis", "password" => Hash::make("password")]);
        $teacher2->save();

        $teacher3 = new User(["username" => "teacher3", "name" => "Dagnis", "surname" => "Ūsāns", "password" => Hash::make("password")]);
        $teacher3->save();

        $teacher4 = new User(["username" => "teacher4", "name" => "Vera", "surname" => "Gale", "password" => Hash::make("password")]);
        $teacher4->save();

        $teacher5 = new User(["username" => "teacher5", "name" => "Doloresa", "surname" => "Dāve", "password" => Hash::make("password")]);
        $teacher5->save();

        $teacher6 = new User(["username" => "teacher6", "name" => "Ausmis", "surname" => "Vitenburgs", "password" => Hash::make("password")]);
        $teacher6->save();
        
        $teacher7 = new User(["username" => "teacher7", "name" => "Igmārs", "surname" => "Diedelis", "password" => Hash::make("password")]);
        $teacher7->save();

        $teacher8 = new User(["username" => "teacher8", "name" => "Luīze", "surname" => "Vurče", "password" => Hash::make("password")]);
        $teacher8->save();

        $teacher9 = new User(["username" => "teacher9", "name" => "Ainārs", "surname" => "Kašels", "password" => Hash::make("password")]);
        $teacher9->save();

        $teacher10 = new User(["username" => "teacher10", "name" => "Klementīne", "surname" => "Druka", "password" => Hash::make("password")]);
        $teacher10->save();

        $teacher11 = new User(["username" => "teacher11", "name" => "Voldemārs", "surname" => "Žīgurs", "password" => Hash::make("password")]);
        $teacher11->save();

        $teacher12 = new User(["username" => "teacher12", "name" => "Melita", "surname" => "Dzelzskalēja", "password" => Hash::make("password")]);
        $teacher12->save();

        $teacher13 = new User(["username" => "teacher13", "name" => "Romualda", "surname" => "Bērzupe", "password" => Hash::make("password")]);
        $teacher13->save();

        $teacher14 = new User(["username" => "teacher14", "name" => "Kristers", "surname" => "Lukstišs", "password" => Hash::make("password")]);
        $teacher14->save();

        $teacher15 = new User(["username" => "teacher15", "name" => "Ivars", "surname" => "Boldaveško", "password" => Hash::make("password")]);
        $teacher15->save();

        $teacher16 = new User(["username" => "teacher16", "name" => "Iraīda", "surname" => "Duka", "password" => Hash::make("password")]);
        $teacher16->save();

        $teacher17 = new User(["username" => "teacher17", "name" => "Alvis", "surname" => "Smiltēns", "password" => Hash::make("password")]);
        $teacher17->save();

        $teacher18 = new User(["username" => "teacher18", "name" => "Ģirts", "surname" => "Bāriņš", "password" => Hash::make("password")]);
        $teacher18->save();

        $teacherProfile1 = new UserSchool(["user_id" => $teacher1->id, "school_id" => $school_2->id]);
        $teacherProfile1->assignRole("teacher");
        $teacherProfile1->save();

        $teacherProfile2 = new UserSchool(["user_id" => $teacher2->id, "school_id" => $school_2->id]);
        $teacherProfile2->assignRole("teacher");
        $teacherProfile2->save();

        $teacherProfile3 = new UserSchool(["user_id" => $teacher3->id, "school_id" => $school_2->id]);
        $teacherProfile3->assignRole("teacher");
        $teacherProfile3->save();

        $teacherProfile4 = new UserSchool(["user_id" => $teacher4->id, "school_id" => $school_2->id]);
        $teacherProfile4->assignRole("teacher");
        $teacherProfile4->save();

        $teacherProfile5 = new UserSchool(["user_id" => $teacher5->id, "school_id" => $school_2->id]);
        $teacherProfile5->assignRole("teacher");
        $teacherProfile5->save();

        $teacherProfile6 = new UserSchool(["user_id" => $teacher6->id, "school_id" => $school_2->id]);
        $teacherProfile6->assignRole("teacher");
        $teacherProfile6->save();

        $teacherProfile7 = new UserSchool(["user_id" => $teacher7->id, "school_id" => $school_2->id]);
        $teacherProfile7->assignRole("teacher");
        $teacherProfile7->save();

        $teacherProfile8 = new UserSchool(["user_id" => $teacher8->id, "school_id" => $school_2->id]);
        $teacherProfile8->assignRole("teacher");
        $teacherProfile8->save();

        $teacherProfile9 = new UserSchool(["user_id" => $teacher9->id, "school_id" => $school_2->id]);
        $teacherProfile9->assignRole("teacher");
        $teacherProfile9->save();

        $teacherProfile10 = new UserSchool(["user_id" => $teacher10->id, "school_id" => $school_2->id]);
        $teacherProfile10->assignRole("teacher");
        $teacherProfile10->save();

        $teacherProfile11 = new UserSchool(["user_id" => $teacher11->id, "school_id" => $school_2->id]);
        $teacherProfile11->assignRole("teacher");
        $teacherProfile11->save();

        $teacherProfile12 = new UserSchool(["user_id" => $teacher12->id, "school_id" => $school_2->id]);
        $teacherProfile12->assignRole("teacher");
        $teacherProfile12->save();

        $teacherProfile13 = new UserSchool(["user_id" => $teacher13->id, "school_id" => $school_2->id]);
        $teacherProfile13->assignRole("teacher");
        $teacherProfile13->save();

        $teacherProfile14 = new UserSchool(["user_id" => $teacher14->id, "school_id" => $school_2->id]);
        $teacherProfile14->assignRole("teacher");
        $teacherProfile14->save();

        $consultation1 = new Consultation(["teacher_id" => $teacher1->id, "room_id" => $room5->id, "date" => "2022-01-01T00:23:12.854Z", "attendant_count" => $room5->seat_count, "topic" => "1. Tēma par kinemātiku. Brīvās krišanas paātrinājums"]);
        $consultation1->save();

        $consultation2 = new Consultation(["teacher_id" => $teacher2->id, "room_id" => $room6->id, "date" => "2021-12-15T00:23:53.854Z", "attendant_count" => $room6->seat_count, "topic" => "Interpunkcija"]);
        $consultation2->save();

        $consultation3 = new Consultation(["teacher_id" => $teacher3->id, "room_id" => $room7->id, "date" => "2021-12-15T00:23:53.854Z", "attendant_count" => $room7->seat_count, "topic" => "Ogļūdeņraži I"]);
        $consultation3->save();

        $consultation4 = new Consultation(["teacher_id" => $teacher1->id, "room_id" => $room8->id, "date" => "2021-12-15T00:23:53.854Z", "attendant_count" => $room8->seat_count, "topic" => "Kvadrātvienādojumi"]);
        $consultation4->save();

        $consultation5 = new Consultation(["teacher_id" => $teacher1->id, "room_id" => $room5->id, "date" => "2021-12-15T00:23:53.854Z", "attendant_count" => $room5->seat_count, "topic" => "1. Pārbaudes darba labošana"]);
        $consultation5->save();

        $consultation6 = new Consultation(["teacher_id" => $teacher4->id, "room_id" => $room5->id, "date" => "2021-12-15T00:23:53.854Z", "attendant_count" => $room5->seat_count, "topic" => "Atkārtojums par Eiropas valstīm"]);
        $consultation6->save();

        $consultation7 = new Consultation(["teacher_id" => $teacher5->id, "room_id" => $room6->id, "date" => "2021-12-15T00:23:53.854Z", "attendant_count" => $room6->seat_count, "topic" => "Pārbaudes darba pārrakstīšana"]);
        $consultation7->save();

        $consultation8 = new Consultation(["teacher_id" => $teacher6->id, "room_id" => $room9->id, "date" => "2021-12-23T00:16:00.854Z", "attendant_count" => $room9->seat_count, "topic" => "Pārsprieduma pārrakstīšana"]);
        $consultation8->save();

        $consultation9 = new Consultation(["teacher_id" => $teacher2->id, "room_id" => $room15->id, "date" => "2021-12-23T00:16:00.854Z", "attendant_count" => $room15->seat_count, "topic" => "Projekta darba konsultācija"]);
        $consultation9->save();

        $consultation10 = new Consultation(["teacher_id" => $teacher2->id, "room_id" => $room16->id, "date" => "2021-12-23T00:16:00.854Z", "attendant_count" => $room16->seat_count, "topic" => "Eksāmena sagatavošanās"]);
        $consultation10->save();

        $consultation11 = new Consultation(["teacher_id" => $teacher5->id, "room_id" => $room14->id, "date" => "2021-12-23T00:16:00.854Z", "attendant_count" => $room14->seat_count, "topic" => "Olimpiādes uzdevumu pildīšana"]);
        $consultation11->save();

        $appointment1 = new Appointment(["consultation_id" => $consultation8->id, "student_id" => $studentProfile1->id, "reason" => "Apgūt kvadrātvienādojumus."]);
        $appointment1->save();

        $appointment2 = new Appointment(["consultation_id" => $consultation8->id, "student_id" => $studentProfile2->id, "reason" => "Labot pēdējo pārbaudes darbu."]);
        $appointment2->save();

        $appointment3 = new Appointment(["consultation_id" => $consultation8->id, "student_id" => $studentProfile3->id, "reason" => "Uzrakstīt jauno mājasdarbu."]);
        $appointment3->save();

        $appointment4 = new Appointment(["consultation_id" => $consultation8->id, "student_id" => $studentProfile4->id, "reason" => "Saprast informāciju par datu struktūrām."]);
        $appointment4->save();

        $appointment5 = new Appointment(["consultation_id" => $consultation8->id, "student_id" => $studentProfile5->id, "reason" => "Labot pārbaudes darbu."]);
        $appointment5->save();

        $appointment6 = new Appointment(["consultation_id" => $consultation8->id, "student_id" => $studentProfile6->id, "reason" => "Uzraksīt pārspriedumu."]);
        $appointment6->save();

        $appointment7 = new Appointment(["consultation_id" => $consultation8->id, "student_id" => $studentProfile7->id, "reason" => "Izrunāt prezentācijas vērtējumu."]);
        $appointment7->save();

        $appointment8 = new Appointment(["consultation_id" => $consultation8->id, "student_id" => $studentProfile8->id, "reason" => "Sagatavoties pārbaudes darbam."]);
        $appointment8->save();

        $appointment9 = new Appointment(["consultation_id" => $consultation8->id, "student_id" => $studentProfile9->id, "reason" => "Labot pirmo pārbaudes darbu."]);
        $appointment9->save();

        $appointment10 = new Appointment(["consultation_id" => $consultation8->id, "student_id" => $studentProfile10->id, "reason" => "Labot otro pārbaudes darbu."]);
        $appointment10->save();

        $appointment11 = new Appointment(["consultation_id" => $consultation8->id, "student_id" => $studentProfile11->id, "reason" => "Izlabot nv"]);
        $appointment11->save();
    }
}
